package ru.ekassir.tests.pochta.dbp.dashboard;

import static org.apache.http.HttpStatus.SC_OK;
import ru.ekassir.dbp.helpers.JsonHelper;
import ru.ekassir.dbp.helpers.JsonHelper;

import ru.ekassir.common.load.CommonWraps;
import ru.ekassir.dbp.models.json.MoneyModel;
import ru.ekassir.dbp.models.json.dashboard.DashboardModel;
import ru.ekassir.dbp.models.json.service.DefiniteServiceModel;
import org.junit.*;

public class DashboardTests extends CommonWraps  {
	  //Проверка что у пользователя с возможностью выпуска ПВК есть соответствующий сервис на dashboard
	  @Test()
	  public void dashboardPVKApprovedStatusTest(){
	    String dashboardJson = login(DASHBOARD_URL, user);
	    DashboardModel dashboardModel = JsonHelper.deserializeJson(DashboardModel.class, dashboardJson);
	    checkStatus(SC_OK);    
	    JsonHelper.isAllNotNullFieldsPresent(dashboardModel.session.account);
	    JsonHelper.isAllNotNullFieldsPresent(dashboardModel.session.account.owner);
	    JsonHelper.isAllNotNullFieldsPresent(dashboardModel.session.user);
	    boolean isNewWalletServiceOnDashboard = false;
	    for(DefiniteServiceModel service : dashboardModel.services.items){
	      if(service.name.equals("Предоплаченная Виртуальная карта")){
	        isNewWalletServiceOnDashboard  = true;	        
	        Assert.assertEquals("Wallet opening service type.", service.type, "newWallet");
	        Assert.assertEquals("Wallet opening service status.", service.application.status, "approved");
	      }
	    }
	    Assert.assertFalse("No wallet opening service on dashboard.", !isNewWalletServiceOnDashboard);
	  }
	
	  //Проверка соответствия полей в ресурсе "/self" с API DBP
	  @Test()
	  public void dashboardTest(){
	    String dashboardJson = login(DASHBOARD_URL, user);
	    DashboardModel dashboardModel = JsonHelper.deserializeJson(DashboardModel.class, dashboardJson);    
	    JsonHelper.isAllNotNullFieldsPresent(dashboardModel.session.account);
	    JsonHelper.isAllNotNullFieldsPresent(dashboardModel.session.account.owner);
	    JsonHelper.isAllNotNullFieldsPresent(dashboardModel.session.user);
	  }
	  
	  //Проверка фильтра exclude=events на dashboard 
	  @Test()
	  public void dashboardWithoutEventsFilterTest() {
	    String dashboardJson = login(DASHBOARD_URL, user);
	    DashboardModel dashboardModel = JsonHelper.deserializeJson(DashboardModel.class, dashboardJson);
	    JsonHelper.isAllNotNullFieldsPresent(dashboardModel);
	    Assert.assertTrue("No importantEventCount on dashboard when exclude=events filter set", dashboardModel.importantEventCount == null);
	  }
	  
	  //Проверка работы query параметра exclude со значением services на dashboard
	  @Test()
	  public void dashboardWithoutServicesFilterTest() {
	    String dashboardJson = login(DASHBOARD_URL, user);                                       
	    DashboardModel dashboardModel = JsonHelper.deserializeJson(DashboardModel.class, dashboardJson);
	    JsonHelper.isAllNotNullFieldsPresent(dashboardModel);
	    Assert.assertFalse("No services on dashboard", dashboardModel.services == null);
	    Assert.assertTrue("At least one service return on dashboard", dashboardModel.services.items.length > 0);
	    String dashboardWithoutServices = get.send(DASHBOARD_URL + "&exclude=services");
	    checkStatus(SC_OK);    
	    DashboardModel dashboardWithotServicesModel = JsonHelper.deserializeJson(DashboardModel.class, dashboardWithoutServices);
	    JsonHelper.isAllNotNullFieldsPresent(dashboardWithotServicesModel);
	    Assert.assertTrue("No services on dashboard with exclude=services.", dashboardWithotServicesModel.services == null);
	  }
	  
	  
	//Проверка того что для LITE пользователя на dashboard с query параметром expand=identificationStatus передается identificationStatus 
	  @Test()
	  public void liteIdentUserDashboardTest(){
	    //TODO: найти пользователя
	    login(DASHBOARD_URL, user);
	    String dashboardJson = get.send("/self?expand=identificationStatus");
	    DashboardModel dashboardModel = JsonHelper.deserializeJson(DashboardModel.class, dashboardJson);    
	    JsonHelper.isAllNotNullFieldsPresent(dashboardModel.session.account);
	    JsonHelper.isAllNotNullFieldsPresent(dashboardModel.session.account.owner);
	    JsonHelper.isAllNotNullFieldsPresent(dashboardModel.session.user);
	    Assert.assertTrue( "User with lite identification have \"identificationStatus\" field", dashboardModel.identificationStatus != null);
	    JsonHelper.isAllNotNullFieldsPresent(dashboardModel.identificationStatus);   
	  }
	  
	//Проверка подсчета totalAvailable на dashboard
	  @Test()
	  public void totalAvailableTest() {
	    String dashJson = login(DASHBOARD_URL, user);
	    DashboardModel dashModel = JsonHelper.deserializeJson(DashboardModel.class, dashJson);
	    MoneyModel total = dashModel.totalAvailableAmount;
	    Long calculatedTotalAvailable = calculateTotalAvailableOnDashboard().getAmount("RUB");
	    log("Calculated totalAvailable: " + calculatedTotalAvailable.toString());
	    Long totalAvailable = total.getAmount();
	    log("totalAvailable from dashboard: " + totalAvailable);
	    Assert.assertTrue(calculatedTotalAvailable.equals(totalAvailable));
	  }
	  
	  @Test()
    public void dashboardWithApplications(){
      String dashboardJson = login(DASHBOARD_URL, user);                                       
      DashboardModel dashboardModel = JsonHelper.deserializeJson(DashboardModel.class, dashboardJson);
      JsonHelper.isAllNotNullFieldsPresent(dashboardModel);
      String dashboardWithPromosAndArea = get.send(DASHBOARD_URL + "&expand=applications");
      checkStatus(SC_OK);
      DashboardModel dashboardWithPromosAndAreaModel = JsonHelper.deserializeJson(DashboardModel.class, dashboardWithPromosAndArea);
      JsonHelper.isAllNotNullFieldsPresent(dashboardWithPromosAndAreaModel);  
    }
	  
	  @Test()
    public void dashboardWithIdentificationStatus(){
      String dashboardJson = login(DASHBOARD_URL, user);                                       
      DashboardModel dashboardModel = JsonHelper.deserializeJson(DashboardModel.class, dashboardJson);
      JsonHelper.isAllNotNullFieldsPresent(dashboardModel);
      String dashboardWithPromosAndArea = get.send(DASHBOARD_URL + "&expand=identificationStatus");
      checkStatus(SC_OK);
      DashboardModel dashboardWithPromosAndAreaModel = JsonHelper.deserializeJson(DashboardModel.class, dashboardWithPromosAndArea);
      JsonHelper.isAllNotNullFieldsPresent(dashboardWithPromosAndAreaModel);  
    }
	  	    
	  @Test()
	  public void dashboardWithPromosAndArea(){
		  String dashboardJson = login(DASHBOARD_URL, user);                                       
		  DashboardModel dashboardModel = JsonHelper.deserializeJson(DashboardModel.class, dashboardJson);
		  JsonHelper.isAllNotNullFieldsPresent(dashboardModel);
		  String dashboardWithPromosAndArea = get.send(DASHBOARD_URL + "&expand=promos&area=dashboard");
		  checkStatus(SC_OK);
		  DashboardModel dashboardWithPromosAndAreaModel = JsonHelper.deserializeJson(DashboardModel.class, dashboardWithPromosAndArea);
		  JsonHelper.isAllNotNullFieldsPresent(dashboardWithPromosAndAreaModel);  
	  }
	  
	  @Test()
	  public void dashboardWithPromosAndTwoArea(){
		  String dashboardJson = login(DASHBOARD_URL, user);                                       
		  DashboardModel dashboardModel = JsonHelper.deserializeJson(DashboardModel.class, dashboardJson);
		  JsonHelper.isAllNotNullFieldsPresent(dashboardModel);
		  String dashboardWithPromosAndArea = get.send(DASHBOARD_URL + "&expand=promos&area=banner&area=dashboard");
		  checkStatus(SC_OK);
		  DashboardModel dashboardWithPromosAndAreaModel = JsonHelper.deserializeJson(DashboardModel.class, dashboardWithPromosAndArea);
		  JsonHelper.isAllNotNullFieldsPresent(dashboardWithPromosAndAreaModel);  
	  }
	  
	  @Test()
	  public void dashboardWithPromosAndTwoAreaAndApplicationsAndidentificationStatus(){
		  String dashboardJson = login(DASHBOARD_URL, user);                                       
		  DashboardModel dashboardModel = JsonHelper.deserializeJson(DashboardModel.class, dashboardJson);
		  JsonHelper.isAllNotNullFieldsPresent(dashboardModel);
		  String dashboardWithPromosAndArea = get.send(DASHBOARD_URL + "&expand=promos&area=banner&area=dashboard&expand=applications&expand=identificationStatus");
		  checkStatus(SC_OK);
		  DashboardModel dashboardWithPromosAndAreaModel = JsonHelper.deserializeJson(DashboardModel.class, dashboardWithPromosAndArea);
		  JsonHelper.isAllNotNullFieldsPresent(dashboardWithPromosAndAreaModel);
	  }
	  
	  @Test()
	  public void dashboardWithPromosAndTwoAreasAndidentificationStatus(){
		  String dashboardJson = login(DASHBOARD_URL, user);                                       
		  DashboardModel dashboardModel = JsonHelper.deserializeJson(DashboardModel.class, dashboardJson);
		  JsonHelper.isAllNotNullFieldsPresent(dashboardModel);
		  String dashboardWithPromosAndArea = get.send(DASHBOARD_URL + "&expand=promos&area=banner&area=dashboard&expand=identificationStatus");
		  checkStatus(SC_OK);
		  DashboardModel dashboardWithPromosAndAreaModel = JsonHelper.deserializeJson(DashboardModel.class, dashboardWithPromosAndArea);
		  JsonHelper.isAllNotNullFieldsPresent(dashboardWithPromosAndAreaModel);
	  }
	  
	  @Test()
	  public void dashboardWithPromosAndTwoAreasAndApplications(){
		  String dashboardJson = login(DASHBOARD_URL, user);                                       
		  DashboardModel dashboardModel = JsonHelper.deserializeJson(DashboardModel.class, dashboardJson);
		  JsonHelper.isAllNotNullFieldsPresent(dashboardModel);
		  String dashboardWithPromosAndArea = get.send(DASHBOARD_URL + "&expand=promos&area=banner&area=dashboard&expand=applications");
		  checkStatus(SC_OK);
		  DashboardModel dashboardWithPromosAndAreaModel = JsonHelper.deserializeJson(DashboardModel.class, dashboardWithPromosAndArea);
		  JsonHelper.isAllNotNullFieldsPresent(dashboardWithPromosAndAreaModel);
	  }
	  

}
