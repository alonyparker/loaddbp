package ru.ekassir.tests.pochta.dbp.account;

import  ru.ekassir.dbp.helpers.JsonHelper;

import ru.ekassir.common.load.CommonWraps;
import ru.ekassir.dbp.models.json.base.SessionInfoModel;
import org.junit.*;
public class SessionTests extends CommonWraps{
//Проверка соответствия полей в ресурсе "/session" с API DBP
  @Test
  public void sessionTest(){
    String session = login("/session", user);
    SessionInfoModel sessionModel = JsonHelper.deserializeJson(SessionInfoModel.class, session);
    JsonHelper.isAllNotNullFieldsPresent(sessionModel);
  }

}
