package ru.ekassir.tests.pochta.dbp.operation;

import static org.apache.http.HttpStatus.SC_OK;
import  ru.ekassir.dbp.helpers.JsonHelper;

import org.json.JSONObject;

import org.junit.*;
import ru.ekassir.common.load.OperationsWraps;
import ru.ekassir.dbp.helpers.TestHelper;
import ru.ekassir.dbp.models.json.base.ResultModel;
import ru.ekassir.dbp.models.json.operation.PaymentToolModel;
import ru.ekassir.dbp.models.json.operation.PaymentToolsModel;
import ru.ekassir.dbp.models.json.operation.TemplateModel;
import ru.ekassir.dbp.models.json.operation.TemplatesModel;

public class TemplateTests extends OperationsWraps{
  
  @Test()
  public void getTemplatesTest() {
    String templatesJson = login("/banking/operations/templates", user);
    TemplatesModel templatesModel = JsonHelper.deserializeJson(TemplatesModel.class, templatesJson);
    Assert.assertFalse( "User \"" + user + "\" have no templates.", templatesModel.items.length == 0);
    for(TemplateModel template : templatesModel.items){
      JsonHelper.isAllNotNullFieldsPresent(template);
      JsonHelper.isAllNotNullFieldsPresent(template.amount);      
      get.send(environmentUrl + "/banking/operations/templates/" + template.id);
      //checkStatus(SC_OK);
      TemplateModel concreteTemplate = JsonHelper.deserializeJson(TemplateModel.class,  get.getLastResponse());
      JsonHelper.isAllNotNullFieldsPresent(template.amount);
      Assert.assertEquals(concreteTemplate.id, template.id);
    }
  }
  
  
  //изменение имени шаблона
  @Test()
  public void updateTemplateNameTest()  {
    String templatesJson = login("/banking/operations/templates", user);
    TemplatesModel templatesModel = JsonHelper.deserializeJson(TemplatesModel.class, templatesJson);
    Assert.assertFalse(templatesModel.items.length == 0);
    TemplateModel template = templatesModel.items[0];
    String newName = "newName" + TestHelper.randInt(0, 999999999);
    JSONObject newGroup = new JSONObject();
    newGroup.put("name", newName);
    put.send("/banking/operations/templates/" + template.id, newGroup.toString());
    //checkStatus(SC_OK);
    //проверка изменений в ответе
    ResultModel resultModel = JsonHelper.deserializeJson(ResultModel.class, put.getLastResponse());
    Assert.assertEquals(resultModel.result, "OK");
    //просмотр шаблона после изменения имени
    get.send("/banking/operations/templates/" + template.id);
    //checkStatus(SC_OK);    
    TemplateModel TemplateModelAfterChange = JsonHelper.deserializeJson(TemplateModel.class, get.getLastResponse());
    Assert.assertEquals(TemplateModelAfterChange.name, newName);
  }

  //изменение флага isFavorite
  @Test()
  public void updateTemplateIsFavorite()  {
    String templatesJson = login("/banking/operations/templates", user);
    TemplatesModel templatesModel = JsonHelper.deserializeJson(TemplatesModel.class, templatesJson);
    Assert.assertFalse(templatesModel.items.length == 0);
    TemplateModel template = templatesModel.items[0];
    Boolean newIsFavorite = !template.isFavorite;
    JSONObject isFavorite = new JSONObject();
    isFavorite.put("isFavorite", newIsFavorite);
    put.send("/banking/operations/templates/" + template.id, isFavorite.toString());
   // checkStatus(SC_OK);
    //проверка изменений в ответе
    ResultModel resultModel = JsonHelper.deserializeJson(ResultModel.class, put.getLastResponse());
    Assert.assertEquals(resultModel.result, "OK",  ("result"));
    //проверка шаблона после изменения
    get.send("/banking/operations/templates/" + template.id);
   // checkStatus(SC_OK);
    TemplateModel TemplateModelAfterChange = JsonHelper.deserializeJson(TemplateModel.class, get.getLastResponse());
    Assert.assertEquals(TemplateModelAfterChange.isFavorite, newIsFavorite);
  }
  
  //Изменение суммы в шаблоне
  @Test()
  public void updateTemplateAmountTest()  {
    String templatesJson = login("/banking/operations/templates", user);
    TemplatesModel templatesModel = JsonHelper.deserializeJson(TemplatesModel.class, templatesJson);
    Assert.assertFalse(templatesModel.items.length == 0);
    TemplateModel template = templatesModel.items[0];
    Long newTemplateAmount = template.amount.amount + 1;
    JSONObject newAmount = new JSONObject();
    JSONObject amount = new JSONObject();
    amount.put("currencyCode", "RUB");
    amount.put("amount", newTemplateAmount);
    newAmount.put("amount", amount);
    put.send("/banking/operations/templates/" + template.id, newAmount.toString());
    //checkStatus(SC_OK);
    //проверка изменений в ответе
    ResultModel resultModel = JsonHelper.deserializeJson(ResultModel.class, put.getLastResponse());
    Assert.assertEquals(resultModel.result, "OK");
    //проверка самого шаблона после изменения
    get.send("/banking/operations/templates/" + template.id);
    //checkStatus(SC_OK);
    TemplateModel TemplateModelAfterChange = JsonHelper.deserializeJson(TemplateModel.class, get.getLastResponse());
    Assert.assertEquals(TemplateModelAfterChange.amount.amount, newTemplateAmount);
  }
  
  //Изменение платежного инструмента в шаблоне
  @Test()
  public void updateTemplatePaymentToolTest()  {
    String templatesJson = login("/banking/operations/templates", user);
    TemplatesModel templatesModel = JsonHelper.deserializeJson(TemplatesModel.class, templatesJson);
    Assert.assertFalse(templatesModel.items.length == 0);
    TemplateModel template = templatesModel.items[0];
    get.send("/banking/operations/templates/" + template.id);
    //checkStatus(SC_OK);    
    TemplateModel extendTemplateModel = JsonHelper.deserializeJson(TemplateModel.class, get.getLastResponse());
    get.send("/banking/operations/paymentTools/");
    //checkStatus(SC_OK);    
    PaymentToolsModel availablePaymentTools = JsonHelper.deserializeJson(PaymentToolsModel.class, get.getLastResponse());
    Assert.assertFalse(availablePaymentTools.items.length == 0);
    PaymentToolModel newPaymentTool = null;
    for(PaymentToolModel paymentTool : availablePaymentTools.items) {
      if(!paymentTool.id.equals(extendTemplateModel.paymentTool.id)) { 
        newPaymentTool = paymentTool;
      }
    }
    Assert.assertFalse(newPaymentTool == null);    
    JSONObject newPaymentToolJson = new JSONObject();
    JSONObject paymentToolObject = new JSONObject(newPaymentTool.toString());
    newPaymentToolJson.put("paymentTool", paymentToolObject);
    put.send("/banking/operations/templates/" + template.id, newPaymentToolJson.toString());
   // checkStatus(SC_OK);
    //проверка изменений в ответе
    ResultModel resultModel = JsonHelper.deserializeJson(ResultModel.class, put.getLastResponse());
    Assert.assertEquals(resultModel.result, "OK");
    //открытие шаблона после изменения
    get.send("/banking/operations/templates/" + template.id);
   // checkStatus(SC_OK);    
    TemplateModel TemplateModelAfterChange = JsonHelper.deserializeJson(TemplateModel.class, get.getLastResponse());
    Assert.assertEquals(TemplateModelAfterChange.paymentTool.id, newPaymentTool.id);
  } 
}
