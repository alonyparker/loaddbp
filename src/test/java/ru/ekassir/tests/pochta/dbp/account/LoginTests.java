package ru.ekassir.tests.pochta.dbp.account;

import static org.apache.http.HttpStatus.SC_OK;

import ru.ekassir.common.load.CommonWraps;
import ru.ekassir.dbp.models.json.user.LoginModel;
import org.junit.*;

public class LoginTests extends CommonWraps  {
	//Проверка соответствия полей в ресурсе "/login" с API DBP
	  @Test
	  public void loginTest(){
	    String loginJson = login("/user/login", user);
	    JsonHelper.deserializeJson(LoginModel.class, loginJson);
	    checkStatus(SC_OK);
	  }
	  
}
