package ru.ekassir.tests.pochta.dbp.agreement;

import static org.apache.http.HttpStatus.SC_OK;
import  ru.ekassir.dbp.helpers.JsonHelper;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import ru.ekassir.common.load.LimitsWraps;
import ru.ekassir.dbp.helpers.TestHelper;
import ru.ekassir.dbp.models.json.limits.LimitModel;
import ru.ekassir.dbp.models.json.limits.LimitsModel;
import org.junit.*;

public class LimitTests extends LimitsWraps{

  @Test()
  public void amountOntimeSetMaxAmountTest()  {
    setMaxAmountTest("amount", "onetime");
  }

  //type= amount, period = onetime, isActive = false
  @Test()
  public void resetAmountOnetimeLimitTest()  {
    resetLimitTest("amount", "onetime");
  }

  //type= amount, period = onetime, increase maxAmount , isActive = true
  @Test()
  public void amountOntimeIncreaseMaxAmountTest()  {
    increaseMaxAmountTest("amount", "onetime");
  }

  //type= amount, period = onetime, decrease maxAmount , isActive = true
  @Test()
  public void amountOntimeDecreaseMaxAmountTest()  {
    decreaseMaxAmountTest("amount", "onetime");
  }

  //type= amount, period = onetime, decrease maxAmount , isActive = true to true
  @Test()
  public void amountOntimeSameMaxAmountTest()  {
    sameMaxAmountTest("amount", "onetime");
  }

  //type = "amount", period = "onetime",put {type:amount, period: onetime, isActive=true} for limits with isActive=False
  @Test()
  public void amountOnetimeActivateWithoutMaxAmountTest()  {
    activateWithoutMaxAmountTest("amount", "onetime");
  }

/*------ Amount, Day limits ---- */

  //type= amount, period = day, maxAmount from null to value , isActive = true
  @Test()
  public void amountDaySetMaxAmountTest()  {
    setMaxAmountTest("amount", "day");
  }

  //type= amount, period = day, isActive = false
  @Test()
  public void resetAmountDayLimitTest()  {
    resetLimitTest("amount", "day");
  }

  //type= amount, period = day, increase maxAmount , isActive = true
  @Test()
  public void amountDayIncreaseMaxAmountTest()  {
    increaseMaxAmountTest("amount", "day");
  }

  //type= amount, period = day, decrease maxAmount , isActive = true
  @Test()
  public void amountDayDecreaseMaxAmountTest()  {
    decreaseMaxAmountTest("amount", "day");
  }

  //type= amount, period = day, decrease maxAmount , isActive = true to true
  @Test()
  public void amountDaySameMaxAmountTest()  {
    sameMaxAmountTest("amount", "day");
  }

  //type = "amount", period = "day",put {type:amount, period: day, isActive=true} for limits with isActive=False
  @Test()
  public void amountDayActivateWithoutMaxAmountTest()  {
    activateWithoutMaxAmountTest("amount", "day");
  }

  /*------ Amount, Month limits ---*/
  //type= amount, period = month, maxAmount from null to value , isActive = true
  @Test()
  public void amountMonthSetMaxAmountTest()  {
    setMaxAmountTest("amount", "month");
  }
//
@Test()
////type= amount, period = month, isActive = false
public void resetAmountMonthLimitTest()  {
  resetLimitTest("amount", "month");
}
//
@Test()
//type= amount, period = month, increase maxAmount , isActive = true
public void amountMonthIncreaseMaxAmountTest()  {
  increaseMaxAmountTest("amount", "month");
}
//
@Test()
//type= amount, period = month, decrease maxAmount , isActive = true
public void amountMonthDecreaseMaxAmountTest()  {
  decreaseMaxAmountTest("amount", "month");
}
//
@Test()
//type= amount, period = month, decrease maxAmount , isActive = true to true
public void amountMonthSameMaxAmountTest()  {
  sameMaxAmountTest("amount", "month");
}
//type = "amount", period = "month",put {type:amount, period: month, isActive=true} for limits with isActive=False
@Test()
public void amountMonthActivateWithoutMaxAmountTest()  {
  activateWithoutMaxAmountTest("amount", "month");
}
////Amount, Month limits
//
////Set currentAmountFrom 0 to 0


}

