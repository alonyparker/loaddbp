package ru.ekassir.tests.pochta.dbp.account;

import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_OK;
import static org.apache.http.HttpStatus.SC_UNAUTHORIZED;
import  ru.ekassir.dbp.helpers.JsonHelper;

import org.junit.*;
import ru.ekassir.common.load.CommonWraps;
import ru.ekassir.dbp.models.json.password.PasswordModel;

public class PasswordTests extends CommonWraps  {
	 //Проверка соответствия полей в ресурсе "/password" с API DBP
	  @Test()
	  public void passwordTest(){
	    String passwordJson = login("/user/password", user);
	    checkStatus(SC_OK);
	    JsonHelper.deserializeJson(PasswordModel.class, passwordJson);
	  }
	  @Test()
	  public void correctPasswordChange(){
	    String newPassword = getClassProperty("newPassword");
	    login(ACCOUNT_URL, user);
	    String changeReq = createJsonString("password", newPassword);
	    changeReq = addFieldToJson(changeReq, "oldPassword", password);
	    put.send("/user/password", changeReq);
	    checkStatus(SC_UNAUTHORIZED);
	    put.setAuthorizationHeader(getAuthHeaderWithOtp()).send("/user/password", changeReq);
	    checkStatus(SC_OK);
	    //проверка логина с новым паролем
	    clearCookies();
	    login(ACCOUNT_URL, user, newPassword);
	    //возврат старого пароля
	    changeReq = createJsonString("password", password);
	    changeReq = addFieldToJson(changeReq, "oldPassword", newPassword);
	    put.send("/user/password", changeReq);
	    checkStatus(SC_UNAUTHORIZED);
	    put.setAuthorizationHeader(getAuthHeaderWithOtp()).send("/user/password", changeReq);
	    checkStatus(SC_OK);    
	  }
	  
	  @Test()
	  public void invalidOldPasswordTest() {
	    String newPassword = getClassProperty("newPassword");
	    String invalidOldPassword = getClassProperty("invalidOldPassword");
	    login(ACCOUNT_URL, user);
	    String changeReq = createJsonString("password", newPassword);
	    changeReq = addFieldToJson(changeReq, "oldPassword", invalidOldPassword);
	    put.send("/user/password", changeReq);
	    checkStatus(SC_UNAUTHORIZED);
	    put.setAuthorizationHeader(getAuthHeaderWithOtp()).send("/user/password", changeReq);
	    checkStatus(SC_BAD_REQUEST);    
	  }
	  
	  @Test()
	  public void strongPasswordValidationTest() {
	    String strongPassword = getClassProperty("strongPassword");
	    login(ACCOUNT_URL, user);
	    String validationReq = createSimpleJsonString("password", strongPassword);
	    String validation = post.setUrl("/user/password").setBody(validationReq).addParam("action", "validate").send();
	    PasswordModel passwordModel = JsonHelper.deserializeJson(PasswordModel.class, validation);
	    //TODO придумать пароль максимальной сложности
	    //Assert.assertTrue(passwordModel.complexity.rating == passwordModel.complexity.maxRating, "Password is strong (rating = 7).");    
	    Assert.assertTrue("Password is strong (rating = maxRating).", passwordModel.complexity.rating == passwordModel.complexity.maxRating);
	  }
	  
	  @Test()
	  public void badPasswordValidationTest() {
	    String badPassword = getClassProperty("badPassword");
	    login(ACCOUNT_URL, user);
	    String validationReq = createJsonString("password", badPassword);
	    String validation = post.setUrl("/user/password").setBody(validationReq).addParam("action", "validate").send();
	    PasswordModel passwordModel = JsonHelper.deserializeJson(PasswordModel.class, validation);
	    Assert.assertTrue("Password is bad (rating = 0).", passwordModel.complexity.rating == 0);    
	  }
	  
	  @Test()
	  public void mediumPasswordValidationTest() {
	    String mediumPassword = getClassProperty("mediumPassword");
	    login(ACCOUNT_URL, user);
	    String validationReq = createJsonString("password", mediumPassword);
	    String validation = post.setUrl("/user/password").setBody(validationReq).addParam("action", "validate").send();
	    PasswordModel passwordModel = JsonHelper.deserializeJson(PasswordModel.class, validation);
	    Assert.assertTrue("Password is medium (rating = 4).", passwordModel.complexity.rating == 4);    
	  }
	  
	  @Test()
	  public void newAndOldPasswordsTheSameTest() {
	    login(ACCOUNT_URL, user);
	    String changeReq = createJsonString("password", password);
	    changeReq = addFieldToJson(changeReq, "oldPassword", password);
	    put.send("/user/password", changeReq);
	    checkStatus(SC_BAD_REQUEST);    
	  }
	 
	  @Test()
	  public void badNewPasswordTest() {
	    String badPassword = getClassProperty("badPassword");
	    login(ACCOUNT_URL, user);
	    String changeReq = createJsonString("password", badPassword);
	    changeReq = addFieldToJson(changeReq, "oldPassword", password);
	    put.send("/user/password", changeReq);
	    checkStatus(SC_BAD_REQUEST);
	  }
}
