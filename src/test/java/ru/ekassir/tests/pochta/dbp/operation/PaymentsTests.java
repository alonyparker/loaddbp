package ru.ekassir.tests.pochta.dbp.operation;

import static org.apache.http.HttpStatus.SC_OK;
import static org.apache.http.HttpStatus.SC_UNAUTHORIZED;
import ru.ekassir.dbp.helpers.JsonHelper;


import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.junit.*;
import ru.ekassir.common.load.OperationsWraps;
import ru.ekassir.dbp.helpers.TestHelper;
import ru.ekassir.dbp.models.json.base.MetadataModel;
import ru.ekassir.dbp.models.json.operation.OperationModel;
import ru.ekassir.dbp.models.json.operation.PaymentToolsModel;
import ru.ekassir.dbp.models.json.operation.TemplateModel;
import ru.ekassir.dbp.models.json.operation.TemplatesModel;
import ru.ekassir.dbp.requester.PutRequest;

public class PaymentsTests extends OperationsWraps{
  //Старт операции по договору
  @Test()
  public void startOperationByContractTest(){
    login(user);
    String serviceId = propertyReader.getProperty("beelineServiceId");
    String operationReqJson = createStartOperationJson(serviceId);
    log("Start operation.\nService: " + serviceId);
    String operationRespJson = post.send("/banking/contracts/" + getCardContractId() + "/operations", operationReqJson);
    checkOnError(operationRespJson);
    checkStatus(SC_OK);
    OperationModel operationRespModel = JsonHelper.deserializeJson(OperationModel.class, operationRespJson);
    Assert.assertTrue("\"amount\" field exists in response.", operationRespModel.amount != null);
    Assert.assertTrue( "\"paymentTool\" field exists in response.", operationRespModel.paymentTool != null);   
    //Для amount приходит правильный editor
    Assert.assertTrue("Field with type 'money' and name 'amount' have 'amountEditor'.", getFieldEditorFromMetadata(operationRespModel.__schema, "amount").equals("amountEditor"));
    //Для paymentTool приходит правильный editor
    Assert.assertTrue("Field with type 'paymentTool' have required editor 'paymentTools'.", getFieldEditorFromMetadata(operationRespModel.__schema, "paymentTool").equals("paymentTools"));    
  }
    
  // Старт шаблона операции по договору
  @Test()
  public void startOperationViaTemplateByContractTest(){
    login(user);
    String contractId = getCardContractId();
    String templates = get.send(OPERATIONS_URL + "templates/");
   checkStatus(SC_OK);
    TemplatesModel templatesModel = JsonHelper.deserializeJson(TemplatesModel.class, templates);
    Assert.assertFalse("User have no templates.",templatesModel.items.length == 0);
    String templateId = templatesModel.items[0].id;
    String operationReqJson = createStartOperationByTemplateJson(templateId);
    String operationRespJson = post.send("/banking/contracts/" + contractId + "/operations", operationReqJson);
    checkOnError(operationRespJson);
    checkStatus(SC_OK);
    OperationModel operationRespModel = JsonHelper.deserializeJson(OperationModel.class, operationRespJson);
    Assert.assertTrue("\"amount\" field exists in response.", operationRespModel.amount != null);
    Assert.assertTrue("\"paymentTool\" field exists in response.", operationRespModel.paymentTool != null);    
    //Для amount приходит правильный editor
    Assert.assertTrue(getFieldEditorFromMetadata(operationRespModel.__schema, "amount").equals("amountEditor"));
    //Для paymentTool приходит правильный editor
    Assert.assertTrue(getFieldEditorFromMetadata(operationRespModel.__schema, "paymentTool").equals("paymentTools"));  
  }
  
  //Повтор операции по договору
  @Test()
  public void repeatOperationByContractTest()  {
    login(user);
    String contractId = getCardContractId();
    String eventId = getEventIdToRepeatOperation();
    String operationReqJson = createStartRepeatOperationJson(eventId);
    String operationRespJson = post.send("/banking/contracts/" + contractId + "/operations", operationReqJson);
    checkStatus(SC_OK);
    OperationModel operationRespModel = JsonHelper.deserializeJson(OperationModel.class, operationRespJson);
    Assert.assertTrue("\"amount\" field exists in response.", operationRespModel.amount != null);
    Assert.assertTrue("\"paymentTool\" field exists in response.", operationRespModel.paymentTool != null);    
    //Для amount приходит правильный editor
    Assert.assertTrue("Field with type 'money' and name 'amount' have 'amountEditor'.", getFieldEditorFromMetadata(operationRespModel.__schema, "amount").equals("amountEditor"));
    //Для paymentTool приходит правильный editor
    Assert.assertTrue("Field with type 'paymentTool' have required editor 'paymentTools'.", getFieldEditorFromMetadata(operationRespModel.__schema, "paymentTool").equals("paymentTools"));
  }
  
  //Расчет комиссии от суммы больше максимальной суммы операции
  @Test()
  public void checkErrorIfAmountMoreThanPaymentToolOpMaxAmount()  {
    String serviceId = propertyReader.getProperty("beelineServiceId");
    login("/account", user);
    String operationReqJson = createStartOperationJson(serviceId);
    log("Open service: " + serviceId);
    String operationRespJson = post.send(OPERATIONS_URL, operationReqJson);
    checkOnError(operationRespJson);
    checkStatus(SC_OK);
    OperationModel operationRespModel = JsonHelper.deserializeJson(OperationModel.class, operationRespJson);
    String operationId = operationRespModel.id;    
    log("Set amount more then operation max amount");
    Assert.assertFalse("No \"operationMaxAmount\" field in response.", operationRespModel.paymentTool.operationMaxAmount == null);
    Assert.assertTrue("Currency code in \"operationMaxAmount\" correct.", operationRespModel.paymentTool.operationMaxAmount.currencyCode.equals("RUB"));
    Assert.assertTrue("Currency code in \"operationMinAmount\" correct.", operationRespModel.paymentTool.operationMinAmount.currencyCode.equals("RUB"));
    operationRespJson = setAmountResponseJson(operationId, operationRespJson, operationRespModel.paymentTool.operationMaxAmount.amount + 1);        
    OperationModel operationAfterPut = JsonHelper.deserializeJson(OperationModel.class, operationRespJson);
    Assert.assertFalse("No error message.", operationAfterPut.__error == null);
    Assert.assertTrue( "Error text correct.", operationAfterPut.__error.message.contains("Сумма операции больше максимальной суммы платежа"));
  }
   
  //Расчет комиссии от суммы больше максимальной суммы операции
  @Test()
  public void checkErrorIfAmountMoreThanPaymentToolTotalAbsentOpMaxAmount()  {
    String serviceId = propertyReader.getProperty("beelineServiceId");    
    login(user);    
    log("Open service: " + serviceId);
    String operationReqJson = createStartOperationJson(serviceId);
    String operationRespJson = post.send(OPERATIONS_URL, operationReqJson);
    checkOnError(operationRespJson);
    checkStatus(SC_OK);
    OperationModel operationRespModel = JsonHelper.deserializeJson(OperationModel.class, operationRespJson);
    operationRespJson = removeFieldFromJson(operationRespJson, "total");    
    String operationId = operationRespModel.id;    
    log("Set amount more then paymentTool operation max amount");
    Assert.assertFalse(operationRespModel.paymentTool.operationMaxAmount == null);
    Assert.assertTrue(operationRespModel.paymentTool.operationMaxAmount.currencyCode.equals("RUB"));
    Assert.assertTrue(operationRespModel.paymentTool.operationMinAmount.currencyCode.equals("RUB"));
    operationRespJson = setAmountResponseJson(operationId, operationRespJson, operationRespModel.paymentTool.operationMaxAmount.amount + 1);        
    OperationModel operationAfterPut = JsonHelper.deserializeJson(OperationModel.class, operationRespJson);
    Assert.assertFalse(operationAfterPut.__error == null);
    Assert.assertTrue(operationAfterPut.__error.message.contains("Сумма операции больше максимальной суммы платежа"));
  }
  
  // Расчет комиссии от суммы меньше минимальной суммы операции
  @Test()
  public void checkErrorIfAmountLessThanPaymentToolOpMinAmount() {
    String serviceId =propertyReader.getProperty("beelineServiceId");    
    login(user);    
    log("Open service: " + serviceId);
    String operationReqJson = createStartOperationJson(serviceId);
    String operationRespJson = post.send(OPERATIONS_URL, operationReqJson);
    checkOnError(operationRespJson);
    checkStatus(SC_OK);
    OperationModel operationRespModel = JsonHelper.deserializeJson(OperationModel.class, operationRespJson);
    String operationId = operationRespModel.id;    
    log("Set amount less then operation min amount");
    Assert.assertFalse(operationRespModel.paymentTool.operationMinAmount == null);
    Assert.assertTrue(operationRespModel.paymentTool.operationMaxAmount.currencyCode.equals("RUB"));
    Assert.assertTrue(operationRespModel.paymentTool.operationMinAmount.currencyCode.equals("RUB"));
    operationRespJson = setAmountResponseJson(operationId, operationRespJson, operationRespModel.paymentTool.operationMinAmount.amount - 1);
    OperationModel operationAfterPut = JsonHelper.deserializeJson(OperationModel.class, operationRespJson);
    Assert.assertFalse(operationAfterPut.__error == null);
    Assert.assertTrue(operationAfterPut.__error.message.contains("Сумма операции меньше минимальной суммы платежа "));
  }
  
  
  
  // Расчет комиссии от нулевой суммы
  @Test()
  public void checkErrorIfZeroAmount()  {
    String serviceId =propertyReader.getProperty("beelineServiceId"); 
    login(user);    
    log("Open service: " + serviceId);
    String operationReqJson = createStartOperationJson(serviceId);
    String operationRespJson = post.send(OPERATIONS_URL, operationReqJson);
    checkOnError(operationRespJson);
    checkStatus(SC_OK);
    OperationModel operationRespModel = JsonHelper.deserializeJson(OperationModel.class, operationRespJson);
    String operationId = operationRespModel.id;    
    log("Set amount \"0\".");
    operationRespJson = setAmountResponseJson(operationId, operationRespJson, 0L);
    OperationModel operationAfterPut = JsonHelper.deserializeJson(OperationModel.class, operationRespJson);
    Assert.assertFalse(operationAfterPut.__error == null);
    Assert.assertTrue(operationAfterPut.__error.message.contains("Сумма операции меньше минимальной суммы платежа "));
  }

  //Проведение платежа с созданием шаблона, его изменением, оплате по измененному шаблону и его удалением
  @Test()
  public void paymentWithTemplateCreationChangingDeletingTest()  {
    String serviceId =propertyReader.getProperty("beelineServiceId");
    String account = getTestProperty("account");
    String newDisplayName = getTestProperty("newDisplayName");
    Long newAmount = Long.valueOf(getClassProperty("newAmount"));
    Long amount = Long.valueOf(getClassProperty("amount"));
    
    log("Get list of templates before operation");
    String templatesFirstTimeRespJson = login(OPERATIONS_URL +"/templates", user);    
    TemplatesModel templatesModelFirstTime = JsonHelper.deserializeJson(TemplatesModel.class, templatesFirstTimeRespJson);
    Integer templatesCount = templatesModelFirstTime.items.length;
    log("Delete all templates if exists");
    if(templatesCount > 0) {
      for(int i = 0; i < templatesCount; ++i) {
        TemplateModel tm = templatesModelFirstTime.items[i];
        String id = tm.id;
        delete.send("/banking/operations/templates/" + id); 
        //checkStatus(SC_OK);
      }
    }
    //Проведение платежа
    log("Open service: " + serviceId);
    String operationReqJson = createStartOperationJson(serviceId);
    String operationRespJson = post.send(OPERATIONS_URL, operationReqJson);
    checkOnError(operationRespJson);
    checkStatus(SC_OK);
    OperationModel operationRespModel = JsonHelper.deserializeJson(OperationModel.class, operationRespJson);
    String operationId = operationRespModel.id;    
    
    log("Set amount: " + amount);
    String setAmountRespJson = setAmountResponseJson(operationId, operationRespJson, amount);
    setAmountRespJson = deleteSchema(setAmountRespJson);
    
    log("Set account: " + account);
    String setAccountJson = addAccountToOperationJson(setAmountRespJson, account);
    setAccountJson = addFieldToJson(setAccountJson, "__saveTemplate", true);
    
    log("Send confirmation=next");
    String operationNextReqJson = addConfirmationToOperationJson(setAccountJson, NEXT);    
    String operationNextRespJson = put.send(OPERATIONS_URL + operationId, operationNextReqJson);
    checkOnError(operationNextRespJson);
    checkStatus(SC_UNAUTHORIZED);
    
    log("Send confirmation=commit wuth OTP");
    String commitOperationReqJson = addConfirmationToOperationJson(operationNextRespJson, COMMIT);
    commitOperationReqJson = deleteSchema(commitOperationReqJson);  
    String enterOtpRespJson = put.setAuthorizationHeader(getAuthHeaderWithOtp()).send(OPERATIONS_URL + operationId, commitOperationReqJson);
    checkOnError(enterOtpRespJson);
    checkStatus(SC_OK);
    OperationModel enterOtpRespModel = JsonHelper.deserializeJson(OperationModel.class, enterOtpRespJson);
    log("OperationModel enterOtpRespModel = JsonHelper.deserializeJson(OperationModel.class, enterOtpRespJson)");
    
    log("Remember crested template id");
    Assert.assertFalse("No \"__template\" field in operation response.", enterOtpRespModel.__template == null);
    log("Assert.assertFalse enterOtpRespModel.__template == null");
    String templateId = enterOtpRespModel.__template.id;
    log("String templateId = enterOtpRespModel.__template.id;");
    Assert.assertNotEquals(enterOtpRespModel.status.id, "new"); 
    log("Assert.assertNotEquals(enterOtpRespModel.status.id, new);");
    
    //Проверка что шаблон создался
    log("Get list of templates after operation to check is new template created.");
    String templatesAfterOperationRespJson = get.send(OPERATIONS_URL + "templates/"); 
    checkStatus(SC_OK);
    TemplatesModel templatesAfterOperationRespModel = JsonHelper.deserializeJson(TemplatesModel.class, templatesAfterOperationRespJson);
    int createdTemplateIndex = -1;
    for(int i = 0; i < templatesAfterOperationRespModel.items.length; ++i) {
      if(templatesAfterOperationRespModel.items[i].id.equals(templateId)) {
        createdTemplateIndex = i;
      }
    }
    Assert.assertTrue("Template created. ", createdTemplateIndex != -1);
    Assert.assertTrue(templatesAfterOperationRespModel.items[createdTemplateIndex].isFavorite == false);
    Assert.assertTrue("Created template amount equals amount during operation: " + amount, templatesAfterOperationRespModel.items[createdTemplateIndex].amount.amount == Long.valueOf(amount));
    
    //Изменение шаблона
    templatesAfterOperationRespModel.items[createdTemplateIndex].amount.amount = newAmount;
    templatesAfterOperationRespModel.items[createdTemplateIndex].name = newDisplayName;
    templatesAfterOperationRespModel.items[createdTemplateIndex].isFavorite = true;
    String changeTemplateJson = templatesAfterOperationRespModel.items[createdTemplateIndex].toString();
    log("Change template: \n 1. amount: " + amount + "to " + newAmount);
    log("2. Name to " + newDisplayName);
    log("3. Favorite flag to true");
    String changeTemplateResp = put.send(OPERATIONS_URL + "templates/" + templateId, changeTemplateJson);
    checkOnError(changeTemplateResp);
    checkStatus(SC_OK);
        
    //Проверка изменений в шаблоне
    log("Check template changes");
    String templatesAfterChangeRespJson = get.send(OPERATIONS_URL + "templates/"); 
    //checkStatus(SC_OK);
    TemplatesModel templatesAfterChangeRespModel = JsonHelper.deserializeJson(TemplatesModel.class, templatesAfterChangeRespJson);
    Assert.assertTrue("Amount changes in template applyed.", templatesAfterChangeRespModel.items[createdTemplateIndex].amount.amount.equals(newAmount));
    Assert.assertEquals("Name changes in template applyed.", templatesAfterChangeRespModel.items[createdTemplateIndex].name, newDisplayName);
    Assert.assertTrue("Favorite field changes in template applyed.", templatesAfterChangeRespModel.items[createdTemplateIndex].isFavorite == true);
    
    //Проведение платежа по шаблону
    log("Start operation from template");
    String templateOperationReqJson = createStartOperationByTemplateJson(templatesAfterChangeRespModel.items[createdTemplateIndex].id);
    String templateOperationRespJson = post.send(OPERATIONS_URL, templateOperationReqJson);
    checkOnError(templateOperationRespJson);
    checkStatus(SC_OK);
    OperationModel templateOperationRespModel = JsonHelper.deserializeJson(OperationModel.class, templateOperationRespJson);
    operationId = templateOperationRespModel.id;
    templateOperationReqJson = addConfirmationToOperationJson(templateOperationRespJson, NEXT);
    templateOperationReqJson= deleteSchema(templateOperationReqJson);
    
    log("Send confirmation=next");
    String comfirmationNextRespJson = put.send(OPERATIONS_URL + operationId, templateOperationReqJson); 
    checkOnError(comfirmationNextRespJson);
    checkStatus(SC_UNAUTHORIZED);
    JsonHelper.deserializeJson(OperationModel.class, comfirmationNextRespJson);
    String commitTemplateOperationReqJson = addConfirmationToOperationJson(comfirmationNextRespJson, COMMIT);
    commitTemplateOperationReqJson= deleteSchema(commitTemplateOperationReqJson);
    
    log("Send confirmation=commit wuth OTP");
    String enterOtpTemplateOperationRespJson = put.setAuthorizationHeader(getAuthHeaderWithOtp()).send(OPERATIONS_URL + operationId, commitTemplateOperationReqJson);
    checkOnError(enterOtpTemplateOperationRespJson);
    checkStatus(SC_OK);
    OperationModel enterOtpTemplateOperationRespModel = JsonHelper.deserializeJson(OperationModel.class, enterOtpTemplateOperationRespJson);
    Assert.assertNotEquals(enterOtpTemplateOperationRespModel.status.id, "new", "Operation status not \"new\".");
    
    //Удаление шаблона
    log("Delete template");
    delete.send("/banking/operations/templates/" + templateId); 
    checkStatus(SC_OK);
    String templatesAfterDeleteRespJson = get.send(OPERATIONS_URL + "templates/"); 
    checkStatus(SC_OK);
    TemplatesModel templatesAfterDeleteRespModel = JsonHelper.deserializeJson(TemplatesModel.class, templatesAfterDeleteRespJson);
    
    log("Check that template was removed");
    for(int i = 0; templatesAfterDeleteRespModel.items.length < i; ++i) {
      Assert.assertNotEquals(templatesAfterDeleteRespModel.items[i].id, templateId);
    }
  }
  
//Проведение платежа без шаблона
  @Test()
  public void paymentWithoutTemplateTest()  {
    String serviceId =propertyReader.getProperty("beelineServiceId");
    String account = getTestProperty("account");
    String newDisplayName = getTestProperty("newDisplayName");
    Long newAmount = Long.valueOf(getClassProperty("newAmount"));
    Long amount = Long.valueOf(getClassProperty("amount"));
    
    log("Get list of templates before operation");
    String templatesFirstTimeRespJson = login(OPERATIONS_URL +"/templates", user);    
    //Проведение платежа
    log("Open service: " + serviceId);
    String operationReqJson = createStartOperationJson(serviceId);
    String operationRespJson = post.send(OPERATIONS_URL, operationReqJson);
    checkOnError(operationRespJson);
    checkStatus(SC_OK);
    OperationModel operationRespModel = JsonHelper.deserializeJson(OperationModel.class, operationRespJson);
    String operationId = operationRespModel.id;    
    
    log("Set amount: " + amount);
    String setAmountRespJson = setAmountResponseJson(operationId, operationRespJson, amount);
    setAmountRespJson = deleteSchema(setAmountRespJson);
    
    log("Set account: " + account);
    String setAccountJson = addAccountToOperationJson(setAmountRespJson, account);
    setAccountJson = addFieldToJson(setAccountJson, "__saveTemplate", false);
    
    log("Send confirmation=next");
    String operationNextReqJson = addConfirmationToOperationJson(setAccountJson, NEXT);    
    String operationNextRespJson = put.send(OPERATIONS_URL + operationId, operationNextReqJson);
    checkOnError(operationNextRespJson);
    checkStatus(SC_UNAUTHORIZED);
    
    log("Send confirmation=commit wuth OTP");
    String commitOperationReqJson = addConfirmationToOperationJson(operationNextRespJson, COMMIT);
    commitOperationReqJson = deleteSchema(commitOperationReqJson);  
    String enterOtpRespJson = put.setAuthorizationHeader(getAuthHeaderWithOtp()).send(OPERATIONS_URL + operationId, commitOperationReqJson);
    checkOnError(enterOtpRespJson);
    checkStatus(SC_OK);
    OperationModel enterOtpRespModel = JsonHelper.deserializeJson(OperationModel.class, enterOtpRespJson);
    log("OperationModel enterOtpRespModel = JsonHelper.deserializeJson(OperationModel.class, enterOtpRespJson)");
    
    log("Remember crested template id");
    Assert.assertTrue("Exist \"__template\" field in operation response.", enterOtpRespModel.__template == null);
  }
//Проведение платежа без шаблона
  @Test()
  public void paymentWithTemplateTest()  {
    String serviceId =propertyReader.getProperty("beelineServiceId");
    String account = getTestProperty("account");
    String newDisplayName = getTestProperty("newDisplayName");
    Long newAmount = Long.valueOf(getClassProperty("newAmount"));
    Long amount = Long.valueOf(getClassProperty("amount"));
    
    log("Get list of templates before operation");
    String templatesFirstTimeRespJson = login(OPERATIONS_URL +"/templates", user);    
    //Проведение платежа
    log("Open service: " + serviceId);
    String operationReqJson = createStartOperationJson(serviceId);
    String operationRespJson = post.send(OPERATIONS_URL, operationReqJson);
    checkOnError(operationRespJson);
    checkStatus(SC_OK);
    OperationModel operationRespModel = JsonHelper.deserializeJson(OperationModel.class, operationRespJson);
    String operationId = operationRespModel.id;    
    
    log("Set amount: " + amount);
    String setAmountRespJson = setAmountResponseJson(operationId, operationRespJson, amount);
    setAmountRespJson = deleteSchema(setAmountRespJson);
    
    log("Set account: " + account);
    String setAccountJson = addAccountToOperationJson(setAmountRespJson, account);
    setAccountJson = addFieldToJson(setAccountJson, "__saveTemplate", true);
    
    log("Send confirmation=next");
    String operationNextReqJson = addConfirmationToOperationJson(setAccountJson, NEXT);    
    String operationNextRespJson = put.send(OPERATIONS_URL + operationId, operationNextReqJson);
    checkOnError(operationNextRespJson);
    checkStatus(SC_UNAUTHORIZED);
    
    log("Send confirmation=commit wuth OTP");
    String commitOperationReqJson = addConfirmationToOperationJson(operationNextRespJson, COMMIT);
    commitOperationReqJson = deleteSchema(commitOperationReqJson);  
    String enterOtpRespJson = put.setAuthorizationHeader(getAuthHeaderWithOtp()).send(OPERATIONS_URL + operationId, commitOperationReqJson);
    checkOnError(enterOtpRespJson);
    checkStatus(SC_OK);
    OperationModel enterOtpRespModel = JsonHelper.deserializeJson(OperationModel.class, enterOtpRespJson);
    log("OperationModel enterOtpRespModel = JsonHelper.deserializeJson(OperationModel.class, enterOtpRespJson)");
    
    log("Remember crested template id");
    Assert.assertFalse("No \"__template\" field in operation response.", enterOtpRespModel.__template == null);
  }
  
  //Повтор платежа из ленты
  @Test()
  public void repeatPaymentFromTimelineTest()  {
    login( "/account",user); 
    //Make operation from timeline
    log("Send POST with sourceEventId to start operation from timeline");
    String eventId = getEventIdToRepeatOperation();
    String repeatOperationReqJson = createStartRepeatOperationJson(eventId);    
    String repeatOperationRespJson = post.send( OPERATIONS_URL, repeatOperationReqJson);
    checkOnError(repeatOperationRespJson);
    checkStatus(SC_OK);
    OperationModel repeatOperationRespModel = JsonHelper.deserializeJson(OperationModel.class, repeatOperationRespJson);
    Assert.assertTrue("By default __saveTemplate=false when starting operation from timeline", repeatOperationRespModel.__saveTemplate == false);
    Assert.assertTrue("Fee field is not NULL when starting operation from timeline", repeatOperationRespModel.fee != null);
    Assert.assertTrue("Amount field is not NULL when starting operation from timeline", repeatOperationRespModel.amount != null);
    Assert.assertTrue("Amount more then zero when starting operation from timeline", repeatOperationRespModel.amount.amount > 0);
    Assert.assertTrue("Total field is not NULL when starting operation from timeline", repeatOperationRespModel.total != null);
    Assert.assertTrue("Total more or equal to amount", repeatOperationRespModel.total.amount >= repeatOperationRespModel.amount.amount);
    String operationId = repeatOperationRespModel.id;
    
    log("Send confirmation=next");
    repeatOperationReqJson = addConfirmationToOperationJson(repeatOperationRespJson, NEXT);
    repeatOperationReqJson = deleteSchema(repeatOperationReqJson);    
    String repeatOperationNextRespJson = put.send(OPERATIONS_URL + operationId, repeatOperationReqJson);
    checkOnError(repeatOperationNextRespJson);
    checkStatus(SC_UNAUTHORIZED);
    
    log("Send confirmation=commit with OTP");
    String repeatOperationCommitReqJson = addConfirmationToOperationJson(repeatOperationNextRespJson, COMMIT);
    repeatOperationCommitReqJson = deleteSchema(repeatOperationCommitReqJson);
    String enterOtpRespJson = put.setAuthorizationHeader(getAuthHeaderWithOtp()).send(OPERATIONS_URL + operationId, repeatOperationCommitReqJson);
    checkOnError(enterOtpRespJson);
    checkStatus(SC_OK);
    OperationModel enterOtpRespModel = JsonHelper.deserializeJson(OperationModel.class, enterOtpRespJson);
    Assert.assertNotEquals(enterOtpRespModel.status.id, "new", "Operation status not equals 'new' in final response");
  }
 
  //Make operation where commission calculates from total
  @Test()
  public void calculateCommissionFromTotalTest()  {    
    login("/account", user);
    String serviceId =propertyReader.getProperty("transferToAnotherBankServiceId");
    String bik = getTestProperty("bik");
    String account = getTestProperty("account");
    String lastname = getTestProperty("lastname");
    String firstname = getTestProperty("firstname");
    String secondname = getTestProperty("secondname");
    Long amount = Long.valueOf(getTestProperty("amount"));
    String operationReqJson = createStartOperationJson(serviceId);
   
    log("Open service: " + serviceId);
    String operationRespJson = post.send(OPERATIONS_URL, operationReqJson);
    //checkStatus(SC_OK);
    checkOnError(operationRespJson);
    OperationModel operationRespModel = JsonHelper.deserializeJson(OperationModel.class, operationRespJson);
    Assert.assertTrue( "feeFromTotalAllowed=true.", operationRespModel.feeFromTotalAllowed == true);
    Assert.assertTrue( "feeFromTotal flag false by default.", operationRespModel.feeFromTotal == false);
    String operationId = operationRespModel.id;
    
    log("Enter BIK: " + bik);
    operationRespJson = enterBik(OPERATIONS_URL + operationId, operationRespJson, bik);
    checkOnError(operationRespJson);
    
    log("Enter account: " + account);
    log("Enter lastname, firstname, secondname");    
    operationRespJson = addFieldToOperationJson(operationRespJson, "account", account);
    operationRespJson = addFieldToOperationJson(operationRespJson, "lastname", lastname);
    operationRespJson = addFieldToOperationJson(operationRespJson, "firstname", firstname);
    operationRespJson = addFieldToOperationJson(operationRespJson, "secondname", secondname);
    
    log("Set amount: " + amount);
    operationRespJson = setAmountResponseJson(operationId, operationRespJson, amount);
    checkOnError(operationRespJson);    
    OperationModel commissionRespModel = JsonHelper.deserializeJson(OperationModel.class, operationRespJson);    
    Assert.assertTrue("Amount not null after calculating commission.", commissionRespModel.amount != null);
    Assert.assertTrue(commissionRespModel.fee != null);
    Assert.assertTrue(commissionRespModel.total != null);
    Assert.assertTrue( "Total=fee+amount after calculating commission.", (commissionRespModel.fee.amount + commissionRespModel.amount.amount) == commissionRespModel.total.amount );
    
    log("Set feeFromTotal=true");    
    operationRespJson = setFeeFromTotalValue(OPERATIONS_URL + operationId, operationRespJson, true);
    checkOnError(operationRespJson);
    OperationModel feeFromTotalCalculateModel = JsonHelper.deserializeJson(OperationModel.class, operationRespJson);
    Assert.assertTrue(feeFromTotalCalculateModel.amount != null);
    Assert.assertTrue(feeFromTotalCalculateModel.fee != null);
    Assert.assertTrue(feeFromTotalCalculateModel.total != null);    
    Assert.assertEquals(feeFromTotalCalculateModel.total.amount, amount);
    Assert.assertTrue(feeFromTotalCalculateModel.fee.amount + feeFromTotalCalculateModel.amount.amount == feeFromTotalCalculateModel.total.amount );
    Assert.assertTrue(feeFromTotalCalculateModel.feeFromTotalAllowed == true);
    Assert.assertTrue(feeFromTotalCalculateModel.feeFromTotal == true);
    
    log("Send confirmation=next");
    String operationNextReqJson = addConfirmationToOperationJson(operationRespJson, NEXT);
    operationNextReqJson = deleteSchema(operationNextReqJson);
    String operationNextRespJson = put.send(OPERATIONS_URL + operationId, operationNextReqJson);
    checkOnError(operationNextRespJson);
    checkStatus(SC_UNAUTHORIZED);
    
    log("Send confirmation=commit with OTP");
    String operationCommitReqJson = addConfirmationToOperationJson(operationNextRespJson, COMMIT);
    operationCommitReqJson = deleteSchema(operationCommitReqJson);    
    String enterOtpRespJson = put.setAuthorizationHeader(getAuthHeaderWithOtp()).send(OPERATIONS_URL + operationId, operationCommitReqJson);
    checkOnError(enterOtpRespJson);
    checkStatus(SC_OK);
    OperationModel enterOtpRespModel = JsonHelper.deserializeJson(OperationModel.class, enterOtpRespJson);
    Assert.assertNotEquals(enterOtpRespModel.status.id, "new");
    Assert.assertTrue("feeFromTotalallowed flag is true", enterOtpRespModel.feeFromTotalAllowed == true);
    Assert.assertTrue(enterOtpRespModel.feeFromTotal == true);
    Assert.assertTrue(enterOtpRespModel.fee.amount + enterOtpRespModel.amount.amount == enterOtpRespModel.total.amount);
  }
 
  @Test()
  public void transferBetweenCardsTest()  {
    String transferBetweenAccountsId =propertyReader.getProperty("transferBetweenAccountsId");
    Long amount = Long.valueOf(getClassProperty("amount"));
    String paymentToolName = null, payAccName = null;    
    
    String paymentTools = login("/banking/operations/paymentTools?serviceId=" + transferBetweenAccountsId, user);
    checkCorrectLogin(paymentTools);
    Assert.assertTrue("At last two payment tools present.", JsonHelper.deserializeJson(PaymentToolsModel.class, paymentTools).items.length > 1);
    
    //Получить имя ПИ для установки в поле "откуда"
    PaymentToolsModel paymentToolsModel = JsonHelper.deserializeJson(PaymentToolsModel.class, paymentTools);
    paymentToolName = paymentToolsModel.items[1].displayName;
    payAccName = paymentToolsModel.items[0].displayName;
    
    //Make transfer between cards
    log("Open service: " + transferBetweenAccountsId);
    String operationReqJson = createStartOperationJson(transferBetweenAccountsId);
    String operationRespJson = post.send("/banking/operations", operationReqJson); 
    checkOnError(operationRespJson);
    checkStatus(SC_OK);
    OperationModel operationRespModel = JsonHelper.deserializeJson(OperationModel.class, operationRespJson);  
    String operationId = operationRespModel.id;
    
    log("Set amount " + amount + " and calculate commission");
    operationRespJson = setAmountResponseJson( operationId, operationRespJson, amount);
    //operationRespJson = deleteSchema(operationRespJson);
    
    log("Set paymentTool: " + paymentToolName + " and calculate commission");
    operationRespJson = setPaymentToolResponseJson(operationId, operationRespJson, paymentToolName);
    
    log("Set paymentToolAccount: " + payAccName + " and calculate commission");
    operationRespJson = setPaymentToolAccResponseJson("/banking/operations/"+operationId, operationRespJson, payAccName);
    
    log("Send confirmation=next");
    String operationNextReqJson = addConfirmationToOperationJson(operationRespJson, "next");
    String operationNextRespJson = put.send("/banking/operations/" + operationId, operationNextReqJson);
    checkOnError(operationNextRespJson);
    checkStatus(SC_UNAUTHORIZED);
    
    log("Send confirmation=commit with OTP");
    String operationCommitReqJson = addConfirmationToOperationJson(operationNextRespJson, "commit");
    operationCommitReqJson = deleteSchema(operationCommitReqJson);
    
    String enterOtpRespJson = put.setAuthorizationHeader(getAuthHeaderWithOtp()).send("/banking/operations/" + operationId, operationCommitReqJson);
    checkOnError(enterOtpRespJson);
    checkStatus(SC_OK);
    OperationModel enterOtpRespModel = JsonHelper.deserializeJson(OperationModel.class, enterOtpRespJson);
    Assert.assertNotEquals(enterOtpRespModel.status.id, "new", "Operation status not \"new\"");
  }
  
  @Test()
  public void loanCalculatorTest()  {
    String serviceId =propertyReader.getProperty("creditOrderId");
    login("/account",user); 
    JsonObject loanCalcSchema = new JsonObject();
    String operationReqJson = createStartOperationJson(serviceId);
    String operationRespJson = post.send(OPERATIONS_URL, operationReqJson);
    checkOnError(operationRespJson);
    checkStatus(SC_OK);
    JsonHelper.deserializeJson(OperationModel.class, operationRespJson);
    loanCalcSchema = getFieldMetadataFromSchema(operationRespJson, "Кредитный калькулятор");
    try{
      JsonObject editor = (JsonObject) loanCalcSchema.get("editors").getAsJsonArray().get(0);
      Assert.assertTrue(editor.get("id").getAsString().equals("loanCalculator"));
    }
    catch(NullPointerException e) {
      Assert.fail("No editors in loan calculator metadata");
    }
  }
  
  @Test()
  public void transferToAnotherClientByContractNumberTest()  {
    String transferToAnotherBankClientServiceId =propertyReader.getProperty("transferToAnotherBankClientServiceId");
    String contractToTransfer = getTestProperty("contract");
    Long amount = Long.valueOf(getClassProperty("amount"));
    
    String paymentTools = login("/banking/operations/paymentTools", user); 
    checkCorrectLogin(paymentTools);
    Assert.assertTrue(JsonHelper.deserializeJson(PaymentToolsModel.class, paymentTools).items.length > 0);
    
    log("Open service: " + transferToAnotherBankClientServiceId);
    String operationReqJson = createStartOperationJson(transferToAnotherBankClientServiceId);
    String operationRespJson = post.send(OPERATIONS_URL, operationReqJson);
    checkOnError(operationRespJson);
    checkStatus(SC_OK);
    OperationModel operationRespModel = JsonHelper.deserializeJson(OperationModel.class, operationRespJson);
    String operationId = operationRespModel.id;    
    
    log("Choose 'По номеру договора'");
    operationRespJson = choosePaymentTypeForTransfer(operationRespJson, "\"По номеру договора\"");
    
    log("Enter contract number: " + contractToTransfer);
    operationRespJson = enterAccountForTransfer(OPERATIONS_URL + operationId, operationRespJson, contractToTransfer);
    checkOnError(operationRespJson);
    
    log("Set amount: " + amount + " and calculate commission");
    operationRespJson = setAmountResponseJson(operationId, operationRespJson, amount);
    checkOnError(operationRespJson);
    
    log("Send confirmation=next");
    String operationNextReqJson = addConfirmationToOperationJson(operationRespJson, NEXT);     
    String operationNextRespJson = put.send(OPERATIONS_URL + operationId, operationNextReqJson);
    checkOnError(operationNextRespJson);
    checkStatus(SC_UNAUTHORIZED);
    
    log("Send confirmation=commit with OTP");
    String operationCommitReqJson = addConfirmationToOperationJson(operationNextRespJson, COMMIT); 
    operationCommitReqJson = deleteSchema(operationCommitReqJson);
    
    String enterOtpRespJson = put.setAuthorizationHeader(getAuthHeaderWithOtp()).send(OPERATIONS_URL + operationId, operationCommitReqJson);
    checkOnError(enterOtpRespJson);
    checkStatus(SC_OK);
    OperationModel enterOtpRespModel = JsonHelper.deserializeJson(OperationModel.class, enterOtpRespJson);
    Assert.assertNotEquals(enterOtpRespModel.status.id, "new", "Operation status not 'new' in final response");
  }
 
  @Test()
  public void transferToAnotherClientByAccountNumberTest() {
    String serviceId =propertyReader.getProperty("transferToAnotherBankClientServiceId");
    String accountToTransfer = getTestProperty("account");
    Long amount = Long.parseLong(getClassProperty("amount"));
    String paymentTools = login("/banking/operations/paymentTools",user);    
    Assert.assertTrue("At last one payment tool present.", JsonHelper.deserializeJson(PaymentToolsModel.class, paymentTools).items.length > 0);
    
    log("Open service: " + serviceId);
    String operationReqJson = createStartOperationJson(serviceId);
    String operationRespJson = post.send(OPERATIONS_URL, operationReqJson);
    checkOnError(operationRespJson);
    checkStatus(SC_OK);
    OperationModel operationRespModel = JsonHelper.deserializeJson(OperationModel.class, operationRespJson);
    String operationId = operationRespModel.id;
    
    log("Choose 'По номеру счета'");
    operationRespJson = choosePaymentTypeForTransfer(operationRespJson, "\"По номеру счета\"");
    
    log("Enter account number: " + accountToTransfer);
    operationRespJson = enterAccountForTransfer(OPERATIONS_URL + operationId, operationRespJson, accountToTransfer);
    checkOnError(operationRespJson);    
    
    log("Set amount: " + amount + " and calculate commission");
    operationRespJson = setAmountResponseJson(operationId, operationRespJson, amount);
    checkOnError(operationRespJson);
    
    log("Send confirmation=next");
    String operationNextReqJson = addConfirmationToOperationJson(operationRespJson, NEXT);
    String operationNextRespJson = put.send(OPERATIONS_URL + operationId, operationNextReqJson);
    checkOnError(operationNextRespJson);
    checkStatus(SC_UNAUTHORIZED);
          
    log("Send confirmation=commit with OTP");
    String operationCommitReqJson = addConfirmationToOperationJson(operationNextRespJson, COMMIT); 
    operationCommitReqJson = deleteSchema(operationCommitReqJson);
   
    String enterOtpRespJson = put.setAuthorizationHeader(getAuthHeaderWithOtp()).send(OPERATIONS_URL + operationId, operationCommitReqJson);
    checkOnError(enterOtpRespJson);
    checkStatus(SC_OK);
    OperationModel enterOtpRespModel = JsonHelper.deserializeJson(OperationModel.class, enterOtpRespJson);
    Assert.assertNotEquals(enterOtpRespModel.status.id, "new", "Operation status not 'new' in final response");
  }
  
  @Test()
  // TODO:Нужен правильный номер телефона
  public void transferToAnotherClientByPhoneNumberTest()  {
    String serviceId =propertyReader.getProperty("transferToAnotherBankClientServiceId");
    String phoneToTransfer = getTestProperty("phone");
    Long amount = Long.valueOf(getClassProperty("amount"));
    
    String paymentTools = login("/banking/operations/paymentTools", user);    
    Assert.assertTrue("At last one payment tool present.", JsonHelper.deserializeJson(PaymentToolsModel.class, paymentTools).items.length > 0);
    
    log("Open service: " + serviceId);
    String operationReqJson = createStartOperationJson(serviceId);
    String operationRespJson = post.send(OPERATIONS_URL, operationReqJson);
    checkOnError(operationRespJson);
    checkStatus(SC_OK);
    OperationModel operationRespModel = JsonHelper.deserializeJson(OperationModel.class, operationRespJson);
    String operationId = operationRespModel.id;
    
    log("Choose 'По номеру телефона'");
    operationRespJson = choosePaymentTypeForTransfer(operationRespJson, "\"По номеру телефона\"");
    
    log("Enter phone number: " + phoneToTransfer);
    operationRespJson = enterAccountForTransfer(OPERATIONS_URL + operationId, operationRespJson, phoneToTransfer);
    checkOnError(operationRespJson);
    
    log("Set amount: " + amount + " and calculate commission");
    operationRespJson = setAmountResponseJson(OPERATIONS_URL + operationId, operationRespJson, amount);
    checkOnError(operationRespJson);
    
    log("Send confirmation=next");
    String operationNextReqJson = addConfirmationToOperationJson(operationRespJson, NEXT);
    String operationNextRespJson = put.send(OPERATIONS_URL + operationId, operationNextReqJson);
    checkOnError(operationNextRespJson);
    checkStatus(SC_UNAUTHORIZED);
    
    log("Send confirmation=commit with OTP");
    String operationCommitReqJson = addConfirmationToOperationJson(operationNextRespJson, COMMIT);
    operationCommitReqJson = deleteSchema(operationCommitReqJson);
    
    String enterOtpRespJson = put.setAuthorizationHeader(getAuthHeaderWithOtp()).send(OPERATIONS_URL + operationId, operationCommitReqJson);
    checkOnError(enterOtpRespJson);
    checkStatus(SC_OK);
    OperationModel enterOtpRespModel = JsonHelper.deserializeJson(OperationModel.class, enterOtpRespJson);
    Assert.assertNotEquals(enterOtpRespModel.status.id, "new", "Operation status not 'new' in final response");
  }

  @Test()
  public void transferToAnotherClientByCardNumberTest()  {
    String serviceId =propertyReader.getProperty("transferToAnotherBankClientServiceId");
    String card = getTestProperty("card");
    Long amount = Long.valueOf(getClassProperty("amount"));
    
    String paymentTools = login("/banking/operations/paymentTools", user);
    Assert.assertTrue("At last one payment tool present.", JsonHelper.deserializeJson(PaymentToolsModel.class, paymentTools).items.length > 0);
    

    log("Open service: " + serviceId);
    String operationReqJson = createStartOperationJson(serviceId);
    String operationRespJson = post.send(OPERATIONS_URL, operationReqJson);
    checkOnError(operationRespJson);
    checkStatus(SC_OK);
    
    OperationModel operationRespModel = JsonHelper.deserializeJson(OperationModel.class, operationRespJson);   
    String operationId = operationRespModel.id;
    
    log("Choose 'По номеру карты'");
    operationRespJson = choosePaymentTypeForTransfer(operationRespJson, "\"По номеру карты\"");    
    
    log("Enter card number: " + card);
    operationRespJson = enterAccountForTransfer(OPERATIONS_URL + operationId, operationRespJson, card);
    checkOnError(operationRespJson);
    
    log("Set amount: " + amount + " and calculate commission");
    operationRespJson = setAmountResponseJson(operationId, operationRespJson, amount);
    checkOnError(operationRespJson);
    
    log("Send confirmation=next");
    String operationNextReqJson = addConfirmationToOperationJson(operationRespJson, NEXT);
    String operationNextRespJson = put.send(OPERATIONS_URL + operationId, operationNextReqJson);
    checkOnError(operationNextRespJson);
    checkStatus(SC_UNAUTHORIZED);
          
    log("Send confirmation=commit with OTP");
    String operationCommitReqJson = addConfirmationToOperationJson(operationNextRespJson, COMMIT); 
    operationCommitReqJson = deleteSchema(operationCommitReqJson);
    
    String enterOtpRespJson = put.setAuthorizationHeader(getAuthHeaderWithOtp()).send(OPERATIONS_URL + operationId, operationCommitReqJson);
    checkOnError(enterOtpRespJson);
    //checkStatus(SC_OK);
    OperationModel enterOtpRespModel = JsonHelper.deserializeJson(OperationModel.class, enterOtpRespJson);
    Assert.assertNotEquals(enterOtpRespModel.status.id, "new", "Operation status not 'new' in final response"); 
  }

 
 
  @Test()
  public void transferToAnotherBankTest() {
    String transferToAnotherBankServiceId =propertyReader.getProperty("transferToAnotherBankServiceId");
    String account = getTestProperty("account");
    String bik = getTestProperty("bik");
    Long amount = Long.valueOf(getClassProperty("amount"));
    String lastname = getTestProperty("lastname");
    String firstname = getTestProperty("firstname");
    String secondname = getTestProperty("secondname");
    
    String paymentTools = login("/banking/operations/paymentTools",user);    
    Assert.assertTrue("At last one payment tool present.", JsonHelper.deserializeJson(PaymentToolsModel.class, paymentTools).items.length > 0);
    
    String operationReqJson = createStartOperationJson(transferToAnotherBankServiceId);
    String operationRespJson = post.send(OPERATIONS_URL, operationReqJson);
    checkOnError(operationRespJson);
    checkStatus(SC_OK);
    
    OperationModel operationRespModel = JsonHelper.deserializeJson(OperationModel.class, operationRespJson);
    String operationId = operationRespModel.id;
    
    operationRespJson = enterBik(OPERATIONS_URL + operationId, operationRespJson, bik);
    checkOnError(operationRespJson);
    
    operationRespJson = addFieldToOperationJson(operationRespJson, "account", account);
    operationRespJson = addFieldToOperationJson(operationRespJson, "lastname", lastname);
    operationRespJson = addFieldToOperationJson(operationRespJson, "firstname", firstname);
    operationRespJson = addFieldToOperationJson(operationRespJson, "secondname", secondname);
    operationRespJson = setAmountResponseJson(operationId, operationRespJson, amount);
    checkOnError(operationRespJson);
    
    String operationNextReqJson = addConfirmationToOperationJson(operationRespJson, NEXT);
    String operationNextRespJson = put.send(OPERATIONS_URL + operationId, operationNextReqJson);
    checkOnError(operationNextRespJson);
    checkStatus(SC_UNAUTHORIZED);
    
    String operationCommitReqJson = addConfirmationToOperationJson(operationNextRespJson, COMMIT); 
    operationCommitReqJson = deleteSchema(operationCommitReqJson);
    
    String enterOtpRespJson = put.setAuthorizationHeader(getAuthHeaderWithOtp()).send(OPERATIONS_URL + operationId, operationCommitReqJson); 
    checkOnError(enterOtpRespJson);
    checkStatus(SC_OK);
    
    OperationModel enterOtpRespModel = JsonHelper.deserializeJson(OperationModel.class, enterOtpRespJson);
    Assert.assertNotEquals(enterOtpRespModel.status.id, "new", "Operation status not 'new' in final response");
    Assert.assertTrue("Комиссия 15 рублей", enterOtpRespModel.status.displayName.contains("Комиссия составила 15"));
  }
  
  @Test()
  public void servicesClassServiceDoesntContainPaymentAfterStartOperation()  {
    String serviceId = getTestProperty("serviceId");
    String userJson = login("/user", user);                                            
    checkCorrectLogin(userJson);
    String operationReqJson = createStartOperationJson(serviceId);
    String operationRespJson = post.send(OPERATIONS_URL, operationReqJson);
    checkOnError(operationRespJson);
    checkStatus(SC_OK);
    OperationModel operationModel = JsonHelper.deserializeJson(OperationModel.class, operationRespJson);
    for(MetadataModel metaData : operationModel.__schema) {
      if(metaData.type.equals("paymentTool")) {
       Assert.fail("Response to start operation with services class service(serviceId=" + serviceId + ") contain paymentTool");
      }
    }
  }
  
  
}
