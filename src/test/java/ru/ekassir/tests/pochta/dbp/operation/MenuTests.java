package ru.ekassir.tests.pochta.dbp.operation;

import static org.apache.http.HttpStatus.SC_OK;
import  ru.ekassir.dbp.helpers.JsonHelper;
import org.junit.*;

import ru.ekassir.common.load.OperationsWraps;
import ru.ekassir.dbp.models.json.menu.MenuElementModel;
import ru.ekassir.dbp.models.json.menu.MenuModel;


public class MenuTests extends OperationsWraps{
  
  @Test()
  public void menuTest() {
    String menu = login("/banking/operations/menu", user);
    MenuModel menuModel =JsonHelper.deserializeJson(MenuModel.class, menu);
    Assert.assertFalse("No menu items. " + user, menuModel.items.length == 0);
    checkMenuModel(menuModel);
  }
  
  @Test()
  public void menuByIdTest() {
    String menu = login("/banking/operations/menu/transfers", user);
    MenuModel menuModel =JsonHelper.deserializeJson(MenuModel.class, menu);
    Assert.assertFalse("No menu items. " + user, menuModel.items.length == 0);
    checkMenuModel(menuModel);
  }
  
  @Test()
  public void menuByParentIdTest() {
    String parentId = getTestProperty("parentId");
    login(ACCOUNT_URL, user);
    get.setUrl("/banking/operations/menu").addParam("parentId", parentId).send();    
    MenuModel menuModel =JsonHelper.deserializeJson(MenuModel.class, get.getLastResponse());
    Assert.assertFalse("No menu items. " + user, menuModel.items.length == 0);
    checkMenuModel(menuModel);
  }

  @Test()
  public void menuTopFilterTest() {
    String top = getTestProperty("top");
    login(ACCOUNT_URL, user);
    get.setUrl("/banking/operations/menu").addParam("top", top).send();
    //checkStatus(SC_OK);
    MenuModel menuModel =JsonHelper.deserializeJson(MenuModel.class, get.getLastResponse());
    Assert.assertFalse("No menu items. " + user, menuModel.items.length == 0);
    checkMenuModel(menuModel);
    Assert.assertTrue("Count of items in response equals top value in filter.", menuModel.items.length == Integer.valueOf(top));
    Assert.assertEquals(menuModel.count.intValue(), Integer.parseInt(top));
    //TODO: проверить работу параметра skip для меню
  }
  
  @Test()
  public void menuIdFilterTest() {    
    String parentId = getTestProperty("parentId");
    login(ACCOUNT_URL, user);
    get.setUrl("/banking/operations/menu").addParam("parentId", parentId).send();
    //checkStatus(SC_OK);
    MenuModel menuModel =JsonHelper.deserializeJson(MenuModel.class, get.getLastResponse());
    Assert.assertFalse(menuModel.items.length == 0);
    String id = getRandomMenuItem(menuModel);
    get.setUrl("/banking/operations/menu").addParam("id", id).send();
   // checkStatus(SC_OK);
    MenuModel menuFiltredByIdModel =JsonHelper.deserializeJson(MenuModel.class, get.getLastResponse());
    Assert.assertFalse("No filtered by id menu items!", menuFiltredByIdModel.items.length == 0);
    checkMenuModel(menuFiltredByIdModel);
    for(MenuElementModel menuElement : menuFiltredByIdModel.items){
      Assert.assertEquals("Menu item have the same value as requsted id", menuElement.id, id);
    }
  }

  
  //Поиск по тегу searchTags заданному в PS
  @Test()
  public void menuByServiceSearchTagTest() {
    String query = getTestProperty("query");
    login(ACCOUNT_URL, user);
    get.setUrl("/banking/operations/menu").addParam("query", query).send();
    //checkStatus(SC_OK);    
    MenuModel menuModel =JsonHelper.deserializeJson(MenuModel.class, get.getLastResponse());
    Assert.assertFalse("No menu items. " + user, menuModel.items.length == 0);
    for(MenuElementModel menuElement : menuModel.items){
      JsonHelper.isAllNotNullFieldsPresent(menuElement);
      Assert.assertEquals("displayName correct for search request by service searchTag.", menuElement.displayName, "МТС");
      Assert.assertEquals("templateId correct for search request by service searchTag.", menuElement.id, "126");
    }
  }
  
  @Test()
  public void menuByWrongSearchTest() {
    String query = getTestProperty("query");
    login(ACCOUNT_URL, user);
    get.setUrl("/banking/operations/menu").addParam("query", query).send();
    //checkStatus(SC_OK);
    MenuModel menuModel =JsonHelper.deserializeJson(MenuModel.class, get.getLastResponse());
    Assert.assertFalse("No menu items. " + user, menuModel.items.length == 0);
    for(MenuElementModel menuElement : menuModel.items){
      JsonHelper.isAllNotNullFieldsPresent(menuElement);
      Assert.assertEquals(menuElement.displayName, "Перевод в другой банк");
      Assert.assertEquals(menuElement.id, "711");
    }
  }
  
  @Test()
  public void menuLocalityHomeTest() {
    String serviceId = getTestProperty("serviceId");
    login(ACCOUNT_URL, user);
    get.setUrl("/banking/operations/menu").addParam("id", serviceId).addParam("locality", "home").send();
   // checkStatus(SC_OK);                                  
    MenuModel menuModel =JsonHelper.deserializeJson(MenuModel.class, get.getLastResponse());
    Assert.assertFalse("No menu items. " + user, menuModel.items.length == 0);
    for(MenuElementModel menuElement : menuModel.items){
      JsonHelper.isAllNotNullFieldsPresent(menuElement);
      Assert.assertEquals(menuElement.id, serviceId, "id correct for locality=home request.");
    }
    get.setUrl("/banking/operations/menu").addParam("id", serviceId).addParam("locality", "wideArea").send();
    //checkStatus(SC_OK); 
    MenuModel wideAreaMenu =JsonHelper.deserializeJson(MenuModel.class, get.getLastResponse());
    Assert.assertTrue(wideAreaMenu.items.length == 0);
  }
  
  @Test()
  public void menuLocalityWideAreaTest() {
    String serviceId = getTestProperty("serviceId");
    login(ACCOUNT_URL, user);
    get.setUrl("/banking/operations/menu").addParam("id", serviceId).addParam("locality", "wideArea").send();
   // checkStatus(SC_OK);                                  
    MenuModel menuModel =JsonHelper.deserializeJson(MenuModel.class, get.getLastResponse());
    Assert.assertFalse(menuModel.items.length == 0);
    for(MenuElementModel menuElement : menuModel.items){
      JsonHelper.isAllNotNullFieldsPresent(menuElement);
      Assert.assertEquals(menuElement.id, serviceId, "id correct for locality=wideArea request.");
    }
    get.setUrl("/banking/operations/menu").addParam("id", serviceId).addParam("locality", "home").send();
    //checkStatus(SC_OK);
    MenuModel wideAreaMenu =JsonHelper.deserializeJson(MenuModel.class, get.getLastResponse());
    Assert.assertTrue("Service in home.", wideAreaMenu.items.length == 0);
  }
  
  //Поиск по тегу searchTags заданному в PS
  @Test()
  public void menuByServiceTypeSearchTagTest() {
    String query = getTestProperty("query");
    login(ACCOUNT_URL, user);
    get.setUrl("/banking/operations/menu").addParam("query", query).send();
    //checkStatus(SC_OK);
    MenuModel menuModel =JsonHelper.deserializeJson(MenuModel.class, get.getLastResponse());
    Assert.assertFalse("No menu items. " + user, menuModel.items.length == 0);
    for(MenuElementModel menuElement : menuModel.items){
      JsonHelper.isAllNotNullFieldsPresent(menuElement);
      Assert.assertEquals("Name correct for search request by service searchTag.", menuElement.template.name, "Другому клиенту банка");
    }
  }
  
  
}
