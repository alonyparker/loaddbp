package ru.ekassir.tests.pochta.dbp.agreement;

import static org.apache.http.HttpStatus.SC_OK;
import static ru.ekassir.dbp.helpers.HeadersConstants.EKASSIR_HTML;
import static ru.ekassir.dbp.helpers.HeadersConstants.EKASSIR_PDF;
import  ru.ekassir.dbp.helpers.JsonHelper;
import static ru.ekassir.dbp.utils.DateValidator.DATE_TIME_FORMAT;

import org.joda.time.DateTime;

import com.google.gson.JsonObject;
import org.junit.*;
import ru.ekassir.common.load.TimelineWraps;
import ru.ekassir.dbp.models.json.timeline.DetailedHistoryEventModel;
import ru.ekassir.dbp.models.json.timeline.HistoryEventModel;
import ru.ekassir.dbp.models.json.timeline.TimelineModel;
import ru.ekassir.dbp.utils.DateValidator;

public class TimelineTests extends TimelineWraps{
  //Проверка ленты по контракту
  @Test()
  public void timelineByContractTest() {
    String count = getTestProperty("count");
    login(user);
    String contractId = getLoanContractId();    
    String timeLineJson = get.setUrl("/banking/contracts/" + contractId + "/timeline").addParam("count", count).send();
    checkStatus(SC_OK);
    TimelineModel timeLineModel = JsonHelper.deserializeJson(TimelineModel.class, timeLineJson);    
    Assert.assertFalse( "User: " + user + ", contract: " + contractId + " doesn't contains any events", timeLineModel.events.items.length == 0);
    for(HistoryEventModel event : timeLineModel.events.items){
      Assert.assertFalse("No \"contract\" and \"creditContract\" fields in event: " + event.id, (event.contract == null)&&(event.creditContract == null));
      Assert.assertTrue("Event belong to contract: " + contractId, event.contract.id.equals(contractId)||event.creditContract.id.equals(contractId));     
      String historyEventJson = get.send("/banking/contracts/" + contractId +  "/timeline/events/" + event.id);
      checkStatus(SC_OK);
      DetailedHistoryEventModel historyEventModel = JsonHelper.deserializeJson(DetailedHistoryEventModel.class, historyEventJson);
      Assert.assertFalse("No \"contract\" and \"creditContract\" fields in event: " + historyEventModel.id, (historyEventModel.contract == null)&&(historyEventModel.creditContract == null));
      Assert.assertTrue("Event belong to contract: " + contractId, historyEventModel.contract.id.equals(contractId)||historyEventModel.creditContract.id.equals(contractId));
      Assert.assertEquals(historyEventModel.id, event.id);
      Assert.assertTrue(DateValidator.isThisDateValid(historyEventModel.timestamp, DATE_TIME_FORMAT));
    }
  }
  
  //Проверка общей ленты событий с анализом кажлого события
  @Test()
  public void timelineTest() {
    String count = getTestProperty("count");
    log("Start login");
    login(ACCOUNT_URL, user);
    log("end login");
    log("count = " + count);
    String timeLineJson = get.setUrl("/timeline").addParam("count", count).send();
    checkStatus(SC_OK);
    TimelineModel timeLineModel = JsonHelper.deserializeJson(TimelineModel.class, timeLineJson);    
    eventAnalisotor(timeLineModel);
  }
  
  //Проверка получения разных чеков
  @Test()
  public void timelineGetOperationsTransferTest() {
    String count = getTestProperty("count");
    login("/timeline", user);
    get.setUrl("/timeline").addParam("count", count).send();
    TimelineModel timeLineModel = JsonHelper.deserializeJson(TimelineModel.class, get.getLastResponse());
    Assert.assertFalse("User: \"" + user + "\" have empty timeline.", timeLineModel.events.items.length == 0);
    HistoryEventModel receiptEvent = null;    
    for(HistoryEventModel event : timeLineModel.events.items){
      if((event.availableDocuments != null)&&(event.availableDocuments[0].type.equals("receipt"))) {
        receiptEvent = event;
        break;
      }     
    }
    if(receiptEvent != null) {
      get.setAcceptHeader(EKASSIR_HTML).setUrl("/timeline/events/" + receiptEvent.id).addParam("template", "receipt").send();
      checkStatus(SC_OK);
    }    
    Assert.assertFalse("User: " + user + " have no reciept events", receiptEvent == null);
  }
  
  //Проверка summary с типом month
  @Test()
  public void summaryMonthTest() {
    String startDate =  getTestProperty("startDate");
    String endDate = getTestProperty("endDate");
    String summaryType = "month";    
    login(user);        
    get.setUrl("/timeline").addParam("startDate", startDate).addParam("endDate", endDate).addParam("summaryType", summaryType).send();
    checkStatus(SC_OK);
    TimelineModel timelineModel = JsonHelper.deserializeJson(TimelineModel.class, get.getLastResponse());    
    Assert.assertFalse(timelineModel.events.items.length == 0);
    Assert.assertFalse(timelineModel.summary == null);
    Assert.assertEquals(timelineModel.summary.type, summaryType);
    Assert.assertFalse(timelineModel.totalCreditAmount == null);
    log("totalCreditAmount in timeline: " + timelineModel.totalCreditAmount.amount.intValue());
    Assert.assertEquals(timelineModel.totalCreditAmount.amount.intValue(), countAmountInSummary("credit", timelineModel.summary));
    Assert.assertFalse(timelineModel.totalDebitAmount == null);
    log("totalDebitAmount in timeline: " + timelineModel.totalDebitAmount.amount.intValue());
    Assert.assertEquals(timelineModel.totalDebitAmount.amount.intValue(), countAmountInSummary("debit", timelineModel.summary));   
    for(int i = 0; i < timelineModel.summary.items.length; ++i) {
      Assert.assertFalse(timelineModel.summary.items[i].startDate == null);
      Assert.assertFalse(timelineModel.summary.items[i].endDate == null);
    }
  }
  
  //Проверка summary с типом category
  @Test()
  public void summaryCategoryTest() {
    String startDate =  getTestProperty("startDate");
    String endDate = getTestProperty("endDate");
    String summaryType = "category";    
    login(user);        
    get.setUrl("/timeline").addParam("startDate", startDate).addParam("endDate", endDate).addParam("summaryType", summaryType).send();
    checkStatus(SC_OK);
    TimelineModel timelineModel = JsonHelper.deserializeJson(TimelineModel.class, get.getLastResponse());    
    Assert.assertFalse("No events in timeline.", timelineModel.events.items.length == 0);
    Assert.assertFalse("No \"summary\" field.", timelineModel.summary == null);
    Assert.assertEquals(timelineModel.summary.type, summaryType);
    Assert.assertFalse("No \"totalCreditAmount\"", timelineModel.totalCreditAmount == null);
    log("totalCreditAmount in timeline: " + timelineModel.totalCreditAmount.amount.intValue());
    Assert.assertEquals(timelineModel.totalCreditAmount.amount.intValue(), countAmountInSummary("credit", timelineModel.summary));
    Assert.assertFalse(timelineModel.totalDebitAmount == null);
    log("totalDebitAmount in timeline: " + timelineModel.totalDebitAmount.amount.intValue());
    Assert.assertEquals(timelineModel.totalDebitAmount.amount.intValue(), countAmountInSummary("debit", timelineModel.summary));
    for(int i = 0; i < timelineModel.summary.items.length; ++i) {
      Assert.assertFalse(timelineModel.summary.items[i].categoryId == null);  
    }
  }
  
  //Фильтрация по страницам
  @Test()
  public void pageFilterTest() {
    login(user);
    String page = getTestProperty("page");    
    get.setUrl("/timeline").addParam("page", page).send();
    checkStatus(SC_OK);
    TimelineModel timeLineModel = JsonHelper.deserializeJson(TimelineModel.class, get.getLastResponse());    
    Assert.assertFalse("timeline this user: " + user + " doesn't contain any events.", timeLineModel.events.items.length == 0); 
    Assert.assertEquals("Page have correct value.", timeLineModel.page, Integer.valueOf(page));
  }

  @Test()
  public void futureOffsetFilterTest() {
    login(user);    
    String focusDate = getTestProperty("focusDate");   
    String dateToCompare = focusDate + "T23:59:59+04:00";    
    String futureOffset = getTestProperty("futureOffset");     
    get.setUrl("/timeline").addParam("futureOffset", futureOffset).addParam("focusDate", focusDate).send();
    checkStatus(SC_OK);
    TimelineModel timeLineModel = JsonHelper.deserializeJson(TimelineModel.class, get.getLastResponse());    
    Assert.assertFalse( user + " have no events.", timeLineModel.events.items.length == 0);
    Assert.assertEquals(timeLineModel.futureCount, Integer.valueOf(futureOffset));
    Assert.assertEquals(timeLineModel.futureCount.intValue(), countFutureEvent(dateToCompare, timeLineModel));
  }
  
  @Test()  
  public void focusDateFilterTest()  {
    String focusDate = getTestProperty("focusDate");
    String count = getTestProperty("count");
    login("/account", user);    
    String dateToCompare = focusDate + "T23:59:59+04:00";     
    get.setUrl("/timeline").addParam("count", count).addParam("focusDate", focusDate).send();
    checkStatus(SC_OK);
    TimelineModel timeLineModel = JsonHelper.deserializeJson(TimelineModel.class, get.getLastResponse());    
    Assert.assertFalse(user + " have no events assosiated with filter was set.", timeLineModel.events.items.length == 0);
    DateTime firstEventTimestamp = DateValidator.stringToDate(timeLineModel.events.items[0].timestamp, DATE_TIME_FORMAT);    
    Assert.assertTrue(firstEventTimestamp.isBefore(DateValidator.stringToDate(dateToCompare, DATE_TIME_FORMAT)));
    Assert.assertTrue(isTimelineSortedByDesc(timeLineModel));
  }
  
  @Test()
  public void countFilterTest()  {
    String count = getTestProperty("count");
    login("/account", user);    
    get.setUrl("/timeline").addParam("count", count).send();
    checkStatus(SC_OK);
    TimelineModel timeLineModel = JsonHelper.deserializeJson(TimelineModel.class, get.getLastResponse());    
    Assert.assertFalse(user + " have no events.", timeLineModel.events.items.length == 0);
    Assert.assertEquals(timeLineModel.count, Integer.valueOf(count));
    Assert.assertTrue(timeLineModel.events.items.length == Integer.valueOf(count));
  }

  
  @Test()
  public void contractTypeMainFilterTest() {
    String startDate =  getTestProperty("startDate");
    String endDate = getTestProperty("endDate");
    String contractType = getTestProperty("type");
    login("/account", user);        
    get.setUrl("/timeline").addParam("filter", "main").addParam("contractType", contractType).addParam("startDate", startDate).addParam("endDate", endDate).send();    
    checkStatus(SC_OK);
//    TimelineModel timeLineModel = JsonHelper.deserializeJson(TimelineModel.class, getLastResponse());    
//    Assert.assertFalse(timeLineModel.events.items.length == 0, user + " have no events.");
//    for(HistoryEventModel event : timeLineModel.events.items) {
//      Assert.assertEquals(event.contract.type, contractType,"Is filtred events contractType=" + contractType);
//    }
  }
  
  @Test()
  public void typeOperationMainFilterTest() {
    login( "/account", user);
    String type = "operation";    
    String count = getTestProperty("count");
    get.setUrl("/timeline").addParam("filter", "main").addParam("count", count).addParam("type", type).send();
    checkStatus(SC_OK);
    TimelineModel timeLineModel = JsonHelper.deserializeJson(TimelineModel.class, get.getLastResponse());    
    Assert.assertFalse(user + " have no events.", timeLineModel.events.items.length == 0);
    for(HistoryEventModel event : timeLineModel.events.items) {
      Assert.assertEquals(event.type, type);
    }
  }
  
  
  @Test()
  public void titleMainFilterTest()  {
    login("/account", user);    
    String count = getTestProperty("count");
    get.send("/timeline");
    checkStatus(SC_OK);
    TimelineModel timelineModel = JsonHelper.deserializeJson(TimelineModel.class, get.getLastResponse());
    Assert.assertFalse(user + " have no events in timeline.", timelineModel.events.items.length == 0);
    String title = timelineModel.events.items[0].title;
    get.setUrl("/timeline").addParam("filter", "main").addParam("count", count).addParam("title", title).send();
    checkStatus(SC_OK);
    timelineModel = JsonHelper.deserializeJson(TimelineModel.class, get.getLastResponse());    
    Assert.assertFalse(user + " have no events with title filter.", timelineModel.events.items.length == 0);
    for(HistoryEventModel event : timelineModel.events.items) {
      Assert.assertEquals(event.title, title);
    }
  }
  
  @Test()
  public void categoryIdMainFilterTest()  {
    login("/account", user);    
    String categoryId = getTestProperty("categoryId");
    String count = getTestProperty("count");
    get.setUrl("/timeline").addParam("filter", "main").addParam("count", count).addParam("categoryId", categoryId).send();
    checkStatus(SC_OK);
    TimelineModel timelineModel = JsonHelper.deserializeJson(TimelineModel.class, get.getLastResponse());    
    Assert.assertFalse(user + " have no events.", timelineModel.events.items.length == 0);  
    for(HistoryEventModel event : timelineModel.events.items) {
      Assert.assertEquals(event.categoryId, categoryId);
    }
  }
  
  @Test()
  public void creditOperationTypeMainFilterTest()  {
    login("/account", user);    
    String operationType = "credit";    
    String count = getTestProperty("count");
    get.setUrl("/timeline").addParam("filter", "main").addParam("count", count).addParam("operationType", operationType).send();
    checkStatus(SC_OK);
    TimelineModel timeLineModel = JsonHelper.deserializeJson(TimelineModel.class, get.getLastResponse());    
    Assert.assertFalse(user + " have no events.", timeLineModel.events.items.length == 0);
    for(HistoryEventModel event : timeLineModel.events.items) {
      Assert.assertEquals(event.operationType, operationType);
    }
  }
  
  @Test()
  public void cardFilterTest()  {
    filterTest("card", "50", user);
  }
  
  @Test()
  public void loanFilterTest()  {
    filterTest("loan", "50", user);
  }
  
  @Test()
  public void depositFilterTest()  {
    filterTest("deposit", "50", user);
  }
  
  
  //Проверка что фильтр count Отрабатывает вместе с фильтром main
  @Test()
  public void mainWithCountFilterTest() {
    String count = getTestProperty("count");
    login(ACCOUNT_URL, user);
    get.setUrl("/timeline").addParam("filter", "main").addParam("count", count).send();
    TimelineModel timelineModel = JsonHelper.deserializeJson(TimelineModel.class, get.getLastResponse());
    Assert.assertFalse(timelineModel.events.items.length == 0);
    Assert.assertTrue(timelineModel.count == Integer.valueOf(count));
    Assert.assertTrue(timelineModel.events.items.length == Integer.valueOf(count));
  }
  
 
  //Отправка чека на почту
  @Test()
  public void sendReceiptToEmailTest() {
    String email = getTestProperty("email");
    String eventId = null;
    String timeline = login("/timeline", user);
    TimelineModel timelineModel = JsonHelper.deserializeJson(TimelineModel.class, timeline);
    Assert.assertFalse(user + " have no events.", timelineModel.events.items.length == 0);
    for(HistoryEventModel event : timelineModel.events.items) {
      if((event.availableDocuments != null)&&(event.availableDocuments[0].type.equals("receipt"))) {
        eventId = event.id;
        break;
      }
    }
    Assert.assertFalse("No suitable event in timeline.", eventId == null);
    post.setUrl("/timeline/" + eventId).addParam("action", "send").addParam("template", "receipt")
        .addParam("email", email).addParam("format", "pdf").addParam("channel", "email")
        .send();
    checkStatus(SC_OK);    
  }
  
  //Отправка чека из под контракта на почту
  @Test()
  public void sendReceiptFromContractToEmailTest() {
    String email = getTestProperty("email");
    String eventId = null;
    login(user);
    String contractId = getCardContractId();
    String timeline = get.send("/banking/contracts/" + contractId + "/timeline");
    TimelineModel timelineModel = JsonHelper.deserializeJson(TimelineModel.class, timeline);
    Assert.assertFalse(timelineModel.events.items.length == 0);
    for(HistoryEventModel event : timelineModel.events.items) {
      if((event.availableDocuments != null)&&(event.availableDocuments[0].type.equals("receipt"))) {
        eventId = event.id;
        break;
      }
    }
    Assert.assertFalse(eventId == null);
    post.setUrl("/banking/contracts/" + contractId + "/timeline/" + eventId).addParam("action", "send").addParam("template", "receipt")
        .addParam("email", email).addParam("format", "pdf").addParam("channel", "email")
        .send();
    checkStatus(SC_OK);    
  }
  
  //Получение чека в PDF
  @Test()
  public void receiptPdfTest() {    
    String eventId = null;
    String timeline = login("/timeline", user);
    TimelineModel timelineModel = JsonHelper.deserializeJson(TimelineModel.class, timeline);
    Assert.assertFalse(timelineModel.events.items.length == 0);
    for(HistoryEventModel event : timelineModel.events.items) {
      if((event.availableDocuments != null)&&(event.availableDocuments[0].type.equals("receipt"))) {
        eventId = event.id;
        break;
      }
    }
    Assert.assertFalse(eventId == null);
    get.setAcceptHeader(EKASSIR_PDF).setUrl("/timeline/" + eventId).addParam("template", "receipt").send();
    checkStatus(SC_OK);    
  }
  
  //Получение чека из под контракта в PDF
  @Test()
  public void receiptPdfFromContractTest() {    
    String eventId = null;
    login(user);
    String contractId = getCardContractId();
    String timeline = get.send("/banking/contracts/" + contractId + "/timeline");
    TimelineModel timelineModel = JsonHelper.deserializeJson(TimelineModel.class, timeline);
    Assert.assertFalse(user + " have no events.", timelineModel.events.items.length == 0);
    for(HistoryEventModel event : timelineModel.events.items) {
      if((event.availableDocuments != null)&&(event.availableDocuments[0].type.equals("receipt"))) {
        eventId = event.id;
        break;
      }
    }
    Assert.assertFalse("No suitable event in contract timeline.", eventId == null);
    get.setAcceptHeader(EKASSIR_PDF).setUrl("/timeline/" + eventId).addParam("template", "receipt").send();
    checkStatus(SC_OK);    
  }
}