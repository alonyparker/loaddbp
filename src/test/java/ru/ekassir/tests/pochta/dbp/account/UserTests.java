package ru.ekassir.tests.pochta.dbp.account;

import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_CONFLICT;
import static org.apache.http.HttpStatus.SC_OK;
import static org.apache.http.HttpStatus.SC_UNAUTHORIZED;
import ru.ekassir.dbp.helpers.JsonHelper;
import org.junit.*;

import java.util.ArrayList;

import ru.ekassir.common.load.CommonWraps;
import ru.ekassir.dbp.models.json.user.UserModel;

public class UserTests extends CommonWraps {
	//Проверка соответствия полей в ресурсе "/user" с API DBP
	  @Test()
	  public void userTest(){   
	    String userJson = login("/user", user);    
	    JsonHelper.deserializeJson(UserModel.class, userJson);    
	  }
	  
	//Обновление канала оповещения (push, email, sms)
	  @Test()
	  public void updateNotificationChannelTest() {
	    //Создание коллекции всех возможных каналов нотификации
	    ArrayList<String> notificationChannels = new ArrayList<String>();
	    notificationChannels.add("push");
	    notificationChannels.add("email");
	    notificationChannels.add("sms");
	    String userJson = login("/user", user);
	    UserModel userModel = JsonHelper.deserializeJson(UserModel.class, userJson);
	    String notificationChannel = userModel.notificationChannel;
	    //Исключение из коллекции канала который был установлен у пользователя
	    notificationChannels.remove(notificationChannel);
	    //Создание запроса на изменение канала
	    String newNotificationChannel = notificationChannels.get(0);
	    String newNotificationChannelReq = createJsonString("notificationChannel", newNotificationChannel);
	    patch.send("/user", newNotificationChannelReq);
	    checkStatus(SC_UNAUTHORIZED);
	    String newNotificationChannelResp = patch.setAuthorizationHeader(getAuthHeaderWithOtp()).send("/user", newNotificationChannelReq);
	    checkStatus(SC_OK);
	    userModel = JsonHelper.deserializeJson(UserModel.class, newNotificationChannelResp);
	    Assert.assertTrue( "New notification channel in response: " + newNotificationChannel, userModel.notificationChannel.equals(newNotificationChannel));
	    clearCookies();
	    userJson = login("/user", user);    
	    userModel = JsonHelper.deserializeJson(UserModel.class, userJson);
	    Assert.assertTrue( "Notification channel changed to " + newNotificationChannel,userModel.notificationChannel.equals(newNotificationChannel));    
	    newNotificationChannel = notificationChannels.get(1);
	    newNotificationChannelReq = createJsonString("notificationChannel", newNotificationChannel);
	    patch.send("/user", newNotificationChannelReq);
	    checkStatus(SC_UNAUTHORIZED);
	    newNotificationChannelResp = patch.setAuthorizationHeader(getAuthHeaderWithOtp()).send("/user", newNotificationChannelReq);
	    checkStatus(SC_OK);    
	    userModel = JsonHelper.deserializeJson(UserModel.class, newNotificationChannelResp);
	    Assert.assertTrue( "New notification channel in response: " + newNotificationChannel, userModel.notificationChannel.equals(newNotificationChannel));
	    clearCookies();
	    userJson = login("/user", user);
	    userModel = JsonHelper.deserializeJson(UserModel.class, userJson);
	    Assert.assertTrue("Notification channel changed to " + newNotificationChannel,userModel.notificationChannel.equals(newNotificationChannel));
	    //Возврат канала который был изначально у пользователя
	    String notificationChannelReq = createJsonString("notificationChannel", notificationChannel);
	    patch.send("/user", notificationChannelReq);
	    checkStatus(SC_UNAUTHORIZED);
	    patch.setAuthorizationHeader(getAuthHeaderWithOtp()).send("/user", notificationChannelReq);
	    checkStatus(SC_OK);    
	  }
	  
	  //Изменение логина
	  @Test
	  public void updateLoginTest() {
	    String newLogin = getTestProperty("newLogin");
	    String userJson = login("/user", user);
	    JsonHelper.deserializeJson(UserModel.class, userJson);    
	    //Смена логина на новый
	    String newLoginReq = createJsonString("login", newLogin);
	    patch.send("/user", newLoginReq);
	    checkStatus(SC_UNAUTHORIZED);
	    String newUserJson = patch.setAuthorizationHeader(getAuthHeaderWithOtp()).send("/user", newLoginReq);
	    checkStatus(SC_OK);
	    //Проверка что в ответе новый логин
	    UserModel newUserModel = JsonHelper.deserializeJson(UserModel.class, newUserJson);
	    Assert.assertTrue( "New login equals login in change response.", newUserModel.login.equals(newLogin));
	    //Переход к ресурсу /user для проверки что там действительно новый логин 
	    newUserJson = get.send("/user");
	    checkStatus(SC_OK);
	    newUserModel = JsonHelper.deserializeJson(UserModel.class, newUserJson);
	    Assert.assertTrue("New login equals login in \"user\" resource.",newUserModel.login.equals(newLogin));
	    //Разлогин и вход по новому логину
	    clearCookies();
	    login("/user", newLogin);
	    //Возвращение старого логина
	    String loginReq = createJsonString("login", user);
	    patch.send("/user", loginReq);
	    checkStatus(SC_UNAUTHORIZED);
	    userJson = patch.setAuthorizationHeader(getAuthHeaderWithOtp()).send("/user", loginReq);
	    checkStatus(SC_OK);
	    //Проверка что в ответе снова старый логин
	    UserModel userModel = JsonHelper.deserializeJson(UserModel.class, userJson);
	    Assert.assertTrue("Old login equals login change in response.", userModel.login.equals(user));
	    //Переход к ресурсу /user для проверки что там старый логин
	    userJson = get.send("/user");
	   // checkStatus(SC_OK);
	    userModel = JsonHelper.deserializeJson(UserModel.class, userJson);
	    Assert.assertTrue("Old login equals login in \"user\" resource.", userModel.login.equals(user));
	  }
	  
	  
	  //Смена логина на уже существующий в системе логин
	  @Test()
	  public void updateLoginToExistingTest() {
	    String existingLogin = getTestProperty("existingLogin");
	    String userJson = login("/user", user);
	    UserModel userModel = JsonHelper.deserializeJson(UserModel.class, userJson);
	    String oldLogin = userModel.login;
	    String newExistingLoginReq = createJsonString("login", existingLogin);
	    patch.send("/user", newExistingLoginReq);
	    checkStatus(SC_UNAUTHORIZED);
	    patch.setAuthorizationHeader(getAuthHeaderWithOtp()).send("/user", newExistingLoginReq);
	    checkStatus(SC_CONFLICT);
	    clearCookies();
	    userJson = login("/user", user);
	    userModel = JsonHelper.deserializeJson(UserModel.class, userJson);
	    Assert.assertTrue("Login was not changed", userModel.login.equals(oldLogin));
	    clearCookies();
	    userJson = login("/user", user);
	    userModel = JsonHelper.deserializeJson(UserModel.class, userJson);
	    Assert.assertTrue("Login was not changed",userModel.login.equals(oldLogin));
	  }
	  
	 
}
