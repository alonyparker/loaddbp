package ru.ekassir.common.load;

import static org.apache.http.HttpStatus.SC_OK;
import static ru.ekassir.dbp.helpers.HeadersConstants.EKASSIR_JSON;
import static ru.ekassir.dbp.helpers.HeadersConstants.STANDART_JSON;
import static ru.ekassir.dbp.helpers.HeadersConstants.STANDART_JSON_UTF8;
import static ru.ekassir.dbp.helpers.HeadersConstants.WEB_FORM;
//import static ru.ekassir.dbp.helpers.TestHelper.checkCorrectLogin;
//import static ru.ekassir.dbp.requester.Request.getResponseStatusCode;

import ru.ekassir.dbp.helpers.JsonHelper;
import ru.ekassir.dbp.helpers.MoneyBag;
import ru.ekassir.dbp.models.json.dashboard.BaseContractModel;
import ru.ekassir.dbp.models.json.dashboard.CardBaseContractModel;
import ru.ekassir.dbp.models.json.dashboard.CardExtendedContractModel;
import ru.ekassir.dbp.models.json.dashboard.CurrentBaseContractModel;
import ru.ekassir.dbp.models.json.dashboard.DashboardModel;
import ru.ekassir.dbp.models.json.dashboard.DepositBaseContractModel;
import ru.ekassir.dbp.models.json.dashboard.LoanBaseContractModel;

import org.junit.Assert;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import ru.ekassir.dbp.utils.DateValidator;


public class CommonWraps extends TestBase {
   final public  String DASHBOARD_URL = "/self?exclude=events";
   final public  String ACCOUNT_URL = "/account";
   final public  String CONTRACTS_URL = "/banking/contracts/";
	final public  String USER_URL = "/user";
	protected JsonParser parser; 
	protected JsonHelper JsonHelper;
	protected DateValidator DateValidator;
	public CommonWraps()
	{
		super();
		DateValidator = new DateValidator();
		parser = new JsonParser();
		JsonHelper = new JsonHelper();
	}
	
	public void checkStatus(int statusCode){    
	    Assert.assertEquals("Wrong response status.", lastResp.getLastResponseCode(), statusCode);
	  }
	public String login(String user) {
	    get.setAcceptHeader(STANDART_JSON).send(ACCOUNT_URL);
	    post.setAcceptHeader(STANDART_JSON).setContentTypeHeader(WEB_FORM);
	    post.addBodyQueryParam("username", user).addBodyQueryParam("password", password).addBodyQueryParam("confirmation", "0").buildParams();    
	    String response = post.send(get.getLastRequestedUrl());  
	    checkStatus(SC_OK);
	    response = sendOtp(response);
	    checkCorrectLogin(response);
	    refreshGet();
	    refreshPost();
	    return response;
	  }
	public String login(String url, String user, String password) {
	    get.setAcceptHeader(STANDART_JSON).send(url);
	    post.setAcceptHeader(STANDART_JSON).setContentTypeHeader(WEB_FORM);
	    post.addBodyQueryParam("username", user).addBodyQueryParam("password", password).addBodyQueryParam("confirmation", "0").buildParams();    
	    String response = post.send(get.getLastRequestedUrl());
	    checkStatus(SC_OK);    
	    response = sendOtp(response);
	    checkCorrectLogin(response);
	    refreshGet();
	    refreshPost();
	    return response;
	  }
	public String login(String url, String user) {
	    get.setAcceptHeader(STANDART_JSON).send(url);
	    post.setAcceptHeader(STANDART_JSON).setContentTypeHeader(WEB_FORM);
	    post.addBodyQueryParam("username", user).addBodyQueryParam("password", password).addBodyQueryParam("confirmation", "0").buildParams();
	    String response = post.send(get.getLastRequestedUrl());  
	    response = sendOtp(response);
	    checkCorrectLogin(response);
	    refreshGet();
	    refreshPost();
	    return response;
	  }
	
	public String sendOtp(String response){
	    String otpResponse = null;
	    String sendOtpBody = "[{\"name\":\"confirmation\",\"value\":\"0\"},{\"name\":\"otp\",\"value\":\"1234\"}]";
	    JsonObject responseObject = parser.parse(response).getAsJsonObject();
	    if((responseObject.get("template") != null)&&(responseObject.get("template").getAsString().equals("otp"))){
	      otpResponse = post.addHeader("Accept", EKASSIR_JSON).setContentTypeHeader(STANDART_JSON_UTF8).send(get.getLastRequestedUrl(), sendOtpBody);
	      return otpResponse;
	    }    
	    return response;
	  }
	public void checkCorrectLogin(String accountJson) {
		 
		   JsonObject operationJsonObject = parser.parse(accountJson).getAsJsonObject();   
		   if((operationJsonObject.get("errors") != null)&&(operationJsonObject.get("errors").getAsJsonArray().size() != 0)){
		     Assert.fail(operationJsonObject.get("errors").getAsJsonArray().get(0).getAsJsonObject().get("value").toString());
		   }
		   if((operationJsonObject.get("header") != null)&&(operationJsonObject.get("callbacks") != null)) {
		     Assert.fail("Incorrect screen after login!\nHeader: " + operationJsonObject.get("header").getAsString());
		   }
		 }

	
	public void delay(long sec) {
	    try {
	      log("Waiting " + sec + "sec.");
	      Thread.sleep(sec * 1000);
	    } catch (InterruptedException e) {
	      Assert.fail("Thred was interrupted."); 
	    }
	  }
	//Возвращает id первого карточного контракта в статусе "opened"
	  public String getCardContractId(){    
	    String contractId = null;
	    String self = get.send(DASHBOARD_URL);
	   // checkStatus(SC_OK);
	    DashboardModel dashboardModel = JsonHelper.deserializeJson(DashboardModel.class, self);
	    if(dashboardModel.cardContracts.items.length == 0){
	     log("User have no CARD contract.");
	     Assert.fail("User have no CARD contract.");
	     return contractId;
	    }    
	    for (CardBaseContractModel contract : dashboardModel.cardContracts.items ){
	      if(contract.state.equals("opened")){
	        contractId = contract.id;
	        break;//нашли первый подходящий и возвращаем его
	      }
	    }
	    if(contractId == null) {
	      logWarn("User have no CARD contract in \"opened\" state.");
	      Assert.fail("User have no CARD contract in \"opened\" state.");
	    }
	    return contractId;
	  }
	//Проверка json на присутсвие поля _error и в случае присутствия вывод на экран
	  public void checkOnError(String json) {
	    JsonObject jsonObject = parser.parse(json).getAsJsonObject();
	    if(jsonObject.get("__error") != null) {
	      logError(jsonObject.get("__error").getAsJsonObject().get("message").getAsString());
	      Assert.fail("ERROR: " + jsonObject.get("__error").getAsJsonObject().get("message").getAsString());
	    }
	  }
	//Создание Json строки из одного Json элемента
	  public String createJsonString(String name, String value){    
	    JsonElement je = parser.parse(value);
	    JsonObject jsonObject = new JsonObject();
	    jsonObject.add(name, je);
	    return jsonObject.toString();
	  }  
	  
	  public String createSimpleJsonString(String name, String value){
	    JsonObject jsonObject = new JsonObject();
	    jsonObject.addProperty(name, value);
	    return jsonObject.toString();
	  }
	  
	  public String createSimpleJsonString(String name, Boolean value){
	    JsonObject jsonObject = new JsonObject();
	    jsonObject.addProperty(name, value);
	    return jsonObject.toString();
	  }
	  
	  //Добавление простейшего поля name:value к операции
	  public String addFieldToJson(String json, String name, String value){    
	    JsonObject jsonObject = parser.parse(json).getAsJsonObject();
	    jsonObject.addProperty(name, value);
	    return jsonObject.toString();
	  }  
	  
	  //Добавление простейшего поля name:value к операции
	  public String addFieldToJson(String json, String name, Double value){    
	    JsonObject jsonObject = parser.parse(json).getAsJsonObject();
	    jsonObject.addProperty(name, value);
	    return jsonObject.toString();
	  }
	  
	//Добавление простейшего поля name:value к операции
	  public String addFieldToJson(String json, String name, Boolean value){    
	    JsonObject jsonObject = parser.parse(json).getAsJsonObject();
	    jsonObject.addProperty(name, value);
	    return jsonObject.toString();
	  }
	  
	//TODO: пересмотреть
	  //Установка ОТР в авторизационный заголовок
	  public String getAuthHeaderWithOtp(){
	    String otp = getOTP(); 
	    return "VerificationCode code=\"" + otp + "\"";
	  }
	//Суммирование всех доступных сумм по контрактам
	  public MoneyBag calculateTotalAvailableOnDashboard(){
	    //Влаг говорящий о том, учитывать или нет депозиты
	    String includeDeposit = getTestProperty("includeDeposit");
	    String dashboardJson = get.send(DASHBOARD_URL);
	    checkStatus(SC_OK);
	    DashboardModel dashModel = JsonHelper.deserializeJson(DashboardModel.class, dashboardJson);
	    MoneyBag total = new MoneyBag();
	    if(dashModel.masterAccount != null) {
	      total.add(dashModel.masterAccount.availableLimitAmount);
	    }
	    if(dashModel.cardContracts.items.length != 0) {
	      for(CardBaseContractModel card : dashModel.cardContracts.items) {
	        total.add(card.availableLimitAmount);
	      }
	    }
	    if(dashModel.loanContracts.items.length != 0) {
	      for(LoanBaseContractModel loan : dashModel.loanContracts.items) {
	        boolean addFlag = loan.state.equals("preclosed") || (loan.state.equals("opened") && loan.limitClosed != null && (loan.limitClosed == false));
	        if(addFlag) {
	          total.add(loan.availableLimitAmount);
	        }
	      }
	    }
	    if((includeDeposit.equals("true"))&&(dashModel.depositContracts.items.length != 0)) {
	      for(DepositBaseContractModel deposit : dashModel.depositContracts.items) {
	        total.add(deposit.depositAmount);
	      }      
	    }    
	    if((dashModel.walletContracts != null)&&(dashModel.walletContracts.items.length != 0)) {
	      for(BaseContractModel wallet : dashModel.walletContracts.items) {
	        total.add(wallet.availableLimitAmount);
	      }
	    }
	    if((dashModel.currentContracts != null)&&(dashModel.currentContracts.items.length != 0)) {
	      for(CurrentBaseContractModel current : dashModel.currentContracts.items) {
	        total.add(current.availableLimitAmount);
	      }
	    }
	    return total;
	  }
	  //Возвращает id первой карты у первого карточного контракта в статусе "opened"
	  public String getCardId(String contractId){    
	    String cardId = null;    
	    get.setUrl("/banking/contracts/" + contractId).addParam("expand", "cards").send();
	    checkStatus(SC_OK);
	    CardExtendedContractModel cardContractModel = JsonHelper.deserializeJson(CardExtendedContractModel.class, get.getLastResponse());
	    Assert.assertFalse("No \"cards\" field in card contract " + contractId, cardContractModel.cards == null);
	    Assert.assertFalse("No card in card contract." + contractId, cardContractModel.cards.items.length == 0);
	    cardId = cardContractModel.cards.items[0].id;    
	    return cardId;
	  }
	//Возвращает id первого кредитного контракта в статусе "opened"
	  public String getLoanContractId(){    
	    String contractId = null;
	    String self = get.send(DASHBOARD_URL);    
	    checkStatus(SC_OK);
	    DashboardModel dashboardModel = JsonHelper.deserializeJson(DashboardModel.class, self);
	    if(dashboardModel.loanContracts.items.length == 0){      
	      logError("User \"" + user + "\" have no LOAN contract.");
	      Assert.fail("User \"" + user + "\" have no LOAN contract.");
	      return contractId;
	    }    
	    for (LoanBaseContractModel contract : dashboardModel.loanContracts.items ){
	      if(contract.state.equals("opened")){
	        contractId = contract.id;
	      }
	    }
	    if(contractId == null) {
	      logWarn("User \"" + user + "\" have no LOAN contract in \"opened\" state.");
	      Assert.fail("User \"" + user + "\" have no LOAN contract in \"opened\" state.");
	    }
	    return contractId;
	  }

}
