package ru.ekassir.common.load;


import java.io.IOException;
import java.lang.reflect.Method;

import org.apache.http.cookie.Cookie;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ru.ekassir.dbp.clients.OudClient;
import ru.ekassir.dbp.requester.DeleteRequest;
import ru.ekassir.dbp.requester.GetRequest;
import ru.ekassir.dbp.requester.HttpClientInizializer;
import ru.ekassir.dbp.requester.LastResponseProvider;
import ru.ekassir.dbp.requester.PatchRequest;
import ru.ekassir.dbp.requester.PostRequest;
import ru.ekassir.dbp.requester.PutRequest;
import ru.ekassir.dbp.requester.RequestFactory;
import ru.ekassir.dbp.utils.PropertyReader;
import org.apache.jmeter.protocol.java.sampler.JUnitSampler;
import org.junit.*;
import org.junit.rules.*;
import ru.ekassir.dbp.helpers.*;


public class TestBase {
	 protected Logger logger;
	public String user;
	public String password;
	protected int requestTimeoutInSecond, port;
	protected String environmentUrl, environoment, userAgent, language, trustStorePath, trustStorePassword, host;
	public boolean useFiddler;
	protected HttpClientInizializer requester = null;
	public RequestFactory requestFactory = null;
	protected GetRequest get;
	protected PostRequest post;
	protected PutRequest put;
	protected DeleteRequest delete;
	protected PatchRequest patch;
	protected PropertyReader propertyReader;
	private  String CONFIG_DIR = "";
	protected String classPropertyPath = "";
	protected String testPropertyPath = "";
	protected TestHelper TestHelper;
	protected LastResponseProvider lastResp;
	
	public TestBase()
	{
		lastResp = new LastResponseProvider();
		TestHelper= new TestHelper();
		logger = LogManager.getRootLogger();
		JUnitSampler sampler = new JUnitSampler();
		try{
			CONFIG_DIR = sampler.getThreadContext().getVariables().get("pathToConfigs");
		}
		catch (NullPointerException e){
			log("Using local testconfig");
		}
		propertyReader = new PropertyReader();
		propertyReader.init(CONFIG_DIR + "testconfig.properties");
		initProperties();
		initWebRequester();

	}
	
	private void initWebRequester()
	{
		 refreshGet();
		    refreshPost();
		    refreshPut();
		    refreshDelete();
		    refreshPatch();
		    clearCookies();
	}
	
	private void initProperties()
	{
		environoment = propertyReader.getProperty("environment"); 
	    userAgent = propertyReader.getProperty("userAgent");
	    language = propertyReader.getProperty("language");
	    requestTimeoutInSecond = Integer.parseInt(propertyReader.getProperty("requestTimeoutInSecond"));
	    useFiddler = Boolean.parseBoolean(propertyReader.getProperty("useFiddler")); 
	    //Инициализация файла конфигурации среды и ее тестовых данных
	    propertyReader.init(CONFIG_DIR + environoment + "TestData.properties");
	    propertyReader.init(CONFIG_DIR + environoment + "EnvSettings.properties");
	    //Инициализация файла с путями к файлам-картинкам
	    propertyReader.init(CONFIG_DIR + "pathsToImages.properties");
	    environmentUrl = propertyReader.getProperty("environmentUrl");
	    try {
	      if(useFiddler){
	        trustStorePath = propertyReader.getProperty("trustStorePath");
	        trustStorePassword = propertyReader.getProperty("trustStorePassword");
	        host = propertyReader.getProperty("host");
	        port = Integer.parseInt(propertyReader.getProperty("port"));
	        requester = new HttpClientInizializer(trustStorePath, trustStorePassword, host, port);
	      } else {
	        requester = new HttpClientInizializer(null, null , null , 0);
	      }
	    } catch (SecurityException e) {
	      log("SecurityException: " + e.getLocalizedMessage());      
	    } catch (IOException e) {
	      log("IOException: " + e.getLocalizedMessage());
	    } 
	   // requestFactory = new RequestFactory(requester.getHttpClient(), environmentUrl, requestTimeoutInSecond, userAgent, language);
	    requestFactory = new RequestFactory(requester.getHttpClient(), environmentUrl, requestTimeoutInSecond, userAgent, language, lastResp);
	    
	    
	}
	@Rule public TestName name = new TestName();
	@Before
	public void initCredentials() {
	    String className = getClass().getSimpleName();
	    String testName = name.getMethodName();
	    testPropertyPath = className + "." + testName + "."; 
	    classPropertyPath = className + ".";
	    if(propertyReader.getProperty(testPropertyPath + "user") != null) {
	      user = propertyReader.getProperty(testPropertyPath + "user");
	    }
	    else if((propertyReader.getProperty(testPropertyPath + "user") == null)&&(propertyReader.getProperty(classPropertyPath + "user") != null)) {
	      user = propertyReader.getProperty(classPropertyPath + "user");
	    }
	    else{
	      logWarn("No property with key \"" + testPropertyPath + "user" + "\" found!");
	      logWarn("No property with key \"" + classPropertyPath + "user" + "\" found!");
	      logWarn("Test uses default \"user\" value!");
	      user = propertyReader.getProperty("user");
	    }
	    if(propertyReader.getProperty(testPropertyPath + "password") != null) {
	      password = propertyReader.getProperty(testPropertyPath + "password");
	    }
	    else if((propertyReader.getProperty(testPropertyPath + "password") == null)&&(propertyReader.getProperty(classPropertyPath + "password") != null)) {
	      password = propertyReader.getProperty(classPropertyPath + "password");
	    }
	    else{
	      password = propertyReader.getProperty("password");
	    }

	  }
	
	public void refreshPost(){
	    post = (PostRequest) requestFactory.getRequest("POST");
	  }
	  
	  public void refreshGet(){
	    get = (GetRequest) requestFactory.getRequest("GET");
		 
	  }
	  
	  public void refreshPut(){
	    put = (PutRequest) requestFactory.getRequest("PUT");
	  }
	  
	  public void refreshPatch(){
	    patch = (PatchRequest) requestFactory.getRequest("PATCH");
	  }
	  
	  public void refreshDelete(){
	    delete = (DeleteRequest) requestFactory.getRequest("DELETE");
	  }
	  public void clearCookies(){
		    requester.getCookieStore().clear();
		  }
	  public void log(String s) {
		    logger.info(s);
		  }
		  
		  public void logWarn(String s) {
		    logger.warn(s);
		  }
		  
		  public void logError(String s) {
		    logger.error(s);
		  }
		  public void setPasswordToCn(String cn, String password) {
			    OudClient oc = new OudClient();
			    log("Set password \"" + password + "\" to user \"" + cn + "\".");
			    oc.findByCnAndModify(user, "userPassword", password);
			    oc.unBindAndClose();
		  }
		  
		  public String getClassProperty(String name) {
			    return propertyReader.getProperty(classPropertyPath + name); 
			  }
		  public String getTestProperty(String name) {
			    return propertyReader.getProperty(testPropertyPath + name); 
			  }
		  protected String getOTP(){
			    try{
			      return TestHelper.getOtp(Boolean.parseBoolean(propertyReader.getProperty("takeOtpFromFile")), "1234", propertyReader.getProperty("smsFilePath"));
			    } catch(IOException e) {
			      log("IOException: " + e.getLocalizedMessage());
			      Assert.fail("Can't read OTP!");
			    }
			    return null;
			  }
	
}
