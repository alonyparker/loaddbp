package ru.ekassir.common.load;
import static org.apache.http.HttpStatus.SC_OK;
import static org.apache.http.HttpStatus.SC_UNAUTHORIZED;
import ru.ekassir.dbp.helpers.JsonHelper;
import static ru.ekassir.dbp.utils.DateValidator.DATE_TIME_FORMAT;
import org.junit.*;

import com.google.gson.JsonObject;

import ru.ekassir.dbp.clients.CouchbaseHelper;
import ru.ekassir.dbp.helpers.MessageLogHelper;
import ru.ekassir.dbp.helpers.TestHelper;
import ru.ekassir.dbp.models.json.dashboard.CardBaseContractModel;
import ru.ekassir.dbp.models.json.dashboard.DashboardModel;
import ru.ekassir.dbp.models.json.limits.LimitModel;
import ru.ekassir.dbp.models.json.limits.LimitsModel;
import ru.ekassir.dbp.utils.DateValidator;
import ru.ekassir.dbp.utils.PropertyReader;


public class LimitsWraps extends CommonWraps{
  //Сброс корзины с авторизационными сессиями
/*  @BeforeMethod
  public void flushUnifiedBucket() {
    CouchbaseHelper cbh = new CouchbaseHelper(PropertyReader.getProperty("unifiedBucket"), PropertyReader.getProperty("unifiedBucketPassword"));
    cbh.flushBucket();
    delay(7);
    cbh.shutdownClient();
  }*/
  
 	//TODO: засунуть в конструктор инициализацию	
	public JsonObject createLimitJson(String type, String period, Integer maxAmount, String isActive, String id){
    JsonObject limitJson = new JsonObject();
    if (maxAmount == null) {
      limitJson.addProperty("type", type);
      limitJson.addProperty("period", period);
      limitJson.addProperty("id", id);
      limitJson.addProperty("isActive", isActive);
      return limitJson;
    } 
    JsonObject maxAmountJson = new JsonObject();
    maxAmountJson.addProperty("amount", maxAmount);
    maxAmountJson.addProperty("currencyCode", "RUB");
    limitJson.addProperty("type", type);
    limitJson.addProperty("period", period);
    limitJson.addProperty("id", id);
    limitJson.addProperty("isActive", isActive);
    limitJson.add("maxAmount", maxAmountJson);
    return limitJson;
  }
	
	public JsonObject createLimitJson(String type, String period, Long maxAmount, String isActive, String id){
	  JsonObject limitJson = new JsonObject();
    if (maxAmount == null) {
      limitJson.addProperty("type", type);
		  limitJson.addProperty("period", period);
		  limitJson.addProperty("id", id);
		  limitJson.addProperty("isActive", isActive);
		  return limitJson;
		} 
		JsonObject maxAmountJson = new JsonObject();
		maxAmountJson.addProperty("amount", maxAmount);
		maxAmountJson.addProperty("currencyCode", "RUB");
    limitJson.addProperty("type", type);
    limitJson.addProperty("period", period);
    limitJson.addProperty("id", id);
    limitJson.addProperty("isActive", isActive);
    limitJson.add("maxAmount", maxAmountJson);
    return limitJson;
  }
  
  public LimitsModel getLimits(String url, String resource) {
	  login(url, user);	  
	  String limitsJson = get.send(url + resource);
    return JsonHelper.deserializeJson(LimitsModel.class, limitsJson);
  }
  
  public String getLimitIdFromModel(LimitsModel limitsModel, String type, String period){
    String id = null;
    for(LimitModel limit : limitsModel.items){
      if(limit.type.equals(type) && limit.period.equals(period)){
        id = limit.id;
        return id;
      }
    }
    if(id == null){      
      logError("User haven't limits with type: " + type + " and period: " + period);
      Assert.fail("User haven't limits with type: " + type + " and period: " + period);
    }
    return id;
  }
  
  public String getCardContractNumberFromDashboard () {
	  String contractId = null;
	  String self = get.send(DASHBOARD_URL);
	  checkStatus(SC_OK);
	  DashboardModel dashboardModel = JsonHelper.deserializeJson(DashboardModel.class, self);
	  if(dashboardModel.cardContracts.items.length == 0){
	    log("User have no CARD contract.");
	    Assert.fail("User have no CARD contract.");
	    return contractId;
	  }    
	  for (CardBaseContractModel contract : dashboardModel.cardContracts.items ){
	    if(contract.state.equals("opened")){
	      contractId = contract.id;
	    }
	  }
	  if(contractId == null) {
	    logWarn("User have no CARD contract in \"opened\" state.");
	    Assert.fail("User have no CARD contract in \"opened\" state.");
	  }
	  return contractId;
  }
    
  // сбрасываем лимит и ставим maxAmount в null
  protected void activateWithoutMaxAmountTest(String type, String period) {
    String limitsJson = login(ACCOUNT_URL, user); 
    String contractId =  getCardContractId();
    String cardId = getCardId(contractId);    
    limitsJson = get.send("/banking/contracts/"+ contractId + "/cards/"+ cardId + "/limits/");
    LimitsModel limits = JsonHelper.deserializeJson(LimitsModel.class, limitsJson);
    String amountOnetimeLimitId = getLimitIdFromModel(limits, type, period);
    log("reset current limit(if isActive == true)");
    if(getCurrentLimit(amountOnetimeLimitId, contractId, cardId).isActive){
      resetLimit(type, period, amountOnetimeLimitId, contractId, cardId);
    }
    log("Update limits with maxAmount==NULL");
    String newLimitBody = createLimitJson(type, period, (Integer)null, "true", amountOnetimeLimitId).toString();
    put.send("/banking/contracts/"+ contractId + "/cards/"+ cardId + "/limits/" + amountOnetimeLimitId, newLimitBody); 
    checkStatus(SC_OK);
    log("Check current limit after change maxAmount");
    LimitModel limitAfterChange = getCurrentLimit(amountOnetimeLimitId, contractId, cardId);
    assertStartEndTimeFormat(limitAfterChange);
    assertThatLimitCorrect(limitAfterChange, amountOnetimeLimitId, type, period, null, Boolean.FALSE);
  }
  
  protected void changeLimit(String limitId, String newLimitBody){
    String req = newLimitBody;
    String contractId =  getCardContractId();
    String cardId = getCardId(contractId);    
    put.send("/banking/contracts/"+ contractId + "/cards/"+ cardId + "/limits/"+ limitId, req);
    checkStatus(SC_UNAUTHORIZED);
    put.setAuthorizationHeader(getAuthHeaderWithOtp()).send("/banking/contracts/"+ contractId + "/cards/"+ cardId + "/limits/" + limitId, req);
    checkStatus(SC_OK);
  }
  
  protected void resetLimit(String type, String period, String limitId) {
    String resetLimitBody = createLimitJson(type, period, (Integer)null, "false", limitId).toString();
    changeLimit(limitId, resetLimitBody);
  }
  
  protected void changeLimit(String limitId, String newLimitBody, String contractId, String cardId){
    String req = newLimitBody; 
    put.send("/banking/contracts/"+ contractId + "/cards/"+ cardId + "/limits/"+ limitId, req);
    checkStatus(SC_UNAUTHORIZED);
    put.setAuthorizationHeader(getAuthHeaderWithOtp()).send("/banking/contracts/"+ contractId + "/cards/"+ cardId + "/limits/" + limitId, req);
    checkStatus(SC_OK);
  }
  
  protected void resetLimit(String type, String period, String limitId, String contractId, String cardId) {
    String resetLimitBody = createLimitJson(type, period, (Integer)null, "false", limitId).toString();
    changeLimit(limitId, resetLimitBody, contractId, cardId);
  }

  protected void increaseMaxAmountTest(String type, String period) {
    String limitsJson = login(ACCOUNT_URL, user);
    String contractId =  getCardContractId();
    String cardId = getCardId(contractId);    
    limitsJson = get.send("/banking/contracts/"+ contractId + "/cards/"+ cardId + "/limits/");
    LimitsModel limits = JsonHelper.deserializeJson(LimitsModel.class, limitsJson);
    String amountOnetimeLimitId = getLimitIdFromModel(limits, type, period);
    log("reset current limit(need otp)");
    if(getCurrentLimit(amountOnetimeLimitId, contractId, cardId).isActive){
      resetLimit(type, period, amountOnetimeLimitId, contractId, cardId);
    }
    log("set maxAmount and currentAmmount(don't need otp)");
    Integer newMaxAmount = 100;
    String newLimitBody = createLimitJson(type, period, newMaxAmount, "true", amountOnetimeLimitId).toString();
    put.send("/banking/contracts/"+ contractId + "/cards/"+ cardId + "/limits/" + amountOnetimeLimitId, newLimitBody);
    checkStatus(SC_OK);
    Long increasedMaxAmount = (getCurrentLimit(amountOnetimeLimitId, contractId, cardId).maxAmount.amount + TestHelper.randInt(1, 50));
    log("set new maxAmount > current maxAmount");
    String increasedMaxAmountLimitBody = createLimitJson(type, period,increasedMaxAmount, "true", amountOnetimeLimitId).toString();
    changeLimit(amountOnetimeLimitId, increasedMaxAmountLimitBody, contractId, cardId);
    log("Check current limit after change maxAmount");
    LimitModel limitAfterChange = getCurrentLimit(amountOnetimeLimitId, contractId, cardId);
    assertStartEndTimeFormat(limitAfterChange);
    assertThatLimitCorrect(limitAfterChange, amountOnetimeLimitId, type, period, increasedMaxAmount, Boolean.TRUE);
  }

  protected void decreaseMaxAmountTest(String type, String period) {
    String limitsJson = login(ACCOUNT_URL, user);
    String contractId =  getCardContractId();
    String cardId = getCardId(contractId);    
    limitsJson = get.send("/banking/contracts/"+ contractId + "/cards/"+ cardId + "/limits/");
    LimitsModel limits = JsonHelper.deserializeJson(LimitsModel.class, limitsJson);
    String amountOnetimeLimitId = getLimitIdFromModel(limits, type, period);
    if(getCurrentLimit(amountOnetimeLimitId, contractId, cardId).isActive){
      resetLimit(type, period, amountOnetimeLimitId, contractId, cardId);
    }
    log("set maxAmount and currentAmmount(don't need otp)");
    Integer newMaxAmount = 100;        
    String newLimitBody = createLimitJson(type, period, newMaxAmount, "true", amountOnetimeLimitId).toString();
    put.send("/banking/contracts/"+ contractId + "/cards/"+ cardId + "/limits/" + amountOnetimeLimitId, newLimitBody);
    checkStatus(SC_OK);
    Long decreasedMaxAmount = getCurrentLimit(amountOnetimeLimitId, contractId, cardId).maxAmount.amount - TestHelper.randInt(1, 50);
    log("set new maxAmount < current maxAmount");
    String decreasedMaxAmountLimitBody = createLimitJson(type, period, decreasedMaxAmount, "true", amountOnetimeLimitId).toString();
    changeLimit(amountOnetimeLimitId, decreasedMaxAmountLimitBody, contractId, cardId);
    log("Check current limit after change maxAmount");
    LimitModel limitAfterChange = getCurrentLimit(amountOnetimeLimitId, contractId, cardId);
    assertStartEndTimeFormat(limitAfterChange);
    assertThatLimitCorrect(limitAfterChange, amountOnetimeLimitId, type, period, decreasedMaxAmount, Boolean.TRUE);
  }
  
  protected void sameMaxAmountTest(String type, String period) {
    String limitsJson = login(ACCOUNT_URL, user);
    String contractId =  getCardContractId();
    String cardId = getCardId(contractId);    
    limitsJson = get.send("/banking/contracts/"+ contractId + "/cards/"+ cardId + "/limits/");
    LimitsModel limits = JsonHelper.deserializeJson(LimitsModel.class, limitsJson);
    String amountOnetimeLimitId = getLimitIdFromModel(limits, type, period);
    if(getCurrentLimit(amountOnetimeLimitId, contractId, cardId).isActive){
      resetLimit(type, period, amountOnetimeLimitId, contractId, cardId);
    }
    log("set maxAmount and currentAmmount(don't need otp)");
    Integer newMaxAmount = 100;
    String newLimitBody = createLimitJson(type, period, newMaxAmount, "true", amountOnetimeLimitId).toString();
    put.send("/banking/contracts/"+ contractId + "/cards/"+ cardId + "/limits/" + amountOnetimeLimitId, newLimitBody);
    checkStatus(SC_OK);
    log("set new maxAmount = current maxAmount");
    Long sameMaxAmount = getCurrentLimit(amountOnetimeLimitId, contractId, cardId).maxAmount.amount;
    String sameMaxAmountLimitBody = createLimitJson(type, period, sameMaxAmount, "true", amountOnetimeLimitId).toString();
    put.send("/banking/contracts/"+ contractId + "/cards/"+ cardId + "/limits/" + amountOnetimeLimitId, sameMaxAmountLimitBody);
    checkStatus(SC_OK);
    log("Check current limit after change maxAmount");
    LimitModel limitAfterChange = getCurrentLimit(amountOnetimeLimitId, contractId, cardId);
    assertStartEndTimeFormat(limitAfterChange);
    assertThatLimitCorrect(limitAfterChange, amountOnetimeLimitId, type, period, sameMaxAmount, Boolean.TRUE);
  }

  protected void setMaxAmountTest(String type, String period) {
    String limitsJson = login(ACCOUNT_URL, user);
    String contractId =  getCardContractId();
    String cardId = getCardId(contractId);    
    limitsJson = get.send("/banking/contracts/"+ contractId + "/cards/"+ cardId + "/limits/");
    LimitsModel limits = JsonHelper.deserializeJson(LimitsModel.class, limitsJson);
    String amountOnetimeLimitId = getLimitIdFromModel(limits, type, period);
    log("reset current limit(if isActive == true)");
    if(getCurrentLimit(amountOnetimeLimitId, contractId, cardId).isActive){
      resetLimit(type, "onetime", amountOnetimeLimitId, contractId, cardId);
    }
    log("set maxAmount and currentAmmount(don't need otp)");
    Long newMaxAmount = new Long(100);
    String newLimitBody = createLimitJson(type, period, newMaxAmount, "true", amountOnetimeLimitId).toString();
    put.send("/banking/contracts/"+ contractId + "/cards/"+ cardId + "/limits/" + amountOnetimeLimitId, newLimitBody);
    checkStatus(SC_OK);
    log("Check current limit after change maxAmount");
    LimitModel limitAfterChange = getCurrentLimit(amountOnetimeLimitId, contractId, cardId);
    assertStartEndTimeFormat(limitAfterChange);
    assertThatLimitCorrect(limitAfterChange, amountOnetimeLimitId, type, period, newMaxAmount, Boolean.TRUE);
  }
  
  protected void setCurrentAmountZeroToZeroTest(String type, String period) {
    String limitsJson = login(ACCOUNT_URL, user); 
    String contractId =  getCardContractId();
    String cardId = getCardId(contractId);    
    limitsJson = get.send("/banking/contracts/"+ contractId + "/cards/"+ cardId + "/limits/");
    LimitsModel limits = JsonHelper.deserializeJson(LimitsModel.class, limitsJson);
    String amountDayLimitId = getLimitIdFromModel(limits, type, period);
    log("reset current limit(if isActive == true)");
    if(getCurrentLimit(amountDayLimitId, contractId, cardId).isActive){
      resetLimit(type, period, amountDayLimitId, contractId, cardId);
    }
    log("set maxamount(don't need otp)");
    Long defaultMaxAmount = new Long(100);
    String newLimitBody = createLimitJson(type, period, defaultMaxAmount, "true", amountDayLimitId).toString();
    log("Выставляем maxAmount в 100");
    put.send("/banking/contracts/"+ contractId + "/cards/"+ cardId + "/limits/" + amountDayLimitId, newLimitBody);
    checkStatus(SC_OK);
    LimitModel activatedLimit = getCurrentLimit(amountDayLimitId, contractId, cardId);
    assertStartEndTimeFormat(activatedLimit);
    assertThatLimitCorrect(activatedLimit, amountDayLimitId, type, period, defaultMaxAmount , Boolean.TRUE);
    log("Check current limit after change maxAmount");
    assertThatLimitCorrect(activatedLimit, amountDayLimitId, type, period, defaultMaxAmount, Boolean.TRUE);
    Assert.assertEquals(activatedLimit.currentAmount.amount, new Long(0));
    delete.send("/banking/contracts/"+ contractId + "/cards/"+ cardId + "/limits/" + amountDayLimitId + "/currentAmount");
    checkStatus(SC_OK);
    log("Check current limit after delete limit");
    LimitModel limitAfterREsetCurrentAmount = getCurrentLimit(amountDayLimitId, contractId, cardId);
    assertStartEndTimeFormat(limitAfterREsetCurrentAmount);
    assertThatLimitCorrect(limitAfterREsetCurrentAmount, amountDayLimitId, type, period, defaultMaxAmount , Boolean.TRUE);
    Assert.assertEquals(limitAfterREsetCurrentAmount.currentAmount.amount, new Long(0));
  }

  protected void resetLimitTest(String type, String period) {
    String limitsJson = login(ACCOUNT_URL, user); 
    String contractId =  getCardContractId();
    String cardId = getCardId(contractId);    
    limitsJson = get.send("/banking/contracts/"+ contractId + "/cards/"+ cardId + "/limits/");
    checkStatus(SC_OK);
    LimitsModel limits = (LimitsModel)JsonHelper.deserializeJson(LimitsModel.class, limitsJson);
    String amountDayLimitId = getLimitIdFromModel(limits, type, period);
    if(!getCurrentLimit(amountDayLimitId, contractId, cardId).isActive){
      Integer newMaxAmount = 100;
      String newLimitBody = createLimitJson(type, period, newMaxAmount, "true", amountDayLimitId).toString();
      put.send("/banking/contracts/"+ contractId + "/cards/" + cardId + "/limits/" + amountDayLimitId, newLimitBody);
      checkStatus(SC_OK);
    }
    String resetLimitBody = createLimitJson(type, period, (Integer)null, "false", amountDayLimitId).toString();
    changeLimit(amountDayLimitId, resetLimitBody, contractId, cardId);
    LimitModel limitAfterChange = getCurrentLimit(amountDayLimitId, contractId, cardId);
    assertStartEndTimeFormat(limitAfterChange);
    assertThatLimitCorrect(limitAfterChange, amountDayLimitId, type, period, null, Boolean.FALSE);
  }
  
  protected LimitModel getCurrentLimit(String limitId) {
    String contractId =  getCardContractId();
    String cardId = getCardId(contractId);    
    String  currentLimitJson=  get.send("/banking/contracts/"+ contractId + "/cards/"+ cardId + "/limits/" + limitId);
    checkStatus(SC_OK);
    LimitModel currentLimit = JsonHelper.deserializeJson(LimitModel.class, currentLimitJson);
    return  currentLimit;
  }
  
  protected LimitModel getCurrentLimit(String limitId, String contractId, String cardId) { 
    String  currentLimitJson=  get.send("/banking/contracts/"+ contractId + "/cards/"+ cardId + "/limits/" + limitId);
    checkStatus(SC_OK);
    LimitModel currentLimit = JsonHelper.deserializeJson(LimitModel.class, currentLimitJson);
    return  currentLimit;
  }
  
  protected void assertThatLimitCorrect(LimitModel limit, String id, String type, String period, Long maxAmountValue, Boolean isActive){
    Assert.assertEquals(limit.id, id);
    Assert.assertEquals(limit.type, type);
    Assert.assertEquals(limit.period, period);
    Assert.assertEquals(limit.isActive, isActive);
    if(maxAmountValue != null){
      Assert.assertEquals(limit.maxAmount.amount, maxAmountValue);
    }
  }
  
  protected void assertStartEndTimeFormat(LimitModel limit){
    Assert.assertTrue(DateValidator.isThisDateValid(limit.startTime, DATE_TIME_FORMAT));
    Assert.assertTrue(DateValidator.isThisDateValid(limit.endTime, DATE_TIME_FORMAT));
  }
  
  /*
  private void  checkResetHistory(LimitModel limit){
    if(limit.isActive.equals(Boolean.TRUE)){
      if(limit.resetHistory.items.length <= 0){
        log("ResetHistory list empty ", true);
        Assert.assertTrue(false);
      }
      for(HistoryModel history : limit.resetHistory.items) {
        JsonHelper.IsAllNotNullFieldsPresent(history);
        Assert.assertTrue(DateValidator.isThisDateValid(history.time, "yyyy-MM-dd'T'HH:mm:ssZ"));
        switch (limit.type) {
          case "count": Assert.assertTrue(history.count != null );
            break;
          case "amount": Assert.assertTrue(history.amount != null);;
            break;
        }
      }
    }
  }
  */
}
