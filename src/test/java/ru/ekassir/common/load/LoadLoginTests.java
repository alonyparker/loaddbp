package ru.ekassir.common.load;

import static org.apache.http.HttpStatus.SC_OK;
//import junit.framework.Test;
import static org.junit.Assert.*;
//import static ru.ekassir.dbp.helpers.JsonHelper.deserializeJson;
//import static ru.ekassir.dbp.helpers.JsonHelper.isAllNotNullFieldsPresent;


import org.junit.*;

import ru.ekassir.dbp.models.json.dashboard.DashboardModel;
import ru.ekassir.dbp.models.json.operation.OperationModel;
import ru.ekassir.dbp.models.json.user.LoginModel;

import org.apache.jmeter.protocol.java.sampler.JUnitSampler;
import org.apache.jmeter.threads.*;

public class LoadLoginTests extends OperationsWraps
{


	public LoadLoginTests()
	{
		super();
	}

	@Test
    public void testApp()
    {
		
		//JUnitSampler sampler = new JUnitSampler();
		//String user2 = sampler.getThreadContext().getVariables().get("user");
		String loginJson = login("/user/login", user, password);
		JsonHelper.deserializeJsonWithoutCheck(LoginModel.class, loginJson);
		checkStatus(SC_OK);
		assertTrue(true);
	
    }
	@Test
	  public void dashboardTest(){
	    String dashboardJson = login("/self", user);    
	    DashboardModel dashboardModel = JsonHelper.deserializeJsonWithoutCheck(DashboardModel.class, dashboardJson);    
	    checkStatus(SC_OK);
	    /*JsonHelper.isAllNotNullFieldsPresent(dashboardModel.session.account);
	    JsonHelper.isAllNotNullFieldsPresent(dashboardModel.session.account.owner);
	    JsonHelper.isAllNotNullFieldsPresent(dashboardModel.session.user);*/
	  }
	
	@Test()
	  public void startOperationByContractTest(){
	    login(user);
	    String serviceId = "236";
	    String operationReqJson = createStartOperationJson(serviceId);
	    log("Start operation.\nService: " + serviceId);
	    String operationRespJson = post.send("/banking/contracts/" + getCardContractId() + "/operations", operationReqJson);
	    checkStatus(SC_OK);
	   /* checkOnError(operationRespJson);
	    //checkStatus(SC_OK);
	    OperationModel operationRespModel = JsonHelper.deserializeJson(OperationModel.class, operationRespJson);
	    Assert.assertTrue("\"amount\" field exists in response.", operationRespModel.amount != null);
	    Assert.assertTrue("\"paymentTool\" field exists in response.", operationRespModel.paymentTool != null);   
	    //Для amount приходит правильный editor
	    Assert.assertTrue( "Field with type 'money' and name 'amount' have 'amountEditor'.", getFieldEditorFromMetadata(operationRespModel.__schema, "amount").equals("amountEditor"));
	    //Для paymentTool приходит правильный editor
	    Assert.assertTrue("Field with type 'paymentTool' have required editor 'paymentTools'.", getFieldEditorFromMetadata(operationRespModel.__schema, "paymentTool").equals("paymentTools"));    
	  */
	  }
	
	
	

}
