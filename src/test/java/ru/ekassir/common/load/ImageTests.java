package ru.ekassir.common.load;

import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_NOT_ACCEPTABLE;
import static org.apache.http.HttpStatus.SC_NOT_MODIFIED;
import static org.apache.http.HttpStatus.SC_OK;
import static ru.ekassir.dbp.helpers.HeadersConstants.EKASSIR_JSON;
import static ru.ekassir.dbp.helpers.HeadersConstants.STANDART_GIF;
import static ru.ekassir.dbp.helpers.HeadersConstants.STANDART_JPEG;
import static ru.ekassir.dbp.helpers.HeadersConstants.STANDART_JSON;
import static ru.ekassir.dbp.helpers.HeadersConstants.STANDART_PNG;

import org.junit.Assert;

import ru.ekassir.dbp.helpers.JsonHelper;
import ru.ekassir.dbp.models.json.base.ImageConfigurationModel;
import ru.ekassir.dbp.models.json.operation.ImageDataUriModel;

public class ImageTests extends CommonWraps{
  
  //Загрузка JPEG изображения по url
/*  public void uploadJpgImageTest(String imageUrl, String jpgImagePath){
    uploadImage(imageUrl, jpgImagePath,  "jpeg", STANDART_JPEG, false);
    checkStatus(SC_OK);
    get.setAcceptHeader(STANDART_JPEG).send(imageUrl);
    checkStatus(SC_OK);
    Assert.assertTrue(get.getResponseHeaderValue("Content-Type").equals(STANDART_JPEG));
  }
  
  //Загрузка GIF изображения по url
  public void uploadGifImageTest(String imageUrl, String gifImagePath)  {    
    uploadImage(imageUrl, gifImagePath,  "gif", STANDART_GIF, false); 
    checkStatus(SC_OK);    
    get.setAcceptHeader(STANDART_GIF).send(imageUrl); 
    checkStatus(SC_OK);
    Assert.assertTrue(get.getResponseHeaderValue("Content-Type").equals(STANDART_GIF));
  }
  
  
//Загрузка GIF изображения по url
  public void uploadPngImageTest(String imageUrl, String gifImagePath)  {    
    uploadImage(imageUrl, gifImagePath,  "png", STANDART_PNG, false); 
    checkStatus(SC_OK);    
    get.setAcceptHeader(STANDART_PNG).send(imageUrl); 
    checkStatus(SC_OK);
    Assert.assertTrue(get.getResponseHeaderValue("Content-Type").equals(STANDART_PNG));
  }
  
  //Загрузка  картинки слишком большого размера
  public void uploadGifImageBadSizeTest(String imageUrl, String badSizeImagePath)  {
    uploadImage(imageUrl, badSizeImagePath, "gif", STANDART_GIF, false); 
    checkStatus(SC_BAD_REQUEST);
  }
  public void uploadJpgImageBadSizeTest(String imageUrl, String badSizeImagePath)  {
    uploadImage(imageUrl, badSizeImagePath, "jpeg", STANDART_JPEG, false); 
    checkStatus(SC_BAD_REQUEST);
  }
  public void uploadPngImageBadSizeTest(String imageUrl, String badSizeImagePath)  {
    uploadImage(imageUrl, badSizeImagePath, "png", STANDART_PNG, false); 
    checkStatus(SC_BAD_REQUEST);
  }
  
  public void pngToGifConvertationTest(String imageUrl)  {
    get.setAcceptHeader(STANDART_GIF).send(imageUrl);
    checkStatus(SC_OK);
    Assert.assertTrue(get.getResponseHeaderValue("Content-Type").equals(STANDART_GIF));
   }
  
  public void pngToJpgConvertationTest(String imageUrl)  {
    get.setAcceptHeader(STANDART_JPEG).send(imageUrl); 
    checkStatus(SC_OK);
    Assert.assertTrue(get.getResponseHeaderValue("Content-Type").equals(STANDART_JPEG));
  }
  
  public void gifToPngConvertationTest(String imageUrl)  {
    get.setAcceptHeader(STANDART_PNG).send(imageUrl); 
    checkStatus(SC_OK);
    Assert.assertTrue(get.getResponseHeaderValue("Content-Type").equals(STANDART_PNG));
  }
  
  public void gifToJpgConvertationTest(String imageUrl)  {
    get.setAcceptHeader(STANDART_JPEG).send(imageUrl); 
    checkStatus(SC_OK);
    Assert.assertTrue(get.getResponseHeaderValue("Content-Type").equals(STANDART_JPEG));
  }
  
  public void jpgToGifConvertationTest(String imageUrl)  {
    get.setAcceptHeader(STANDART_GIF).send(imageUrl); 
    checkStatus(SC_OK);
    Assert.assertTrue(get.getResponseHeaderValue("Content-Type").equals(STANDART_GIF));
  }
  
  public void jpgToPngConvertationTest(String imageUrl)  {
    get.setAcceptHeader(STANDART_PNG).send(imageUrl); 
    checkStatus(SC_OK);
    Assert.assertTrue(get.getResponseHeaderValue("Content-Type").equals(STANDART_PNG));
  }
  
  public void uploadJpgImageWithWrongContentTypeTest(String imageUrl)  {
    uploadImage(imageUrl, getProperty("imagePath"),  "jpeg", STANDART_JSON, false); 
    checkStatus(SC_BAD_REQUEST);
  }
  public void uploadGifImageWithWrongContentTypeTest(String imageUrl)  {
    uploadImage(imageUrl, getProperty("imagePath"),  "gif", STANDART_JSON, false); 
    checkStatus(SC_BAD_REQUEST);
  }
  public void uploadPngImageWithWrongContentTypeTest(String imageUrl)  {
    uploadImage(imageUrl, getProperty("imagePath"),  "png", STANDART_JSON, false); 
    checkStatus(SC_BAD_REQUEST);
  }
  
  public void getImageWithWrongAcceptHeaderTest(String imageUrl)  {
    get.setAcceptHeader(STANDART_JSON).send(imageUrl); 
    checkStatus(SC_NOT_ACCEPTABLE);
  }
  
  //Удаление изображения
  public void deleteImageTest(String imageUrl, String jpgImagePath)  {
    //Загрузить собственную картинку
    uploadImage(imageUrl, jpgImagePath, "jpeg", STANDART_JPEG, false); 
    checkStatus(SC_OK);
    get.setAcceptHeader(STANDART_PNG).send(imageUrl); 
    checkStatus(SC_OK);
    //Проверка что картинка загружена
    Assert.assertTrue(get.getResponseHeaderValue("Content-Type").equals(STANDART_PNG));
    //Удалить
    delete.send(imageUrl); checkStatus(SC_OK);
    //Проверка что появилась картинка по умолчанию
    get.setAcceptHeader(STANDART_PNG).send(imageUrl); 
    checkStatus(SC_OK);
    Assert.assertTrue(get.getResponseHeaderValue("Content-Type").equals(STANDART_PNG));
  }
  
  //Получение png изображения указанного размера
  public void getPngImageWithWidthHeightFilter(String imageUrl, String width, String heigth, int httpCode)  {
    get.setAcceptHeader(STANDART_PNG).setUrl(imageUrl).addParam("width", width).addParam("height", heigth).send();
    checkStatus(httpCode);
  }
  //Получение gif изображения указанного размера
  public void getGifImageWithWidthHeightFilter(String imageUrl, String width, String heigth, int httpCode)  {
    get.setAcceptHeader(STANDART_GIF).setUrl(imageUrl).addParam("width", width).addParam("height", heigth).send();
    checkStatus(httpCode);
  }
  //Получение Jpeg изображения указанного размера
  public void getJpegImageWithWidthHeightFilter(String imageUrl, String width, String heigth, int httpCode)  {
    get.setAcceptHeader(STANDART_JPEG).setUrl(imageUrl).addParam("width", width).addParam("height", heigth).send();
    checkStatus(httpCode);
  }
  
  public void getPngImageWithZeroSize(String imageUrl, String zeroSizePngImagePath){
    uploadImage(imageUrl, zeroSizePngImagePath, "png", STANDART_PNG, false); 
    checkStatus(SC_BAD_REQUEST);
  }
  public void getGifImageWithZeroSize(String imageUrl, String zeroSizeGifImagePath){
    uploadImage(imageUrl, zeroSizeGifImagePath, "gif", STANDART_GIF, false); 
    checkStatus(SC_BAD_REQUEST);
  }
  public void getJpgImageWithZeroSize(String imageUrl, String zeroSizeJpgImagePath){
    uploadImage(imageUrl, zeroSizeJpgImagePath, "jpeg", STANDART_JPEG, false); 
    checkStatus(SC_BAD_REQUEST);
  }
 
  
  public void etagTest(String imageUrl)  {    
    get.setAcceptHeader(STANDART_PNG).send(imageUrl);
    checkStatus(SC_OK);
    Assert.assertNotNull(get.getResponseHeaderValue("ETag"), "\"Etag\" header in response exist.");
    String eTag = get.getResponseHeaderValue("ETag");        
    get.setHeader("If-None-Match", eTag).send(imageUrl);
    checkStatus(SC_NOT_MODIFIED);
    refreshGet();
  }
  
  public void getImageWithVndEkassirAcceptHeaderPngFormatTest(String imageUrl)  {
    String image = get.setAcceptHeader(EKASSIR_JSON).setUrl(imageUrl).addParam("width", "40").addParam("height", "40").addParam("format", "png").send(); 
    checkStatus(SC_OK);   
    ImageDataUriModel imageDataUri = JsonHelper.deserializeJson(ImageDataUriModel.class, image);
    Assert.assertTrue(imageDataUri.data.contains("data:image/png;base64"));
  }
 
  public void getImageWithVndEkassirAcceptHeaderJpegFormatTest(String imageUrl)  {
    String image = get.setAcceptHeader(EKASSIR_JSON).setUrl(imageUrl).addParam("format", "jpeg").send(); 
    checkStatus(SC_OK);    
    ImageDataUriModel imageDataUri = JsonHelper.deserializeJson(ImageDataUriModel.class, image);
    Assert.assertTrue(imageDataUri.data.contains("data:image/jpeg;base64"));
  }
  
  public void getImageWithVndEkassirAcceptHeaderGifFormatTest(String imageUrl)  {
    String image = get.setAcceptHeader(EKASSIR_JSON).setUrl(imageUrl).addParam("format", "gif").send(); 
    checkStatus(SC_OK);    
    ImageDataUriModel imageDataUri = JsonHelper.deserializeJson(ImageDataUriModel.class, image);
    Assert.assertTrue(imageDataUri.data.contains("data:image/gif;base64"));
  }

  public void getImageWithImagePngAcceptHeaderTest(String imageUrl){
    get.setAcceptHeader(STANDART_PNG).send(imageUrl);
    checkStatus(SC_OK);
    Assert.assertTrue(get.getResponseHeaderValue("Content-Type").equals("image/png"),"Content-type not image/png");
    Assert.assertTrue(get.getResponseHeaderValue("Content-Disposition").endsWith(".png"), "Content-Disposition ends not .png");
  }
  public void getImageWithImageJpegAcceptHeaderTest(String imageUrl){
    get.setAcceptHeader(STANDART_JPEG).send(imageUrl);
    checkStatus(SC_OK);
    Assert.assertTrue(get.getResponseHeaderValue("Content-Type").equals("image/jpeg"),"Content-type not image/jpeg");
    Assert.assertTrue(get.getResponseHeaderValue("Content-Disposition").endsWith(".jpg"), "Content-Disposition ends not .jpg");
  }
  public void getImageWithImageGifAcceptHeaderTest(String imageUrl){
    get.setAcceptHeader(STANDART_GIF).send(imageUrl);
    checkStatus(SC_OK);
    Assert.assertTrue(get.getResponseHeaderValue("Content-Type").equals("image/gif"),"Content-type not image/gif");
    Assert.assertTrue(get.getResponseHeaderValue("Content-Disposition").endsWith(".gif"), "Content-Disposition ends not .gif");
  }
  
  public void imageConfigurationTest(String imageConfigurationUrl)  {
    String imageConfiguration = login(imageConfigurationUrl, user);
    JsonHelper.deserializeJson(ImageConfigurationModel.class, imageConfiguration);    
  }
  
  public void etagFewTimeTest(String imageUrl, int repeatCount)  {
    get.setAcceptHeader(STANDART_PNG).send(imageUrl);
    Assert.assertNotNull(get.getResponseHeaderValue("ETag"), "\"Etag\" header in response exist.");
    String eTag = get.getResponseHeaderValue("ETag");    
    for(int i = 0; i <= repeatCount; i++) {
      get.setHeader("If-None-Match", eTag).send(imageUrl);
      checkStatus(SC_NOT_MODIFIED);
    }
    refreshGet();
  }*/
}
