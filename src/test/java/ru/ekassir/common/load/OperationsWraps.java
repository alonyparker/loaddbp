package ru.ekassir.common.load;

import org.apache.http.HttpStatus;

import org.json.JSONObject;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import junit.framework.Assert;
import ru.ekassir.dbp.helpers.JsonHelper;
import ru.ekassir.dbp.helpers.TestHelper;
import ru.ekassir.dbp.models.json.base.MetadataModel;
import ru.ekassir.dbp.models.json.currency.RatesListModel;
import ru.ekassir.dbp.models.json.menu.MenuElementModel;
import ru.ekassir.dbp.models.json.menu.MenuModel;
import ru.ekassir.dbp.models.json.operation.OperationModel;
import ru.ekassir.dbp.models.json.operation.TemplateModel;
import ru.ekassir.dbp.models.json.operation.TransferGroupModel;
import ru.ekassir.dbp.models.json.timeline.HistoryEventModel;
import ru.ekassir.dbp.models.json.timeline.TimelineModel;

public class OperationsWraps extends CommonWraps{
  final public String OPERATIONS_URL = "/banking/operations/";
  final public String NEXT = "next";
  final public String COMMIT = "commit";
  final public String REFRESH = "refresh";
  //final public static Long DELAY_FOR_EVENT_APPEARANCE = Long.parseLong(PropertyReader.getProperty("delayForEventAppearance"));
  final public static long DELAY_FOR_EVENT_APPEARANCE = 10000; 
 
  
  //Создание Json для старта операции по serviceId
  public String createStartOperationJson(String serviceId){
    JsonObject operationReq = new JsonObject();
    operationReq.addProperty("serviceId", serviceId);
    String operationReqJson = operationReq.toString();
    return operationReqJson;
  }
  
  //Создание Json для повтора операции 
  public String createStartRepeatOperationJson(String sourceEventId){
    JsonObject operationReq = new JsonObject();
    operationReq.addProperty("sourceEventId", sourceEventId);
    String operationReqJson = operationReq.toString();
    return operationReqJson;
  }
  
  //Создание Json для старта операции по шаблону
  public String createStartOperationByTemplateJson(String templateId){
    JsonObject operationReq = new JsonObject();
    operationReq.addProperty("templateId", templateId);
    String operationReqJson = operationReq.toString();
    return operationReqJson;
  }

  //Добавление confirmation в Json
  public String addConfirmationToOperationJson(String operationJson, String confirmationId){
    JsonObject operationJsonObject = parser.parse(operationJson).getAsJsonObject();
    JsonObject confirmation = new JsonObject();
    confirmation.addProperty("id", confirmationId);
    operationJsonObject.add("confirmation", confirmation);
    return operationJsonObject.toString();
  }
  
  //Добавление рублевой суммы к операции без расчета комиссии
  public String addAmountToOperationJsonWithoutCalculatingCommission(String operationJson, Long sum){    
    JsonObject operationJsonObject = parser.parse(operationJson).getAsJsonObject();    
    JsonObject amount = new JsonObject();    
    amount.addProperty("amount", sum);
    amount.addProperty("currencyCode", "RUB");    
    operationJsonObject.remove("amount");
    operationJsonObject.add("amount", amount);    
    return operationJsonObject.toString();
  }
  
//Добавление суммы к операции без расчета комиссии
  public String addAmountToOperationJsonWithoutCalculatingCommission(String operationJson, Long sum, String currency){    
    JsonObject operationJsonObject = parser.parse(operationJson).getAsJsonObject();    
    JsonObject amount = new JsonObject();    
    amount.addProperty("amount", sum);
    amount.addProperty("currencyCode", currency);    
    operationJsonObject.remove("amount");
    operationJsonObject.add("amount", amount);    
    return operationJsonObject.toString();
  }
  
  //Добавление поля account к операции
  public String addAccountToOperationJson(String operationJson, String account){
    JsonObject operationJsonObject = parser.parse(operationJson).getAsJsonObject();
    operationJsonObject.addProperty("account", account);
    return operationJsonObject.toString();
  } 
  
  //Удаление поля name из операции
  public String removeFieldFromJson(String json, String name){
    JsonObject jsonObject = parser.parse(json).getAsJsonObject();
    jsonObject.remove(name);
    return jsonObject.toString();
  }  
  
  //Добавление рублевой суммы к операции с расчетом комиссии
  public String setAmountResponseJson(String operationId, String operation, Long sum ){
    OperationModel operationModel = JsonHelper.deserializeJson(OperationModel.class, operation);
    Integer paymentTypeInSchemaNumber = -1;
    JsonObject amount = new JsonObject();
    JsonObject paymentTypeInSchema = new JsonObject();
    String paymentTypeValue = "-1";
    amount.addProperty("amount", sum);
    amount.addProperty("currencyCode", "RUB");
    JsonObject operationJsonObject = parser.parse(operation).getAsJsonObject();
    operationJsonObject.remove("amount");
    operationJsonObject.add("amount", amount);
    if(operationJsonObject.get("paymentType") == null) {
      //Если не выставлен paymentType найти его в schema и выставить defaultValue
      for(int i = 0; i < operationModel.__schema.length; ++i) {
        if(operationModel.__schema[i].name.equals("paymentType")) {
          paymentTypeInSchemaNumber = i;
          paymentTypeInSchema = operationJsonObject.get("__schema").getAsJsonArray().get(i).getAsJsonObject();
        }
      }
      if(paymentTypeInSchemaNumber != -1) {
        paymentTypeValue = paymentTypeInSchema.get("defaultValue").getAsString();
        operationJsonObject.addProperty("paymentType", paymentTypeValue);
      }
    }
    operationJsonObject.remove("__schema");
    String operationReqJson = operationJsonObject.toString();
    String operationRespJson = put.send("/banking/operations/" + operationId, operationReqJson);
   // checkStatus(HttpStatus.SC_OK);     
    return operationRespJson;
  }
  
  //Добавление суммы к операции с расчетом комиссии
  public String setAmountResponseJson(String operationId, String operation, Long sum, String currency) {
    OperationModel operationModel = JsonHelper.deserializeJson(OperationModel.class, operation);
    Integer paymentTypeInSchemaNumber = -1;
    JsonObject amount = new JsonObject();
    JsonObject paymentTypeInSchema = new JsonObject();
    String paymentTypeValue = "-1";
    amount.addProperty("amount", sum);
    amount.addProperty("currencyCode", currency);
    JsonObject operationJsonObject = parser.parse(operation).getAsJsonObject();
    operationJsonObject.remove("amount");
    operationJsonObject.add("amount", amount);
    if(operationJsonObject.get("paymentType") == null) {
      //Если не выставлен paymentType найти его в schema и выставить defaultValue
      for(int i = 0; i < operationModel.__schema.length; ++i) {
        if(operationModel.__schema[i].name.equals("paymentType")) {
          paymentTypeInSchemaNumber = i;
          paymentTypeInSchema = operationJsonObject.get("__schema").getAsJsonArray().get(i).getAsJsonObject();
        }
      }
      if(paymentTypeInSchemaNumber != -1) {
        paymentTypeValue = paymentTypeInSchema.get("defaultValue").getAsString();
        operationJsonObject.addProperty("paymentType", paymentTypeValue);
      }
    }
    operationJsonObject.remove("__schema");
    String operationReqJson = operationJsonObject.toString();
    String operationRespJson = put.send("/banking/operations/" + operationId, operationReqJson);
    //checkStatus(HttpStatus.SC_OK);     
    return operationRespJson;
  }

  //Установка платежного инструмента и расчет комиссии
  public String setPaymentToolResponseJson(String operationId, String currentOperationJson, String paymentToolName) {
    Integer paymentToolInSchemaNumber = -1;
    String paymentToolStr = "0";
    JsonObject paymentToolInSchema = new JsonObject();
    OperationModel operationModel = JsonHelper.deserializeJson(OperationModel.class, currentOperationJson);
    JsonParser parser = new JsonParser();
    JsonObject operationJsonObject = parser.parse(currentOperationJson).getAsJsonObject();
    
    //Check if necessary paymentTool equals default paymentTool
    JsonObject defaultPaymentToolJsonObject = operationJsonObject.get("paymentTool").getAsJsonObject(); 
    if(defaultPaymentToolJsonObject.get("displayName").getAsString().equals(paymentToolName)) {
      
      return currentOperationJson;
    }    
    //Find paymentTool index in _schema
    for(int i = 0; i < operationModel.__schema.length; ++i) {
      if(operationModel.__schema[i].name.equals("paymentTool")) {
        paymentToolInSchemaNumber = i;
        paymentToolInSchema = (JsonObject) operationJsonObject.get("__schema").getAsJsonArray().get(i);
      }
    }
    Assert.assertTrue(paymentToolInSchemaNumber != -1);
    
    //Find necessary paymentToll in available values
    for(int i = 0; i < operationModel.__schema[paymentToolInSchemaNumber].availableValues.length; ++i) {
      if(operationModel.__schema[paymentToolInSchemaNumber].availableValues[i].displayName.equals(paymentToolName)) {
        paymentToolStr = ((JsonObject) paymentToolInSchema.get("availableValues").getAsJsonArray().get(i)).get("value").toString();
      }
    }     
    Assert.assertFalse(paymentToolStr.equals("0"));
    
    //Calculate commission
    //operationJsonObject.remove("__schema");
    String operationJson = operationJsonObject.toString();
    operationJson = operationJson.substring(0, operationJson.length() - 1); 
    String calcCommissionReqJson = operationJson + ", \"paymentTool\":" + paymentToolStr + "}"; 
    if(operationModel.amount.amount != 0) {
      String calcCommissionResp = put.send("/banking/operations/" + operationId, calcCommissionReqJson); 
      //checkStatus(HttpStatus.SC_OK);
      return calcCommissionResp;
    }
    return calcCommissionReqJson;    
  }
  
  //Установка платежного инструмента без расчета комиссии
  public String addPaymentToolToJson(String operationId, String json, String paymentToolId) {
    Integer paymentToolIndexInSchema = -1;    
    JsonObject paymentToolInSchema = new JsonObject();    
    OperationModel operationModel = JsonHelper.deserializeJson(OperationModel.class, json);
    JsonObject jsonObject = parser.parse(json).getAsJsonObject();    
    //Если есть ПИ по умолчанию, проверить что он не совпадает с устанавливаемым
    if(jsonObject.get("paymentTool") != null) {
      JsonObject defaultPaymentToolJsonObject = jsonObject.get("paymentTool").getAsJsonObject(); 
      if(defaultPaymentToolJsonObject.get("id").getAsString().equals(paymentToolId)) {
        log("Necessary paymentTool is already set.");
        return json;
      }
    }    
    //Поиск индекса paymentTool в схеме
    for(int i = 0; i < operationModel.__schema.length; ++i) {
      if(operationModel.__schema[i].name.equals("paymentTool")) {
        paymentToolIndexInSchema = i;
        paymentToolInSchema = jsonObject.get("__schema").getAsJsonArray().get(i).getAsJsonObject();
      }
    }
    Assert.assertTrue(paymentToolIndexInSchema != -1);
    
    //Найти нужный инструмент в возможных значениях
    JsonObject paymentToolObject = new JsonObject();
    for(int i = 0; i < operationModel.__schema[paymentToolIndexInSchema].availableValues.length; ++i) {      
      if(paymentToolInSchema.get("availableValues").getAsJsonArray().get(i).getAsJsonObject().get("value").getAsJsonObject().get("id").getAsString().equals(paymentToolId)) {
        paymentToolObject = paymentToolInSchema.get("availableValues").getAsJsonArray().get(i).getAsJsonObject();
      }
    }     
    Assert.assertFalse(paymentToolObject.size() == 0);
    //Добаление нужного платежного инструмента
    jsonObject.add("paymentTool", paymentToolObject.get("value"));        
    return jsonObject.toString();    
  }
  
  //Установка creditPaymentTool без расчета комиссии
  public String addCreditPaymentToolToJson(String operationId, String json, String creditPaymentToolId) {
    Integer creditPaymentToolIndexInSchema = -1;    
    JsonObject creditPaymentToolInSchema = new JsonObject();    
    OperationModel operationModel = JsonHelper.deserializeJson(OperationModel.class, json);
    JsonObject jsonObject = parser.parse(json).getAsJsonObject();    
    //Если есть creditPaymentTool по умолчанию, проверить что он не совпадает с устанавливаемым
    if(jsonObject.get("creditPaymentTool") != null) {
      JsonObject defaultPaymentToolJsonObject = jsonObject.get("creditPaymentTool").getAsJsonObject(); 
      if(defaultPaymentToolJsonObject.get("id").getAsString().equals(creditPaymentToolId)) {
        log("Necessary paymentTool is already set.");
        return json;
      }
    }    
    //Поиск индекса creditPaymentTool в схеме
    for(int i = 0; i < operationModel.__schema.length; ++i) {
      if(operationModel.__schema[i].name.equals("creditPaymentTool")) {
        creditPaymentToolIndexInSchema = i;
        creditPaymentToolInSchema = jsonObject.get("__schema").getAsJsonArray().get(i).getAsJsonObject();
      }
    }
    Assert.assertTrue(creditPaymentToolIndexInSchema != -1);
    
    //Найти нужный инструмент в возможных значениях
    JsonObject creditPaymentToolObject = new JsonObject();
    for(int i = 0; i < operationModel.__schema[creditPaymentToolIndexInSchema].availableValues.length; ++i) {      
      if(creditPaymentToolInSchema.get("availableValues").getAsJsonArray().get(i).getAsJsonObject().get("value").getAsJsonObject().get("id").getAsString().equals(creditPaymentToolId)) {
        creditPaymentToolObject = creditPaymentToolInSchema.get("availableValues").getAsJsonArray().get(i).getAsJsonObject();
      }
    }     
    Assert.assertFalse(creditPaymentToolObject.size() == 0);
    //Добаление нужного инструмента
    jsonObject.add("creditPaymentTool", creditPaymentToolObject.get("value"));        
    return jsonObject.toString();    
  }
  
  //Установка платежного инструмента в transferGroup без расчета комиссии
  public String addPaymentToolToTransferGroupJson(String json, String paymentToolId) {
    Integer paymentToolIndexInSchema = -1;    
    JsonObject paymentToolInSchema = new JsonObject();    
    TransferGroupModel operationModel = JsonHelper.deserializeJson(TransferGroupModel.class, json);
    JsonObject jsonObject = parser.parse(json).getAsJsonObject();    
    //Если есть ПИ по умолчанию, проверить что он не совпадает с устанавливаемым
    if(jsonObject.get("paymentTool") != null) {
      JsonObject defaultPaymentToolJsonObject = jsonObject.get("paymentTool").getAsJsonObject(); 
      if(defaultPaymentToolJsonObject.get("id").getAsString().equals(paymentToolId)) {
        log("Necessary paymentTool is already set.");
        return json;
      }
    }    
    //Поиск индекса paymentTool в схеме
    for(int i = 0; i < operationModel.__schema.length; ++i) {
      if(operationModel.__schema[i].name.equals("paymentTool")) {
        paymentToolIndexInSchema = i;
        paymentToolInSchema = jsonObject.get("__schema").getAsJsonArray().get(i).getAsJsonObject();
      }
    }
    Assert.assertTrue(paymentToolIndexInSchema != -1);
    
    //Найти нужный инструмент в возможных значениях
    JsonObject paymentToolObject = new JsonObject();
    for(int i = 0; i < operationModel.__schema[paymentToolIndexInSchema].availableValues.length; ++i) {      
      if(paymentToolInSchema.get("availableValues").getAsJsonArray().get(i).getAsJsonObject().get("value").getAsJsonObject().get("id").getAsString().equals(paymentToolId)) {
        paymentToolObject = paymentToolInSchema.get("availableValues").getAsJsonArray().get(i).getAsJsonObject();
      }
    }     
    Assert.assertFalse(paymentToolObject.size() == 0);
    //Добаление нужного платежного инструмента
    jsonObject.add("paymentTool", paymentToolObject.get("value"));        
    return jsonObject.toString();    
  }
  
  //Установка creditPaymentTool в transferGroup без расчета комиссии
  public String addCreditPaymentToolToTransferGroupJson(String json, String creditPaymentToolId) {
    Integer creditPaymentToolIndexInSchema = -1;    
    JsonObject creditPaymentToolInSchema = new JsonObject();    
    TransferGroupModel operationModel = JsonHelper.deserializeJson(TransferGroupModel.class, json);
    JsonObject jsonObject = parser.parse(json).getAsJsonObject();    
    //Если есть credit ПИ по умолчанию, проверить что он не совпадает с устанавливаемым
    if(jsonObject.get("creditPaymentTool") != null) {
      JsonObject defaultPaymentToolJsonObject = jsonObject.get("creditPaymentTool").getAsJsonObject(); 
      if(defaultPaymentToolJsonObject.get("id").getAsString().equals(creditPaymentToolId)) {
        log("Necessary creditPaymentTool is already set.");
        return json;
      }
    }    
    //Поиск индекса creditPaymentTool в схеме
    for(int i = 0; i < operationModel.__schema.length; ++i) {
      if(operationModel.__schema[i].name.equals("creditPaymentTool")) {
        creditPaymentToolIndexInSchema = i;
        creditPaymentToolInSchema = jsonObject.get("__schema").getAsJsonArray().get(i).getAsJsonObject();
      }
    }
    Assert.assertTrue(creditPaymentToolIndexInSchema != -1);
    
    //Найти нужный инструмент в возможных значениях
    JsonObject creditPaymentToolObject = new JsonObject();
    for(int i = 0; i < operationModel.__schema[creditPaymentToolIndexInSchema].availableValues.length; ++i) {      
      if(creditPaymentToolInSchema.get("availableValues").getAsJsonArray().get(i).getAsJsonObject().get("value").getAsJsonObject().get("id").getAsString().equals(creditPaymentToolId)) {
        creditPaymentToolObject = creditPaymentToolInSchema.get("availableValues").getAsJsonArray().get(i).getAsJsonObject();
      }
    }     
    Assert.assertFalse(creditPaymentToolObject.size() == 0);
    //Добаление нужного платежного инструмента
    jsonObject.add("creditPaymentTool", creditPaymentToolObject.get("value"));        
    return jsonObject.toString();    
  }
  
  //Получение курса для перевода
  public String addCurrencyRateAndAmount(String transferGroup, Long sum) {
    String target = null, base = null, rateCategory = null;
    Double currencyRate = 1.0;
    Long creditSum = sum;
    JsonObject trGrObject = parser.parse(transferGroup).getAsJsonObject();
    Assert.assertFalse(trGrObject.get("paymentTool") == null);
    Assert.assertFalse(trGrObject.get("paymentTool").getAsJsonObject().get("amount") == null);
    //Получение базовой валюты
    base = trGrObject.get("paymentTool").getAsJsonObject().get("amount").getAsJsonObject().get("currencyCode").getAsString();
    Assert.assertFalse(trGrObject.get("creditPaymentTool") == null);
    Assert.assertFalse(trGrObject.get("creditPaymentTool").getAsJsonObject().get("amount") == null);
    //Получение target валюты
    target = trGrObject.get("creditPaymentTool").getAsJsonObject().get("amount").getAsJsonObject().get("currencyCode").getAsString();
    Assert.assertFalse(trGrObject.get("rateCategory") == null);
    if(!target.equals(base)) {
      //Получение категории курса
      rateCategory = trGrObject.get("rateCategory").getAsString();
      //Получение курса
      String rates = get.setUrl("/currencies/categories/" + rateCategory + "/rates").addParam("target", target).addParam("base", base).send();
      RatesListModel ratesModel = JsonHelper.deserializeJson(RatesListModel.class, rates);
      Assert.assertFalse(ratesModel.items.length == 0);
      currencyRate = ratesModel.items[0].rates.items[0].buy.close;
      creditSum = (long) (sum/currencyRate);
      //Добавление курса и суммы к transferGroup
      transferGroup = addFieldToJson(transferGroup, "currencyRate", currencyRate);
    }
    transferGroup = addAmountToOperationJsonWithoutCalculatingCommission(transferGroup, sum, base);
        
    JsonObject creditAmount = new JsonObject();    
    creditAmount.addProperty("amount", creditSum);
    creditAmount.addProperty("currencyCode", target);    
    trGrObject = parser.parse(transferGroup).getAsJsonObject();
    trGrObject.add("creditAmount", creditAmount);    
    return trGrObject.toString();
  }
  
  //Добавление transferGroup в операцию
  public String addTransferGroupToOperationJson(String operationJson, String transferGroup, String transferGroupName) {
    JsonObject operationJsonObject = parser.parse(operationJson).getAsJsonObject();
    JsonObject transferGroupJsonObject = parser.parse(transferGroup).getAsJsonObject();
    operationJsonObject.add(transferGroupName, transferGroupJsonObject);
    return operationJsonObject.toString();
  }
  
  //Добавление продукта из availableValues по его id
  public String addProductToOperationJson(String json, String productId) {
    Integer productIndexInSchema = -1;
    JsonObject productInSchema = new JsonObject();
    OperationModel operationModel = JsonHelper.deserializeJson(OperationModel.class, json);
    JsonObject jsonObject = parser.parse(json).getAsJsonObject();  
    
    //Если есть продукт по умолчанию, проверить что он не совпадает с устанавливаемым
    if(jsonObject.get("product_type") != null) {
      JsonObject defaultProductJsonObject = jsonObject.get("product_type").getAsJsonObject(); 
      if(defaultProductJsonObject.get("id").getAsString().equals(productId)) {
        log("Necessary product is already set.");
        return json;
      }
    }
    
    //Поиск индекса product_type в схеме
    for(int i = 0; i < operationModel.__schema.length; ++i) {
      if(operationModel.__schema[i].name.equals("product_type")) {
        productIndexInSchema = i;
        productInSchema = jsonObject.get("__schema").getAsJsonArray().get(i).getAsJsonObject();
      }
    }
    Assert.assertTrue(productIndexInSchema != -1);
    
    //Найти нужный продукт в возможных значениях
    JsonObject productObject = new JsonObject();
    for(int i = 0; i < operationModel.__schema[productIndexInSchema].availableValues.length; ++i) {      
      if(productInSchema.get("availableValues").getAsJsonArray().get(i).getAsJsonObject().get("value").getAsJsonObject().get("id").getAsString().equals(productId)) {
        productObject = productInSchema.get("availableValues").getAsJsonArray().get(i).getAsJsonObject();
      }
    }     
    Assert.assertFalse(productObject.size() == 0);
    //Добаление нужного платежного инструмента
    jsonObject.add("product_type", productObject.get("value"));        
    return jsonObject.toString();    
  }
  
  //Получение transferGroup
  public String getTransferGroup(String json) {
    int trGrInSchema = -1;
    JsonObject transferGroupInSchema = new JsonObject();
    OperationModel operationModel = JsonHelper.deserializeJson(OperationModel.class, json);
    JsonObject operationJsonObject = parser.parse(json).getAsJsonObject();
    for(int i = 0; i < operationModel.__schema.length; ++i) {
      if(operationModel.__schema[i].displayName != null) {
        if(operationModel.__schema[i].displayName.equals("transferGroup")) {
          trGrInSchema = i;
          transferGroupInSchema = operationJsonObject.get("__schema").getAsJsonArray().get(i).getAsJsonObject();
        }
      }
    }
    Assert.assertTrue(trGrInSchema != -1);
    return operationJsonObject.get(transferGroupInSchema.get("name").getAsString()).toString();    
  }
  
  //Получение имени transferGroup
  public String getTransferGroupName(String json) {
    int trGrInSchema = -1;
    //JsonObject transferGroupInSchema = new JsonObject();
    OperationModel operationModel = JsonHelper.deserializeJson(OperationModel.class, json);
    //JsonObject operationJsonObject = parser.parse(json).getAsJsonObject();
    for(int i = 0; i < operationModel.__schema.length; ++i) {
      if(operationModel.__schema[i].displayName != null) {
        if(operationModel.__schema[i].displayName.equals("transferGroup")) {
          trGrInSchema = i;
          return operationModel.__schema[i].name;
        }
      }
    }
    Assert.assertTrue(trGrInSchema != -1);
    return null;    
  }
  
  //Проверка присутсвия события в ленте по __historyEventId в последнем ответе проведения операции
  public void eventPresenceInTimelineCheck(String operationJson) {
    boolean eventExistanceFlag = false;
    OperationModel opModel = JsonHelper.deserializeJson(OperationModel.class, operationJson);
    Assert.assertFalse(opModel.__historyEventId == null);
    String historyEventId = opModel.__historyEventId;
    log("Waiting 10 sec.");
    delay(10000);    
    String timeline = get.send("/timeline");
    TimelineModel timelineModel = JsonHelper.deserializeJson(TimelineModel.class, timeline);    
    for(HistoryEventModel event : timelineModel.events.items) {
      if(event.id.equals(historyEventId)) {
        eventExistanceFlag = true;
        log("Event appear in 10 sec.");
      }
    }
    if(eventExistanceFlag == false) {
      log("Waiting 20 sec.");
      delay(20000);    
      timeline = get.send("/timeline");
      timelineModel = JsonHelper.deserializeJson(TimelineModel.class, timeline);    
      for(HistoryEventModel event : timelineModel.events.items) {
        if(event.id.equals(historyEventId)) {
          eventExistanceFlag = true;
          log("Event appear in 30 sec.");
        }
      } 
    }
    if(eventExistanceFlag == false) {
      log("Waiting 30 sec.");
      delay(30000);    
      timeline = get.send("/timeline");
      timelineModel = JsonHelper.deserializeJson(TimelineModel.class, timeline);    
      for(HistoryEventModel event : timelineModel.events.items) {
        if(event.id.equals(historyEventId)) {
          eventExistanceFlag = true;
          log("Event appear in 60 sec.");
        }
      } 
    }
    Assert.assertFalse(eventExistanceFlag == false);
  }
  
  //Получение id события для которого возможен повтор
  public String getEventIdToRepeatOperation(){
    String timeline = get.setUrl("/timeline").addParam("count", "50").send();
   // checkStatus(HttpStatus.SC_OK);    
    TimelineModel timelineModel = JsonHelper.deserializeJson(TimelineModel.class, timeline);
    Assert.assertFalse(timelineModel.events.items.length == 0);
    for(HistoryEventModel event : timelineModel.events.items){
      if(event.canRepeat != null) {
        if(event.canRepeat == true) {
          return event.id;
        }
      }
    }
    Assert.fail("No events with \"true\" value of \"canRepeat\" flag");
    return null;
  }
 
  public String setPaymentToolAccResponseJson(String url, String currentOperationJson, String paymentToolName){
    Integer paymentToolAccInSchemaNumber = -1;
    String paymentToolStr = "0";
    JsonObject paymentToolInSchema = new JsonObject();
    OperationModel operationModel = JsonHelper.deserializeJson(OperationModel.class, currentOperationJson);
    JsonParser parser = new JsonParser();
    JsonObject operationJsonObject = parser.parse(currentOperationJson).getAsJsonObject();
    
    //Check if necessary paymentTool equals default paymentTool
    if(operationJsonObject.get("paytool_acc") != null) {
      JsonObject defaultPaymentToolJsonObject = operationJsonObject.get("paytool_acc").getAsJsonObject(); 
      if(defaultPaymentToolJsonObject.get("displayName").getAsString().equals(paymentToolName)) {
        log("Necessary paymentTool is already set");
        return currentOperationJson;
      }    
    }
    else {
      logWarn("No \"paytool_acc\" by default.");
    }
    //Find paymentTool index in _schema
    for(int i = 0; i < operationModel.__schema.length; ++i) {
      if(operationModel.__schema[i].name.equals("paytool_acc")) {
        paymentToolAccInSchemaNumber = i;
        paymentToolInSchema = (JsonObject) operationJsonObject.get("__schema").getAsJsonArray().get(i);
      }
    }
    Assert.assertTrue(paymentToolAccInSchemaNumber != -1);
    
    //Find necessary paymentToll in available values
    for(int i = 0; i < operationModel.__schema[paymentToolAccInSchemaNumber].availableValues.length; ++i) {
      if(operationModel.__schema[paymentToolAccInSchemaNumber].availableValues[i].displayName.equals(paymentToolName)) {
        paymentToolStr = ((JsonObject) paymentToolInSchema.get("availableValues").getAsJsonArray().get(i)).get("value").toString();                
      }
    }     
    Assert.assertFalse(paymentToolStr.equals("0"));

    //Calculate commission
    //operationJsonObject.remove("__schema");
    String operationJson = operationJsonObject.toString();
    operationJson = operationJson.substring(0, operationJson.length() - 1); 
    String calcCommissionReqJson = operationJson + ", \"paytool_acc\":" + paymentToolStr + "}"; 
    
    if(operationModel.amount.amount != 0) {
      String calcCommissionResp = put.send(url, calcCommissionReqJson);
      //checkStatus(HttpStatus.SC_OK);
      
      return calcCommissionResp;
    }
    return calcCommissionReqJson;
  } 
//  
//  public String setCreditPaymenttooResponseJson(String url, String currentOperationJson, String paymentToolName){
//    Integer paymentToolAccInSchemaNumber = -1;
//    String paymentToolStr = "0";
//    JsonObject paymentToolInSchema = new JsonObject();
//    OperationModel operationModel = JsonHelper.stringToModelWithNulls(OperationModel.class, currentOperationJson);
//    JsonParser parser = new JsonParser();
//    JsonObject operationJsonObject = parser.parse(currentOperationJson).getAsJsonObject();
//    
//    //Check if necessary paymentTool equals default paymentTool
//    JsonObject defaultPaymentToolJsonObject = operationJsonObject.get("creditPaymentTool").getAsJsonObject(); 
//    if(defaultPaymentToolJsonObject.get("displayName").getAsString().equals(paymentToolName)) {
//      Reporter.log("Necessary paymentTool is already set", true);
//      return currentOperationJson;
//    }    
//    //Find paymentTool index in _schema
//    for(int i = 0; i < operationModel.__schema.length; ++i) {
//      if(operationModel.__schema[i].name.equals("creditPaymentTool")) {
//        paymentToolAccInSchemaNumber = i;
//        paymentToolInSchema = (JsonObject) operationJsonObject.get("__schema").getAsJsonArray().get(i);
//      }
//    }
//    Assert.assertTrue(paymentToolAccInSchemaNumber != -1, "No \"creditPaymentTool\" in _schema");
//    
//    //Find necessary paymentToll in available values
//    for(int i = 0; i < operationModel.__schema[paymentToolAccInSchemaNumber].availableValues.length; ++i) {
//      if(operationModel.__schema[paymentToolAccInSchemaNumber].availableValues[i].displayName.equals(paymentToolName)) {
//        paymentToolStr = ((JsonObject) paymentToolInSchema.get("availableValues").getAsJsonArray().get(i)).get("value").toString();                
//      }
//    }     
//    Assert.assertFalse(paymentToolStr.equals("0"), "Payment tool account: No payment tool account with such name");
//
//    //Calculate commission
//    operationJsonObject.remove("__schema");
//    String operationJson = operationJsonObject.toString();
//    operationJson = operationJson.substring(0, operationJson.length() - 1); 
//    String calcCommissionReqJson = operationJson + ", \"creditPaymentTool\":" + paymentToolStr + "}"; 
//    
//    HttpResponse calcCommissionResp = put.send(url, calcCommissionReqJson, "");
//    Assert.assertEquals(calcCommissionResp.getStatusLine().getStatusCode(), HttpStatus.SC_OK);
//    EntityUtils.consume(calcCommissionResp.getEntity());
//    String calcCommissionRespJson = GetLastResponse();
//    return calcCommissionRespJson;
//  } 
// 
  //Удаление _schema чтобы не слать в запросе
  public String deleteSchema(String request){
    JsonParser parser = new JsonParser();
    JsonObject requestJsonObject = parser.parse(request).getAsJsonObject();
    requestJsonObject.remove("__schema"); 
    return requestJsonObject.toString();
  }
  
  //Получение метаданных поля из схемы по его displayName
  public JsonObject getFieldMetadataFromSchema(String operationJson, String displayName) {
    OperationModel operationModel = JsonHelper.deserializeJson(OperationModel.class, operationJson);    
    JsonObject operationJsonObject = parser.parse(operationJson).getAsJsonObject();
    Integer metadataIndex = -1;
    JsonObject metadata = new JsonObject();
    
    for(int i = 0; i < operationModel.__schema.length; ++i) {
      if(operationModel.__schema[i].displayName.equals(displayName)) {
        metadataIndex = i;
        metadata = operationJsonObject.get("__schema").getAsJsonArray().get(i).getAsJsonObject();
        break;
      }
    }
    
    if(metadataIndex == -1) {
        Assert.fail("No " + displayName + " in schema");
    }
    return metadata;
  }
  
  //Получение editors поля из схемы по его имени (name)
  public String getFieldEditorFromMetadata(MetadataModel[] mm, String name) {    
    for(int i = 0; i < mm.length; ++i) {
      if(mm[i].name.equals(name)) {
        //TODO: проверка на дубликаты
        return mm[i].editors[0].id;
      }
    }
    Assert.fail("No \"" + name + "\" in schema");
    return null;
  }
  
//  
  public String choosePaymentTypeForTransfer(String operationJson, String paymentTypeName) {
    String paymentTypeValue = "0";
    JsonParser parser = new JsonParser();
    JsonObject operationJsonObject = parser.parse(operationJson).getAsJsonObject();
    JsonObject metadata = getFieldMetadataFromSchema(operationJson, "Способ перевода");
    //Найти элемент в availableValues с указанным именем и получить значение поля value 
    for(int i = 0; i < metadata.get("availableValues").getAsJsonArray().size() ; ++i) {
      if(((JsonObject) metadata.get("availableValues").getAsJsonArray().get(i)).get("displayName").toString().equals(paymentTypeName)){
        //paymentTypeValue = ((JsonObject) metadata.get("availableValues").getAsJsonArray().get(i)).get("value").toString();
        paymentTypeValue = ((JsonObject) metadata.get("availableValues").getAsJsonArray().get(i)).get("value").getAsString();
        //paymentTypeValue = paymentTypeValue.replaceAll("\\D+", "");
        break;
      }
    }
    if(paymentTypeValue.equals("0")) {
      Assert.fail("No such payment type in available values: " + paymentTypeName);
    }
    //Добавить в json поле с полученным значением
    operationJsonObject.addProperty("paymentType", paymentTypeValue);
    return operationJsonObject.toString();
  }
//  
  public String enterAccountForTransfer(String url, String operationJson, String account) {
    String paymentType = "0";
    JsonParser parser = new JsonParser();   
    JsonObject operationJsonObject = parser.parse(operationJson).getAsJsonObject();
    Assert.assertTrue(operationJsonObject.get("paymentType") != null);
    paymentType = operationJsonObject.get("paymentType").toString();
    paymentType = paymentType.replaceAll("\\D+", "");
    log(paymentType);
    //В зависимости от способа перевода добавляются разные поля
    switch(paymentType) {
      case "17": 
        operationJsonObject.addProperty("account", account);
        break;
      case "18": 
        operationJsonObject.addProperty("pan", account);
        break;
      case "20": 
        operationJsonObject.addProperty("phoneNum", account);
        break;
      case "24": 
        operationJsonObject.addProperty("accountNumber", account);
        break;
      case "273": 
        operationJsonObject.addProperty("qrcode", account);
        break;
      default: Assert.fail("Unknown paymentType!");  
    }
    String newOperationJson = operationJsonObject.toString();
    newOperationJson = addConfirmationToOperationJson(newOperationJson, "refresh");
    newOperationJson = deleteSchema(newOperationJson);
    
    String  refreshResponse = put.send(url, newOperationJson);
   // checkStatus(HttpStatus.SC_OK);
    //String refreshRespJson = GetLastResponse();
    JsonObject refreshRespJsonObject = parser.parse(refreshResponse).getAsJsonObject();
    //Проверка что вместе со статусом 200 не пришла ошибка
    if(refreshRespJsonObject.get("__error") != null) {
      Assert.fail(refreshRespJsonObject.get("__error").getAsJsonObject().get("message").toString());
    }
    
    return refreshResponse;
  }
  
  public String enterBik(String url, String operationJson, String bik) {
    String bikReqJson = addFieldToOperationJson(operationJson, "bik", bik);
    bikReqJson = deleteSchema(bikReqJson);
    bikReqJson = addConfirmationToOperationJson(bikReqJson, "refresh");
    String bikResponse = put.send(url, bikReqJson);
    return bikResponse;
  }
//  
  public String amountForServiceWithCommission(String operationJson, String fieldName, String fieldValue){
    JsonParser parser = new JsonParser();
    JsonObject operationJsonObject = parser.parse(operationJson).getAsJsonObject();
    //Assert.assertTrue(operationJsonObject.get(fieldName) == null, "Нет добавляемого поля в json.");
    operationJsonObject.addProperty(fieldName, fieldValue);
    return operationJsonObject.toString();
  }  
  
  public String setFeeFromTotalValue(String url, String operationJson, Boolean feeFromTotal){
    OperationModel operationModel = JsonHelper.deserializeJson(OperationModel.class, operationJson);
    Assert.assertTrue(operationModel.feeFromTotal != null);
    operationModel.feeFromTotal = feeFromTotal;
    operationModel.total.amount = operationModel.amount.amount;
    String operationReqJson = JsonHelper.serializeModel(operationModel);
    operationReqJson = deleteSchema(operationReqJson);
    String operationResp = put.send(url, operationReqJson);        
    return operationResp;
 }
//  
  public String chooseMobileOperatorResponse(String operationJson, String serviceId){
    JsonParser parser = new JsonParser();    
    OperationModel operationModel = JsonHelper.deserializeJson(OperationModel.class, operationJson);
    String operationId = operationModel.id;
    String mobileOperatorJson = get.setAcceptHeader("application/json").send("/banking/operations/menu?id=" + serviceId);
    //checkStatus(HttpStatus.SC_OK);
    JsonObject mobileOperatorJsonObject = parser.parse(mobileOperatorJson).getAsJsonObject();
    Assert.assertTrue(mobileOperatorJsonObject.get("items").getAsJsonArray().size() != 0);    
    mobileOperatorJsonObject = mobileOperatorJsonObject.get("items").getAsJsonArray().get(0).getAsJsonObject();
    JsonObject operationJsonObject = parser.parse(operationJson).getAsJsonObject();    
    if(operationJsonObject.get("detectedService") == null) {
      operationJsonObject.add("detectedService", mobileOperatorJsonObject);
      return operationJsonObject.toString();
    }
    operationJsonObject.remove("detectedService");    
    operationJsonObject.add("detectedService", mobileOperatorJsonObject);   
    operationJson = operationJsonObject.toString();
    operationJson = addConfirmationToOperationJson(operationJson, "refresh");
    operationJson = put.send("/banking/operations/" + operationId, operationJson);
   // checkStatus(HttpStatus.SC_OK);
    return operationJson;
  }
//  
  public String setRefreshOnChangeFieldValue(String url, String operationJson, String fieldName, String value){
    String reqJson = addFieldToOperationJson(operationJson, fieldName, value);
    reqJson = deleteSchema(reqJson);
    reqJson = addConfirmationToOperationJson(reqJson, "refresh");
    String response = put.send(url, reqJson);
    //checkStatus(HttpStatus.SC_OK);
    
    //Проверка что вместе со статусом 200 не пришла ошибка
    /*if(refreshRespJsonObject.get("__error") != null) {
      Assert.fail(refreshRespJsonObject.get("__error").getAsJsonObject().get("message").toString());
    }*/
  
    return response;
  }
  //Template methods
  public TemplateModel createTemplate(){
    String name = "template" + TestHelper.randInt(0, 999999999);
    String operationId = TestHelper.generateRandonGuid();
    JSONObject newGroup = new JSONObject();
    newGroup.put("name", name);
    newGroup.put("operationId", operationId);
    post.send("/banking/operations/templates", newGroup.toString());
    //checkStatus(HttpStatus.SC_OK);
    TemplateModel createdTemplate = JsonHelper.deserializeJson(TemplateModel.class, post.getLastResponse());
    return createdTemplate;
  }
  
  //Menu methods
  public void checkMenuModel(MenuModel menuModel) {
    String commonScore = menuModel.items[0].score;
    for(MenuElementModel menuElement : menuModel.items){
    	JsonHelper.isAllNotNullFieldsPresent(menuElement);
      Assert.assertEquals(menuElement.score, commonScore, "All menu item have same score");
    }
    //Assert.assertEquals(menuModel.totalCount.intValue(), menuModel.items.length, "Is total count and menu.item lenght same?");
  }
  
  public String getRandomMenuItem(MenuModel menuModel) {
    int randomItemIndex = TestHelper.randInt(0, menuModel.items.length - 1);
    String randomItemId = menuModel.items[randomItemIndex].id;
    if(TestHelper.isInteger(randomItemId)) {
      return randomItemId;
    }else {
      getRandomMenuItem(menuModel);
    }
    return null;
  }
  
  public String addFieldToOperationJson(String operationJson, String fieldName, String fieldValue){
    JsonParser parser = new JsonParser();
    JsonObject operationJsonObject = parser.parse(operationJson).getAsJsonObject();
    //Assert.assertTrue(operationJsonObject.get(fieldName) == null, "Нет добавляемого поля в json.");
    operationJsonObject.addProperty(fieldName, fieldValue);
    return operationJsonObject.toString();
  } 
}
