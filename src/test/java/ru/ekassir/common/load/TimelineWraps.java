package ru.ekassir.common.load;

import static org.apache.http.HttpStatus.SC_OK;
import  ru.ekassir.dbp.helpers.JsonHelper;
import static ru.ekassir.dbp.utils.DateValidator.DATE_TIME_FORMAT;

import java.util.ArrayList;

import org.joda.time.DateTime;
import org.junit.*;

import com.google.gson.JsonParser;

import ru.ekassir.dbp.models.json.operation.SummariesModel;
import ru.ekassir.dbp.models.json.operation.SummaryModel;
import ru.ekassir.dbp.models.json.timeline.DetailedHistoryEventModel;
import ru.ekassir.dbp.models.json.timeline.HistoryEventModel;
import ru.ekassir.dbp.models.json.timeline.TimelineModel;
import ru.ekassir.dbp.utils.DateValidator;

public class TimelineWraps extends CommonWraps{
  protected JsonParser parser = new JsonParser();
  
  public void filterTest(String filter, String count, String user)  {
    login("/account", user);
    get.setUrl("/timeline").addParam("filter", filter).addParam("count", count).send();
    checkStatus(SC_OK);
    TimelineModel timeLineModel = JsonHelper.deserializeJson(TimelineModel.class, get.getLastResponse());
    Assert.assertFalse("\"" + user + "\" have no events in timeline.", timeLineModel.events.items.length == 0);
    for(HistoryEventModel event : timeLineModel.events.items) {
      Assert.assertEquals("Is filtred events contract type=" + filter, event.contract.type, filter);
    }
  }

  public int countFutureEvent(String date, TimelineModel timeline) {
    int futureCount = 0;
    for(HistoryEventModel event : timeline.events.items) {      
      if(DateValidator.stringToDate(event.timestamp, DATE_TIME_FORMAT).isAfter(DateValidator.stringToDate(date, DATE_TIME_FORMAT))) {
        futureCount++;
      }
    }
    return futureCount;
  }

  public int countAmountInSummary(String operationType, SummariesModel summary){
    int amount = 0;
    for(SummaryModel sum : summary.items){
      if(sum.operationType != null && sum.operationType.equals(operationType)){
        amount += sum.amount.amount;
      }
    }
    log("Calculated total amount with operationType: " + operationType + " is "+ amount);
    return amount;
  }

  public boolean isTimelineSortedByDesc(TimelineModel timeline) {
    boolean sorted = true;
    for (int i = 0; i < timeline.events.items.length - 1; i++) {
      DateTime curEvent = DateValidator.stringToDate(timeline.events.items[i].timestamp, DATE_TIME_FORMAT);
      DateTime nextEvent = DateValidator.stringToDate(timeline.events.items[i+1].timestamp, DATE_TIME_FORMAT);      
      if (nextEvent.isAfter(curEvent)) {
        sorted = false;
        log("Events is not sorted by desc.");
        log("Last wrong sorted dates: " + timeline.events.items[i].timestamp + " and " + timeline.events.items[i+1].timestamp);
        break;
      }
    }
    return sorted;
  }

    //startDateStr,endDateStr: DATE_FORMAT
    //dateStr : DATE_TIME_FORMAT
  public boolean isDateInInterval(String startDateStr, String endDateStr, String dateStr) {
    DateTime startDate = DateValidator.stringToDate(startDateStr, DATE_TIME_FORMAT);
    DateTime endDate = DateValidator.stringToDate(endDateStr, DATE_TIME_FORMAT);
    DateTime date = DateValidator.stringToDate(dateStr, DATE_TIME_FORMAT);    
    if(!(date.equals(startDate)) && !(date.equals(endDate))) {
      boolean isDateAfterStartDate = date.isAfter(startDate);
      boolean isDateBeforeEndDate = date.isBefore(endDate);
      return isDateAfterStartDate & isDateBeforeEndDate;
    }
    return true;
  }
  
  public void eventAnalisotor(TimelineModel timelineModel) { 
    ArrayList<String> eventsId = new ArrayList<String>();
    ArrayList<String> duplicates = new ArrayList<String>();
    boolean operation = false;
    boolean contractServiceHistory = false;
    boolean scheduledPayment = false;
    boolean repayment = false;
    boolean fees = false;
    boolean changeStatus = false;
    boolean applications = false;
    boolean promo = false;
    if(timelineModel.events.items.length == 0){
      log("Timeline is empty"); 
      Assert.fail("Timeline is empty");
    }
    for(HistoryEventModel historyEvent : timelineModel.events.items){  
      String historyEventJson = get.send(environmentUrl + "/timeLine/events/" + historyEvent.id);
      checkStatus(SC_OK);
      DetailedHistoryEventModel event = JsonHelper.deserializeJson(DetailedHistoryEventModel.class, historyEventJson);
      Assert.assertEquals(historyEvent.id, event.id);
      Assert.assertTrue(DateValidator.isThisDateValid(historyEvent.timestamp, DATE_TIME_FORMAT));
      String type = event.type;
      if(eventsId.contains(event.id)) {
        log("Event with id:\"" + event.id + "\" have duplicate");
        duplicates.add(event.id);        
      }
      eventsId.add(event.id);
      switch(type) {
        case "operation" : {
          operation = true;
          //Временная фильтрация по operationClass=services для фильтра событий "Заказ карты"
          //Для операций оплаченных наличными и событий из МК может не быть контракта в событии
          if((!event.operationClass.equals("Services"))&&(!event.id.contains("MK"))&&(event.paymentToolType != null)&&(!event.paymentToolType.equalsIgnoreCase("CASH"))) {
        	  Assert.assertFalse("\nNo \"contract\" field in event with \"type\":\"" + type + "\" \"eventId\":\"" + event.id + "\"", event.contract == null);
          }
          Assert.assertFalse("\nNo \"amount\" field in event with \"type\":\"" + type + "\" \"eventId\":\"" + event.id + "\"", event.amount == null);         
          Assert.assertFalse("\nNo \"operationClass\" field in event with \"type\":\"" + type + "\" \"eventId\":\"" + event.id + "\"", event.operationClass == null);
          Assert.assertFalse("\nNo \"operationType\" field in event with \"type\":\"" + type + "\" \"eventId\":\"" + event.id + "\"", event.operationType == null);
          if((event.operationType.equals("debit") || event.operationType.equals("credit"))&&(!event.id.contains("MK"))
              &&(event.paymentToolType != null)&&(!event.paymentToolType.equalsIgnoreCase("CASH"))) {
            if(!event.operationClass.equals("Services")) {
            	Assert.assertFalse("\nNo \"contract\" field in event with \"operationType\":\"" + event.operationType + "\" \"eventId\":\"" + event.id + "\"", event.contract == null);
            }
            Assert.assertTrue("\nNo \"creditContract\" field in event with \"operationType\":\"" + event.operationType + "\"eventId\":\"" + event.id + "\"", event.creditContract == null);
          }
          else if(event.operationType.equals("debitcredit")) {
        	  Assert.assertFalse("\nNo \"contract\" field in event with \"operationType\":\"" + event.operationType + "\" \"eventId\":\"" + event.id + "\"", event.contract == null);
        	  Assert.assertFalse("\nNo \"creditContract\" field in event with \"operationType\":\"" + event.operationType + "\"eventId\":\"" + event.id + "\"", event.creditContract == null);
          }
        }
        break;
        case "contractServiceHistory": {
          contractServiceHistory = true;
          Assert.assertFalse("\nNo \"contract\" field in event with \"type\":\"" + type + "\" \"eventId\":\"" + event.id + "\"", event.contract == null);
        }
        break;
        case "scheduledPayment" : {
          scheduledPayment = true;
          Assert.assertFalse("\nNo \"contract\" field in event with \"type\":\"" + type + "\" \"eventId\":\"" + event.id + "\"", event.contract == null);
        }
        break;
        case "repayment" : {
          repayment = true;
          Assert.assertFalse("\nNo \"contract\" field in event with \"type\":\"" + type + "\" \"eventId\":\"" + event.id + "\"", event.contract == null);
        }
        break;
        case "changeStatus" : {
          changeStatus = true;
          Assert.assertFalse("\nNo \"contract\" field in event with \"type\":\"" + type + "\" \"eventId\":\"" + event.id + "\"", event.contract == null);
        }
        break;
        case "fees": {
          fees = true;
        }
        break;
        case "applications" : {
          applications = true;
          Assert.assertFalse("\nNo \"application\" field in event with \"type\":\"" + type + "\" \"eventId\":\"" + event.id + "\"", event.application == null);
        }
        break;
        case "promo" : {
          promo = true;
          Assert.assertFalse("\nNo \"promo\" field in event with \"type\":\"" + type + "\" \"eventId\":\"" + event.id + "\"", event.promo == null);
        }
        break;
        default : {
          log("Unknown event type.");
        }      
      }
    }
    if(operation == false) {log("Event with type \"operation\" wasn't checked. No such event in timeline.");}
    if(contractServiceHistory == false) {log("Event with type \"contractServiceHistory\" wasn't checked. No such event in timeline.");}
    if(scheduledPayment == false) {log("Event with type \"scheduledPayment\" wasn't checked. No such event in timeline.");}
    if(repayment == false) {log("Event with type \"repayment\" wasn't checked. No such event in timeline.");}
    if(changeStatus == false) {log("Event with type \"changeStatus\" wasn't checked. No such event in timeline.");}
    if(fees == false) {log("Event with type \"fees\" wasn't checked. No such event in timeline.");}
    if(applications == false) {log("Event with type \"applications\" wasn't checked. No such event in timeline.");}
    if(promo == false) {log("Event with type \"promo\" wasn't checked. No such event in timeline.");}
    if(!duplicates.isEmpty()) {
      log("Timeline have dublicates:\n" + duplicates.toString());
    }    
    Assert.assertTrue("\nTimeline have no duplicates: " + duplicates.toString(), duplicates.isEmpty());

  }
}
