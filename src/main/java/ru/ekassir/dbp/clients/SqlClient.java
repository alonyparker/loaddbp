package ru.ekassir.dbp.clients;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert.*;

import junit.framework.Assert;
import ru.ekassir.dbp.helpers.TestHelper;
import ru.ekassir.dbp.utils.PropertyReader;

public class SqlClient {
  static final String JDBC_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";	
  protected PropertyReader propertyReader = new PropertyReader();
  public Connection connection = null;
  Logger logger = LogManager.getLogger(SqlClient.class);
  TestHelper TestHelper = new TestHelper();
  public SqlClient() {
    connectToSqlServer();
  }
  
  public Connection connectToSqlServer() {
    try{
      Class.forName(JDBC_DRIVER);
      TestHelper.log("Connect to SQLServer: " + propertyReader.getProperty("sqlAddress"));      
      TestHelper.log("Login: " + propertyReader.getProperty("sqlUser") + " Password: " + propertyReader.getProperty("sqlPassword"));
      connection = DriverManager.getConnection(propertyReader.getProperty("sqlAddress"), propertyReader.getProperty("sqlUser"), propertyReader.getProperty("sqlPassword"));
      TestHelper.log("Connected database successfully.");      
    }catch(SQLException se){
      se.printStackTrace();
    }catch(Exception e){    
      e.printStackTrace();
    }
    return connection;
  }
  
  public void closeConnection(){
    try{
      TestHelper.log("Closing SQL connection.");      
      if(connection != null)
        connection.close();
      connection = null;
    }catch(Exception e){
      e.printStackTrace();
    }
  }

  public void executeSelectQuery(String query){
    Statement stmt = null;
    ResultSet rs = null; 
    try { 
      stmt = connection.createStatement();
      TestHelper.log("Executing query: " + query);      
      rs = stmt.executeQuery(query);
      ResultSetMetaData rsmd = rs.getMetaData();      
      int numberOfColumns = rsmd.getColumnCount();
      TestHelper.log("");
      for (int i = 1; i <= numberOfColumns; i++) {        
        String columnName = rsmd.getColumnName(i);
        TestHelper.log(columnName);
      }   
      TestHelper.log("");
      while (rs.next()) {
        for (int i = 1; i <= numberOfColumns; i++) {          
          String columnValue = rs.getString(i);
          TestHelper.log(columnValue);
        }        
      }
      TestHelper.log("");
    } catch (SQLException e ) {
        e.printStackTrace();
    } finally {
        if (stmt != null) { try {
          stmt.close();
        } catch (SQLException e) {           
          e.printStackTrace();
        } }
    }
  }
  
  public void executeDeleteQuery(String query){
    Statement stmt = null;
    try {
      stmt = connection.createStatement();
      TestHelper.log("Executing query: " + query);
      stmt.executeUpdate(query);
    } catch (SQLException e ) {
      e.printStackTrace();
    } finally {
      if (stmt != null) { try {
        stmt.close();
      } catch (SQLException e) {        
        e.printStackTrace();
      } }
    }
  }
  
  public String getUserIdByLogin(String loginName) throws SQLException {
    Statement stmt = null;
    ResultSet rs = null;
    String query = "select [IdentityId] from [ekassir.dbp.identityStorage].[dbo].[User] where [Login] = '" + loginName + "'";
    try {
      stmt = connection.createStatement();
      TestHelper.log("Executing query:" + query);
      rs = stmt.executeQuery(query);
      while (rs.next()) {
        String columnValue = rs.getString("IdentityId");
        TestHelper.log(columnValue);
        return columnValue;
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }

  public boolean isUserExists(String externalId) throws SQLException {
    Statement stmt = null;
    ResultSet rs = null;
    String query = "select [IdentityId] from [ekassir.dbp.identityStorage].[dbo].[User] where [ExternalId] = '" + externalId + "'";
    if (connection == null) connection = this.connectToSqlServer();
    try {
      stmt = connection.createStatement();
      TestHelper.log("Executing query:" + query);
      rs = stmt.executeQuery(query);
      if (!rs.isBeforeFirst()) {
      //  boolean x = rs.isBeforeFirst();
        TestHelper.log("User not found in DB");
        rs.close();
        return true;
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
   // boolean x = rs.isAfterLast();
    TestHelper.log("User was found in DB");
    rs.close();
    return false;
  }
  
  @SuppressWarnings("resource")
  public void executeSqlScript(Connection conn, String filePath) throws IOException, SQLException {
    // Delimiter
    String delimiter = ";";

    InputStream contentStream = new FileInputStream(filePath);
    // Create scanner
    Scanner scanner;
    scanner = new Scanner(contentStream).useDelimiter(delimiter);

    // Loop through the SQL file statements
    Statement currentStatement = null;
    while (scanner.hasNext()) {

      // Get statement
      String rawStatement = scanner.next() + delimiter;
      try {
        // Execute statement
        currentStatement = conn.createStatement();
        currentStatement.execute(rawStatement);

      } catch (SQLException e) {
        e.printStackTrace();
        Assert.fail();
      } finally {
        // Release resources
        if (currentStatement != null) {
          try {
            currentStatement.close();
          } catch (SQLException e) {
            e.printStackTrace();
            Assert.fail();
          }
        }
        currentStatement = null;
      }
    }
    scanner.close();
    contentStream.close();
  }

  public void deleteUserFromDb(Connection connection, String userId) throws IOException, SQLException {
    Path filePath = Paths.get("src/main/resources/sql", "deleteUserScript.txt");
    // System.out.print(filePath.toString().replace("\\", "/"));
    List<String> fileContent = new ArrayList<>(Files.readAllLines(filePath, StandardCharsets.UTF_8));
    fileContent.set(0, "");
    fileContent.set(0, "DECLARE @userExternaId nvarchar(128) =" + "'" + userId + "'");
    Files.write(filePath, fileContent, StandardCharsets.UTF_8);
    executeSqlScript(connection, filePath.toString());
  }

  public void deleteUserIfRegistered(String userExternalId) throws SQLException, IOException {
    if (!isUserExists(userExternalId)) {
      deleteUserFromDb(connection, userExternalId);
      TestHelper.log("Executing query DELETE USER");
      this.closeConnection();
    }
  }
}
