package ru.ekassir.dbp.clients;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.directory.api.ldap.model.cursor.CursorException;
import org.apache.directory.api.ldap.model.cursor.EntryCursor;
import org.apache.directory.api.ldap.model.entry.DefaultModification;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.entry.Modification;
import org.apache.directory.api.ldap.model.entry.ModificationOperation;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.message.ModifyDnRequest;
import org.apache.directory.api.ldap.model.message.ModifyDnRequestImpl;
import org.apache.directory.api.ldap.model.message.SearchScope;
import org.apache.directory.api.ldap.model.name.Dn;
import org.apache.directory.api.ldap.model.name.Rdn;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.apache.directory.ldap.client.api.LdapNetworkConnection;

import ru.ekassir.dbp.helpers.TestHelper;
import ru.ekassir.dbp.utils.PropertyReader;

public class OudClient {
  private LdapConnection connection = null;
  protected PropertyReader propertyReader = new PropertyReader();
  TestHelper TestHelper = new TestHelper();
  public OudClient(){
    try{
      TestHelper.log("Connecting to OUD.");
      connection = new LdapNetworkConnection(propertyReader.getProperty("oudHost"), Integer.parseInt(propertyReader.getProperty("oudPort")));
      connection.bind(propertyReader.getProperty("oudUsername"), propertyReader.getProperty("oudPassword"));
      TestHelper.log("Connected to OUD successfully.");
    }catch(Exception e ){
      TestHelper.logError("Failed connect to OUD!");
      e.printStackTrace();
    }
  }
  
  public LdapConnection getConnection() {
    return this.connection;
  }

  public void unBindAndClose(){
    try{
      connection.unBind();
      connection.close();
      connection = null;
    }catch(Exception e){
      TestHelper.logError("Failed to close OUD connection!");
    }
  }
  
  public Entry searchForEntryByAttributeAndValue(String atrTypeToFind, String valueToFind) {
    Entry firstResult = null;
    TestHelper.log("Searching entry with: " + atrTypeToFind + "=" + valueToFind); 
    try {
      EntryCursor cursor = connection.search("dc=letodbotest", "(" + atrTypeToFind + "=" + valueToFind + ")", SearchScope.SUBTREE, "*");
      if(cursor.next()){ 
        firstResult = cursor.get();
      }
    }catch (Exception e) {
        TestHelper.logError("Failed find entry with: " + atrTypeToFind + "=" + valueToFind + " in OUD!");
        e.printStackTrace();
        return firstResult;
    }
    if(firstResult == null) {
      TestHelper.logError("Can't find entry with: " + atrTypeToFind + "=" + valueToFind + " in OUD!");
      return firstResult;
    }
    TestHelper.log("Entry: " + firstResult.toString() + " was found.");
    return firstResult;
  }
  
  public Entry searchForEntryWithEmptyAttribute(String attr) throws LdapException, CursorException, NullPointerException {
    Entry result = null;
    Integer counter = 0;
    TestHelper.log("Searching for entry with empty attr: " + attr);		
    EntryCursor cursor = connection.search("ou=clients,dc=letodbotest", "(&(objectClass=person)(!(" + attr +"=*)))", SearchScope.SUBTREE);		
    while(cursor.next()){
      result = cursor.get();
      TestHelper.log("Entry: " + result.toString() + "was found.");	
      counter++;
    }
    TestHelper.log(counter + " entries without " + attr);
    return result;
  }
  
  public Entry searchLiteClientAndSetHimPassword(String password) throws LdapException, CursorException, NullPointerException {
    Entry currentUser = null;
    TestHelper.log("Searching for entry with: clientType=Lite"); 
    EntryCursor liteClients = connection.search("dc=letodbotest", "(clientType=Lite)", SearchScope.SUBTREE, "*");
    while(liteClients.next()){
      currentUser = liteClients.get();
      if(currentUser.get("userPassword") == null){
        String cn= currentUser.get("cn").get().toString();
        TestHelper.log("set password for Lite client with cn: " + currentUser.get("cn").get().toString());
        findByCnAndModify(cn, "userPassword", password); 
        return currentUser;
      } 
    }
    return currentUser;
  }
  
  public Boolean isEntryWithSuchAttributeExist(String atrTypeToFind, String valueToFind){
    Entry firstResult = null;
    try {
      firstResult = searchForEntryByAttributeAndValue(atrTypeToFind, valueToFind);
    } catch (Exception e) {
      TestHelper.log("Failed find entry  with: " + atrTypeToFind + "=" + valueToFind + "in oud.");
      e.printStackTrace();
    }
    if(firstResult == null) {
      TestHelper.logError("Can't find entry with: " + atrTypeToFind + "=" + valueToFind); 
      return false;
    }
    return true;
  }
	
  public String getAttributeValue(String atrTypeToFind, String valueToFind, String attributeName) {
    Entry firstResult = searchForEntryByAttributeAndValue(atrTypeToFind, valueToFind);
    if(firstResult != null) {
      return firstResult.get(attributeName).get().toString();
    } else {
      return "";
    }
  }

  public void findByCnAndModify(String cn, String atrTypeToModify, String newValue){
    try {
      Modification replaceGn = new DefaultModification(ModificationOperation.REPLACE_ATTRIBUTE, atrTypeToModify, newValue);
      TestHelper.log("Modify attribute \"" + atrTypeToModify + "\" to value \"" + newValue + "\" at entry that found by cn=" + cn);
      connection.modify("cn=" + cn + ",ou=clients,dc=letodbotest", replaceGn);
    }catch(LdapException e){
      TestHelper.logError("Cant modify Entry with cn:" + cn + " cant set " + newValue + " to " + atrTypeToModify);
      return;
    }
    if(!atrTypeToModify.equals("userPassword")) {
      String attrAfterChange = getAttributeValue("cn", cn, atrTypeToModify);
      if(attrAfterChange.equals(newValue)) {
        TestHelper.log("Modify attribute:" + atrTypeToModify + " to value:" + newValue + " at entry that found by cn: " + cn + " success.");
      }else {
        TestHelper.logError("Modify attribute:" + atrTypeToModify + " to value:" + newValue + " at entry that found by cn: " + cn + " fail!");
      }
    }
  }
  
  public void modifyCN(String oldCn,String newCn) {
    try {
      ModifyDnRequest modifyDnRequest=new ModifyDnRequestImpl();
      modifyDnRequest.setName(new Dn("cn=" + oldCn + ",ou=clients,dc=letodbotest"));
      modifyDnRequest.setNewRdn(new Rdn("cn=" + newCn));
      modifyDnRequest.setDeleteOldRdn(true);
      connection.modifyDn(modifyDnRequest);
   }
   catch (LdapException e)
   {
     TestHelper.logError("Cant rename cn:" + oldCn + " to cn:" + newCn);
   }
  }
  public void findBySiebelIDAndModify(String siebelID, String atrTypeToModify, String newValue){
    findEntryByAttributeAndModify("siebelid", siebelID, atrTypeToModify, newValue);
  }
  
  public void findEntryByAttributeAndModify(String atrToFind, String valueToFind, String atrToModify, String newValue){
    TestHelper.log("Modify attribute:" + atrToModify + " to value:" + newValue + " at entry that will be found by attribute:" + atrToFind + " with value:" + valueToFind);
    String resultCn = getAttributeValue(atrToFind, valueToFind, "cn");
    findByCnAndModify(resultCn, atrToModify, newValue);
  }
  
  public void deleteByCn(String cn){
    if(!cn.isEmpty()) {
      try{
        TestHelper.log("Trying delete entry with cn=" + cn);
        ((LdapNetworkConnection) connection).deleteTree("cn=" + cn + ",ou=clients,dc=letodbotest.");
        TestHelper.log("Entry with cn=" + cn + " deleted.");
      }catch(Exception e){
        TestHelper.logError("Entry with cn=" + cn + " not existing in OUD.");
        return;
      }
    }
  }
  //attrName and attrValue must be without characters-to-escap
  //http://social.technet.microsoft.com/wiki/contents/articles/5312.active-directory-characters-to-escape.aspx
  public void deleteByAttr(String attrName, String attrValue){
    TestHelper.log("Trying delete entry with "+ attrName + "=" + attrValue);
    String cn = getAttributeValue(attrName, attrValue ,"cn");
    if(cn != "") {
      deleteByCn(cn);
    }else {
      TestHelper.log("Deleting by attr fail: there is no entry with " + attrName + "=" + attrValue);
    }
  }
  
  public Entry searchLiteClientWithTenDigitCN() {
    Entry currentUser = null;
    TestHelper.log("Searching for entry with: clientType=Lite and ten digit cn."); 
    EntryCursor liteClients = null;
    try {
      liteClients = connection.search("dc=letodbotest", "(clientType=Lite)", SearchScope.SUBTREE, "*");
    }catch (LdapException e){
      TestHelper.logError("Cant find Lite user!");
    }
    try {
      while(liteClients.next()){
        currentUser = liteClients.get();
        Pattern p = Pattern.compile("[0-9]{10}");
        String cn = currentUser.get("cn").get().toString();
        Matcher m = p.matcher(cn);
        if(m.matches()){
          TestHelper.log("Lite client with  ten digit cn cn:" + cn);
          return currentUser;
        } 
      } 
    }catch(Exception e) {
      TestHelper.logError("Cant find Lite user with ten digit cn!");
    }
    return null;
  }
}
