package ru.ekassir.dbp.clients;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.couchbase.client.CouchbaseClient;
import com.couchbase.client.CouchbaseConnectionFactory;
import com.couchbase.client.CouchbaseConnectionFactoryBuilder;

import net.spy.memcached.transcoders.Transcoder;
import ru.ekassir.dbp.helpers.TestHelper;
import ru.ekassir.dbp.utils.NetJsonTranscoder;
import ru.ekassir.dbp.utils.PropertyReader;

public class CouchbaseHelper {
  List<URI> baseList;
  Transcoder<Object> coder = new NetJsonTranscoder(10000);
  CouchbaseConnectionFactoryBuilder cfb;
  CouchbaseConnectionFactory couchbaseConnectionFactory;
  CouchbaseClient client;
  String bucketName;
  String bucketPassword;
  TestHelper TestHelper = new TestHelper();
  protected PropertyReader propertyReader = new PropertyReader();
  
  public CouchbaseHelper(){
    TestHelper.log("Connecting to couchbase.");
    try {
          this.baseList = Arrays.asList(URI.create(propertyReader.getProperty("couchURL")));    
          this.cfb = new CouchbaseConnectionFactoryBuilder();
          cfb.setTranscoder(coder);
          bucketName = propertyReader.getProperty("couchBucket");
          bucketPassword = propertyReader.getProperty("couchBucketPassword");
          this.couchbaseConnectionFactory = cfb.buildCouchbaseConnection(baseList, bucketName, bucketPassword);        
          this.client = new CouchbaseClient(couchbaseConnectionFactory);
    }catch(Exception e ){
          TestHelper.logError("Failed connect to couchbase!");
          e.printStackTrace(); }
  }
  
  public CouchbaseHelper(String url, String bucketName, String bucketPassword){
    TestHelper.log("Connecting to couchbase.");
    try {
          this.baseList = Arrays.asList(URI.create(url));    
          this.cfb = new CouchbaseConnectionFactoryBuilder();
          this.bucketName = bucketName;
          cfb.setTranscoder(coder);          
          this.couchbaseConnectionFactory = cfb.buildCouchbaseConnection(baseList, bucketName, bucketPassword);        
          this.client = new CouchbaseClient(couchbaseConnectionFactory);
    }catch(Exception e ){
      TestHelper.logError("Failed connect to couchbase!");
      e.printStackTrace(); }
  }
  
  /*public CouchbaseHelper(String bucketName, String bucketPassword){
    this(propertyReader.getProperty("couchURL"), bucketName, bucketPassword);
  }*/
  
  public void flushBucket() {
    client.flush();
    TestHelper.log("Bucket \"" + bucketName + "\" flushed.");    
  }
  
  public String getDocumentById(String id) {
    Object document = client.get(id);
    if(document == null) {
      TestHelper.logError("Document: " + id + " doesn't exist!");
      return null;
    }
    TestHelper.log("Document: " + id + "\n" + (String) document);
    return (String) document;
  }
  
  public int getDocumentHashById(String id) {
    String document = getDocumentById(id); 
    if(document != null) {
      TestHelper.log(id + " hash: " + document.hashCode());
      return document.hashCode();
    }    
    return -1;
  }
  
  public void shutdownClient() {
    client.shutdown(3, TimeUnit.SECONDS);
    TestHelper.log("Connection to couchbase shuted down.");
  }
}
