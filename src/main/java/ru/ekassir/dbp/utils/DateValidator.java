package ru.ekassir.dbp.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
//import org.testng.Reporter;

public class DateValidator {
  public final static String DATE_FORMAT = "yyyy-MM-dd";
  public final static String DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";  

  public boolean isThisDateValid(String dateToValidate, String dateFormat){
    if(dateToValidate == null){
      return false;
    }
    SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
    sdf.setLenient(false);
    try {
    //if not valid, it will throw IllegalArgumentException
      DateTimeFormatter dtf = DateTimeFormat.forPattern(dateFormat);
      dtf.parseDateTime(dateToValidate);
    } catch (IllegalArgumentException e) {
     // Reporter.log("Can't convert " + dateToValidate + " to " + dateFormat, true);
      return false;
    }
    return true;
  }
  
  public boolean isDate(String date){
    if(date != null){
      SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
      sdf.setLenient(false);
      try{
        DateTimeFormatter dtf = DateTimeFormat.forPattern(DATE_FORMAT);
        dtf.parseDateTime(date);      
      } catch (IllegalArgumentException e) {      
      return false;
      }
      return true;
    }
    return false;
  }
  
  public  boolean isDateTime(String date){
    if(date != null){
      SimpleDateFormat sdf = new SimpleDateFormat(DATE_TIME_FORMAT);
      sdf.setLenient(false);
      try{
        DateTimeFormatter dtf = DateTimeFormat.forPattern(DATE_TIME_FORMAT);
        dtf.parseDateTime(date);      
      } catch (IllegalArgumentException e) {      
      return false;
      }
      return true;
    }
    return false;
  }
  
  public DateTime stringToDate(String date, String dateFormat){
    SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
    sdf.setLenient(false);
    DateTime convertedDate = null;
    try {
    //if not valid, it will throw IllegalArgumentException
      org.joda.time.format.DateTimeFormatter dtf = DateTimeFormat.forPattern(dateFormat);
      convertedDate = dtf.parseDateTime(date);
    } catch (IllegalArgumentException e) {
    //  Reporter.log("Can't convert " + date + " to " + dateFormat, true);
      return null;
    }
    return convertedDate;
  }
  
  public  String dateToDateTime(String date) {
    Date d;
    SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
    SimpleDateFormat output = new SimpleDateFormat(DATE_TIME_FORMAT);
    try {
      d = sdf.parse(date);
    } catch(ParseException pe) {
     // Reporter.log("Can't parse " + date + " to " + sdf, true);
      return null;
    }
    return output.format(d);
  }
  
  public  String dateTimeToDate(String date) {
    Date d;
    SimpleDateFormat output = new SimpleDateFormat(DATE_FORMAT);
    SimpleDateFormat sdf = new SimpleDateFormat(DATE_TIME_FORMAT);
    try {
      d = sdf.parse(date);
    } catch(ParseException pe) {
     // Reporter.log("Can't parse " + date + " to " + sdf, true);
      return null;
    }
    return output.format(d);
  }
  
  public  Boolean isFirstDateBeforeSecond(String firstDate, String secondDate, String format){
    Date first, second;
    SimpleDateFormat sdf = new SimpleDateFormat(format);
    try {
      first = sdf.parse(firstDate);      
    }catch(ParseException pe) {
     // Reporter.log("Can't parse " + firstDate + " to " + format, true);
      return null;
    }
    try {
      second = sdf.parse(secondDate);      
    }catch(ParseException pe) {
    //  Reporter.log("Can't parse " + secondDate + " to " + format, true);
      return null;
    }
    return first.compareTo(second) <= 0;
  }
}