package ru.ekassir.dbp.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PropertyReader {

  private Properties props = new Properties();
  private Logger logger = LogManager.getRootLogger();
  public void init(String file) throws AssertionError{
    try(InputStreamReader isr = new InputStreamReader(new FileInputStream(file), "UTF-8")) {
        props.load(isr);
    }catch (FileNotFoundException e1) {        
      InputStream inputStream = props.getClass().getResourceAsStream("/" + file);
      try(InputStreamReader isr = new InputStreamReader(inputStream, "UTF-8")) {
        props.load(isr);
      } catch (IOException e) {
          throw new AssertionError("IO error while trying to reach locator's information file: " + e.toString());
      }
    }catch (IOException e1) {
       throw new AssertionError("IO error while trying to reach locator's information file: " + e1.toString());
    }   
  }
  
  public String getProperty(String key) {
    String value = props.getProperty(key);  
    //Выписывает warning для любых параметров кроме user и password
    if((value == null)&&(!key.contains(".user"))&&(!key.contains(".password"))) {
    	logger.warn("No property with key \"" + key + "\" found!");        
        return null;
      }
    return value;
  }
}
