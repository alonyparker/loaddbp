package ru.ekassir.dbp.requester;

import java.io.IOException;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
//import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;

import ru.ekassir.dbp.utils.PropertyReader;

//@SuppressWarnings("deprecation")
public class HttpClientInizializer {
  private HttpClient httpClient;
  private int requestTimeoutInSecond = 360;
  private BasicCookieStore basicCookieStore = new BasicCookieStore();    
  private RequestConfig defaultRequestConfig = RequestConfig.custom()
                                                            .setSocketTimeout(requestTimeoutInSecond*1000)
                                                            .setConnectTimeout(1000)
                                                            .setConnectionRequestTimeout(100)
                                                            .setCookieSpec(CookieSpecs.STANDARD)
                                                            .build();
  private HttpHost proxy = null;  
  
  public HttpClientInizializer(String trustStore, String trustStorePassword, String host, int port) throws SecurityException, IOException {
    if((trustStore != null)&&(trustStorePassword != null)&&(host != null)&&(port != 0)) {
      System.setProperty("javax.net.ssl.trustStore", trustStore);
      System.setProperty("javax.net.ssl.trustStorePassword", trustStorePassword);
      proxy = new HttpHost(host, port);
    }
    httpClient = HttpClients.custom()
                            .setProxy(proxy)
                            //.setSSLHostnameVerifier(new AllowAllHostnameVerifier())
                            .setSSLHostnameVerifier(new NoopHostnameVerifier())
                            .setDefaultCookieStore(basicCookieStore)
                            .setRedirectStrategy(new LaxRedirectStrategy())
                            .setDefaultRequestConfig(defaultRequestConfig)
                            .build();
  }


  public BasicCookieStore getCookieStore(){
    return basicCookieStore;
  }  
  
  public HttpClient getHttpClient(){
    return httpClient;
  } 
}
