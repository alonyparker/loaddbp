package ru.ekassir.dbp.requester;

import static ru.ekassir.dbp.helpers.HeadersConstants.EKASSIR_JSON_UTF8;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

public class GetRequest extends Request{  
  private HttpGet request = new HttpGet();
     
  public GetRequest(HttpClient httpClient, String environmentUrl, int requestTimeoutInSecond, String userAgent, String language){
    super(httpClient, environmentUrl, requestTimeoutInSecond);
    setAcceptHeader(EKASSIR_JSON_UTF8);
    setUserAgentHeader(userAgent);
    setHeader("Accept-Language", language);
  }
  public GetRequest(HttpClient httpClient, String environmentUrl, int requestTimeoutInSecond, String userAgent, String language, LastResponseProvider lr){
	    super(httpClient, environmentUrl, requestTimeoutInSecond, lr);
	    setAcceptHeader(EKASSIR_JSON_UTF8);
	    setUserAgentHeader(userAgent);
	    setHeader("Accept-Language", language);
	  }
  
  public String send(String url){
    return sendRequest(request, url, "get");    
  }
  
  public String send(){
    return sendRequest(request, "get");    
  }
  
  public GetRequest setUrl(String url){
    return (GetRequest) setRequestUrl(request, url);
  }
    
  public GetRequest setHeader(String header, String value) {
    return (GetRequest) setRequestHeader(request, header, value);
  }
  
  public GetRequest addHeader(String header, String value) {
    return (GetRequest) addRequestHeader(request, header, value);
  }
  
  public GetRequest setAcceptHeader(String value) {
    return (GetRequest) setRequestHeader(request, "Accept", value);
  }
  
  public GetRequest setContentTypeHeader(String value) {
    return (GetRequest) setRequestHeader(request, "Content-type", value);
  }
  
  public GetRequest setUserAgentHeader(String value) {
    return (GetRequest) setRequestHeader(request, "User-Agent", value);
  }
  
  public GetRequest setAuthorizationHeader(String value) {
    return (GetRequest) setRequestHeader(request, "Authorization", value);
  }

  public void printRequestInfo(){
    printRequestInfo(request, "get");
  }
  
  public GetRequest addParam(String name, String value){
    String url = request.getURI().toString();
    //TODO: подумать    
    name = name.replaceAll(" ", "%20");
    value = value.replaceAll(" ", "%20");
    if(url.contains("?")){
      url = url + "&" + name + "=" + value;
    }
    else {
      url = url + "?" + name + "=" + value;
    }
    setUrl(url);
    return this;
  }
}
