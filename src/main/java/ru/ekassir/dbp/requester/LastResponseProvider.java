package ru.ekassir.dbp.requester;

public class LastResponseProvider {
	private int lastResponseCode;
	
	public void setLastResponseCode(int code)
	{
		lastResponseCode = code;
	}
	public int getLastResponseCode()
	{
		return this.lastResponseCode;
	}

}
