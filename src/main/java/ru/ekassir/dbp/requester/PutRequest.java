package ru.ekassir.dbp.requester;

import static ru.ekassir.dbp.helpers.HeadersConstants.STANDART_JSON;

import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;

public class PutRequest extends Request{
  private HttpPut request = new HttpPut();
  
  public PutRequest(HttpClient httpClient, String environmentUrl, int requestTimeoutInSecond, String userAgent, String language){
    super(httpClient, environmentUrl, requestTimeoutInSecond);
    setAcceptHeader(STANDART_JSON);
    setContentTypeHeader(STANDART_JSON);
    setUserAgentHeader(userAgent);
    setHeader("Accept-Language", language);
  }
  
  public PutRequest(HttpClient httpClient, String environmentUrl, int requestTimeoutInSecond, String userAgent, String language, LastResponseProvider lr){
	    super(httpClient, environmentUrl, requestTimeoutInSecond, lr);
	    setAcceptHeader(STANDART_JSON);
	    setContentTypeHeader(STANDART_JSON);
	    setUserAgentHeader(userAgent);
	    setHeader("Accept-Language", language);
	  }
 
  public String send(String url) {
    return sendRequest(request, url, "put");    
  }
  
  public String send(String url, String body){
    return sendRequest(request, url, body, "put");
  }
  
  public String send(String url, HttpEntity entity){
    return sendRequest(request, url, entity, "put");
  }
    
  public PutRequest setHeader(String header, String value) {
    return (PutRequest) setRequestHeader(request, header, value);
  }
  
  public PutRequest addHeader(String header, String value) {
    return (PutRequest) addRequestHeader(request, header, value);
  }
  
  public PutRequest setAcceptHeader(String value) {
    return (PutRequest) setRequestHeader(request, "Accept", value);
  }
  
  public PutRequest setContentTypeHeader(String value) {
    return (PutRequest) setRequestHeader(request, "Content-type", value);
  }
  
  public PutRequest setUserAgentHeader(String value) {
    return (PutRequest) setRequestHeader(request, "User-Agent", value);
  }
  
  public PutRequest setAuthorizationHeader(String value) {
    return (PutRequest) setRequestHeader(request, "Authorization", value);
  }
  
  public PutRequest removeHeader(String headerName) {
    return (PutRequest) removeRequestHeader(request, headerName);
  }
  
  public PutRequest removeAuthorizationHeader() {
    return (PutRequest) removeRequestHeader(request, "Authorization");
  }
  
  public PutRequest setBody(String body) {
    return (PutRequest) setRequestBody(request, body);
  }
  
  public PutRequest setBody(HttpEntity entity) {
    return (PutRequest) setRequestBody(request, entity);
  }
  
  public PutRequest clearBody(){
    return setBody("");
  }

  public void printRequestInfo(){
    printRequestInfo(request, "put");
  }
}
