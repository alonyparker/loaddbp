package ru.ekassir.dbp.requester;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import junit.framework.Assert;

import static org.junit.Assert.*;
//import org.testng.Assert;
//import org.testng.Reporter;


import ru.ekassir.dbp.helpers.JsonHelper;
import ru.ekassir.dbp.helpers.ResponseHelper;
import ru.ekassir.dbp.helpers.XmlHelper;

public abstract class Request {
  public static final String PNG = "png";
  public static final String JPEG = "jpeg";
  public static final String GIF = "gif";
  public static final String PDF = "pdf";
  public static final String HTML = "html";
  protected JsonHelper JsonHelper;
  protected Logger logger;
  protected List<NameValuePair> paramsList = new ArrayList<NameValuePair>(1);
  protected XmlHelper XmlHelper = new XmlHelper();
  private HttpClient httpClient = null;
  private HttpResponse response = null;  
  protected ResponseHelper responseHelper;
  protected String environmentUrl;
  protected String userAgent;
  protected String lastResponse;
  protected String lastRequestedUrl;  
  private int requestTimeoutInSecond;
  protected int lastStatusCode;
  protected LastResponseProvider lastResp;
  Header[] headers = null;
  URI uri;
  
  Request(HttpClient httpClient, String environmentUrl, int requestTimeoutInSecond){
    this.httpClient = httpClient;
    this.environmentUrl = environmentUrl;
    this.requestTimeoutInSecond = requestTimeoutInSecond;
    logger = LogManager.getRootLogger(); 
    JsonHelper = new JsonHelper();
    responseHelper = new ResponseHelper();;
  }
  Request(HttpClient httpClient, String environmentUrl, int requestTimeoutInSecond, LastResponseProvider lr){
	    this.httpClient = httpClient;
	    this.environmentUrl = environmentUrl;
	    this.requestTimeoutInSecond = requestTimeoutInSecond;
	    logger = LogManager.getRootLogger(); 
	    JsonHelper = new JsonHelper();
	    responseHelper = new ResponseHelper();
	    this.lastResp = lr;
	  }
  
  //В каждом подклассе должен быть метод "send"
  abstract public String send(String url) throws IOException;   
  
  //SET перезаписывает заголовок
  protected Request setRequestHeader(HttpRequestBase request, String header, String value) {
    if(value != null){
      request.setHeader(header, value);
    }
    return this;
  }
  
  //ADD добавляет заголовок
  protected Request addRequestHeader(HttpRequestBase request, String header, String value) {
    if(value != null){
      request.addHeader(header, value);
    }
    return this;
  }
  
  //Удаление заголовка
  protected Request removeRequestHeader(HttpRequestBase request, String headerName) {
    if(headerName != null){
      request.removeHeaders(headerName);
    }
    return this;
  }
    
  //Вывод заголовков запроса
  public void printRequestHeaders(HttpRequestBase request){
    for(Header header : request.getAllHeaders()) {
      log(header.toString());      
    }
  }
  
  //Установка тела запроса из строки
  protected Request setRequestBody(HttpEntityEnclosingRequestBase request, String body) {
    if(body != null){
      StringEntity params = new StringEntity(body, StandardCharsets.UTF_8);
      request.setEntity(params);
    }
    return this;
  }
  
  //Установка тела запроса из строки
  protected Request setRequestBody(HttpEntityEnclosingRequestBase request, HttpEntity entity) {
    if(entity != null){      
      request.setEntity(entity);
    }
    return this;
  }
  
  //Вывод деталей запроса с телом
  protected void printRequestInfo(HttpEntityEnclosingRequestBase request, String methodName){
    log("Sending " + methodName.toUpperCase() + " request:");   
    log(uri.toString());
    printRequestHeaders(request);
    printRequestBody(request);
  }
  
  //Вывод деталей запроса с телом
  protected void printRequestInfoWithoutBody(HttpEntityEnclosingRequestBase request, String methodName){
    log("Sending " + methodName.toUpperCase() + " request:");    
    log(uri.toString());
    printRequestHeaders(request);    
  }

  
  //Вывод деталей запроса
  protected void printRequestInfo(HttpRequestBase request, String methodName){
    log("Sending " + methodName.toUpperCase() + " request:");
    log(uri.toString());
    printRequestHeaders(request);
  }
  
  //Вывод тела запроса
  protected void printRequestBody(HttpEntityEnclosingRequestBase request){
    String requestString = "", currentLine;    
    BufferedReader br;    
    try {
      br = new BufferedReader (new InputStreamReader(request.getEntity().getContent(),"UTF-8"));
      while ((currentLine = br.readLine()) != null) {
        requestString += currentLine;
      }
      //Делать perttyPrint для json и xml если их стандартные заголовки в Content-Type
      if((!requestString.isEmpty())&&(request.getFirstHeader("Content-Type").getValue().contains("json"))){
        log("Request body:\n" + JsonHelper.toPrettyFormat(requestString));
      }
      else if((!requestString.isEmpty())&&(request.getFirstHeader("Content-Type").getValue().contains("xml"))) {
        log("Request body:\n" + XmlHelper.prettyPrintXml(requestString));
      }
      else {
        log("Request body:\n" + requestString);
      }
    } catch (UnsupportedOperationException | IOException e) {
      logError("UnsupportedOperationException | IOException: " + e.getLocalizedMessage());
    } catch (NullPointerException e) {
      logWarn("Body is empty.\n");
    }    
  }
  
  //Установка URL
  protected Request setRequestUrl(HttpRequestBase request, String url) {
    uri = createURI(url);
    request.setURI(uri);
    return this;
  }
  
  //Отправка запроса у которого уже выставлен URL
  protected String sendRequest(HttpRequestBase request, String methodName){
    printRequestInfo(request, methodName);
    executeRequest(request);
    return lastResponse;
  }
  
  //Отправка запроса у которого уже выставлен URL
  protected String sendRequest(HttpEntityEnclosingRequestBase request, String methodName){
    printRequestInfo(request, methodName);
    executeRequest(request);
    return lastResponse;
  }
  
  //Отправка запроса
  protected String sendRequest(HttpRequestBase request, String url, String methodName){
    //TODO: подумать
    //url = URLEncoder.encode(url, "UTF-8");
    url = url.replaceAll(" ", "%20");
    uri = createURI(url);
    request.setURI(uri);            
    printRequestInfo(request, methodName);
    executeRequest(request);
    return lastResponse;
  }
  
  //Отправка запроса с установвленым телом
  protected String sendRequest(HttpEntityEnclosingRequestBase request, String url, String methodName){
    //TODO: подумать
    //url = URLEncoder.encode(url, "UTF-8");
    url = url.replaceAll(" ", "%20");
    uri = createURI(url);
    request.setURI(uri);        
    printRequestInfo(request, methodName);
    executeRequest(request);
    return lastResponse;
  }
  
  //Отправка запроса с заданием тела из строки 
  protected String sendRequest(HttpEntityEnclosingRequestBase request, String url, String body, String methodName){
    //TODO: подумать
    //url = URLEncoder.encode(url, "UTF-8");
    url = url.replaceAll(" ", "%20");
    uri = createURI(url);
    request.setURI(uri);            
    setRequestBody(request, body);
    printRequestInfo(request, methodName);
    executeRequest(request);
    return lastResponse;
  }
  
  //Отправка запроса с заданием тела как HttpEntity
  protected String sendRequest(HttpEntityEnclosingRequestBase request, String url, HttpEntity entity, String methodName){
    //TODO: подумать
    //url = URLEncoder.encode(url, "UTF-8");
    url = url.replaceAll(" ", "%20");
    uri = createURI(url);
    request.setURI(uri);            
    setRequestBody(request, entity);
    //TODO: подумать, как по другому не выводить тело
    //Не выводить тело если это ByteArrayEntity
    if(entity.getClass().equals(ByteArrayEntity.class)) {
      printRequestInfoWithoutBody(request, methodName);
    }
    else {
      printRequestInfo(request, methodName);
    }
    executeRequest(request);
    return lastResponse;
  }
  
  //Выполнение запроса
  protected void executeRequest(HttpRequestBase request){
    BasicHttpContext context = new BasicHttpContext();
    long startTime = System.currentTimeMillis();
    try {
      response = httpClient.execute(request, context);
    } catch (ClientProtocolException cpe){
      logError("ClientProtocolException: " + cpe.getLocalizedMessage());
    } catch(SocketTimeoutException timeOutException){
      logError("SocketTimeoutException: " + timeOutException.getLocalizedMessage() +"\nRequest executing more than " + requestTimeoutInSecond + " second.");
      //TODO: Подумать
      fail("Request executing more than " + requestTimeoutInSecond + " second");
    } catch(ConnectTimeoutException cte){
      logError("ConnectTimeoutException: " + cte.getLocalizedMessage());
    } catch (IOException ioe) {    
      logError("IOException: " + ioe.getLocalizedMessage());
    }
    //изменть на junit
    //assertFalse(response == null);
    long stopTime = System.currentTimeMillis();
    long elapsedTime = stopTime - startTime;
    log("\nRequest execution: " + elapsedTime/1000.0 + " sec.");

    saveLastRequestedUrl(context, request.getURI().toString());
  //---------
    lastResp.setLastResponseCode(response.getStatusLine().getStatusCode());
    //---------
    saveLastResponse(response);
    lastStatusCode = response.getStatusLine().getStatusCode();
    log("Server response: Status " + response.getStatusLine());
    //assertEquals("Not 200 OK" ,(long)200, (long)response.getStatusLine().getStatusCode());
    //printResponseHeaders();
    log("Print response header");
    //processResponse(response);
    if((!lastResponse.isEmpty())&&(lastResponse != null)) {
      if(response.getFirstHeader("Content-Type").getValue().contains("json")){
        if((lastRequestedUrl.contains("format=")&&((lastRequestedUrl.contains("jpeg"))||(lastRequestedUrl.contains("png"))||(lastRequestedUrl.contains("gif"))))&&lastStatusCode == 200) {
          log("Response is Base64 image.\n");
          //logger.info(lastResponse + "\n");
        }
        else {
          log("Response is:\n" + JsonHelper.toPrettyFormat(lastResponse) + "\n");
        }
      }
      else if(response.getFirstHeader("Content-Type").getValue().contains("xml")){
        log("Response is:\n" + XmlHelper.prettyPrintXml(lastResponse) + "\n");
      }
      else if(response.getFirstHeader("Content-Type").getValue().contains("pdf")){
        //TODO: обрабатывать
        log("Response is PDF file.\n");
       // logger.info(lastResponse + "\n");
      }
      else if(response.getFirstHeader("Content-Type").getValue().contains("html")){
        //TODO: обрабатывать
        log("Response is HTML page.\n");
        //logger.info(lastResponse + "\n");
      }
      else {
        log("Response is:\n" + lastResponse + "\n");        
      }      
    }
    else {
      log("lastResponse was not null");      
    }
    log("end execute header");
    try {
      EntityUtils.consume(response.getEntity());
    } catch (IOException e) {
    	log("IOException: " + e.getLocalizedMessage());
      //logError("IOException: " + e.getLocalizedMessage());
    }    
  }
  public void checkStatus(int statusCode){    
	    Assert.assertEquals( "Wrong response status.", getResponseStatusCode(), statusCode);
	  
	  }
  
  /*public void processResponse(HttpResponse response) {
    try {
      String ct = response.getFirstHeader("Content-type").getValue();
      switch(ct) {
      case "image/png":
        lastResponse = ResponseHelper.responseToImage(response, PNG);
        log("Response is PNG image.");
        break;
      case "image/jpeg":
        lastResponse = ResponseHelper.responseToImage(response, JPEG);
        log("Response is JPEG image.");
        break;
      case "image/gif": 
        lastResponse = ResponseHelper.responseToImage(response, GIF);
        log("Response is GIF image.");
        break; 
      case "application/vnd.ekassir.v1+json; charset=utf-8":
        if((lastRequestedUrl.contains("format")&&(lastRequestedUrl.contains("png")||lastRequestedUrl.contains("jpg")||lastRequestedUrl.contains("gif")))&&(response.getStatusLine().getStatusCode() == 200)){
          lastResponse = ResponseHelper.responseToBase64Image(response);
        }else{
          lastResponse = ResponseHelper.responseToString(response).replace("\uFEFF", "");
        }
        break;
      case "application/vnd.ekassir.v1+json":
        if((lastRequestedUrl.contains("format=")&&((lastRequestedUrl.contains("jpeg"))||(lastRequestedUrl.contains("png"))||lastRequestedUrl.contains("gif"))&&(response.getStatusLine().getStatusCode() == 200))){
          lastResponse = ResponseHelper.responseToBase64Image(response);
        }else{
          lastResponse = ResponseHelper.responseToString(response).replace("\uFEFF", "");
        }
        break;
      case "application/pdf":
        lastResponse = ResponseHelper.responseToImage(response, PDF);
        break;
      case "application/html":
        lastResponse = ResponseHelper.responseToString(response).replace("\uFEFF", "");
        ResponseHelper.responseToImage(response, HTML);
        break;
      case "text/html":
        lastResponse = ResponseHelper.responseToString(response).replace("\uFEFF", "");
        ResponseHelper.responseToImage(response, HTML);
        break;
      default:
        lastResponse = ResponseHelper.responseToString(response).replace("\uFEFF", "");
        break;
      }
    }
    catch(NullPointerException e) {
      try {
        lastResponse = ResponseHelper.responseToString(response).replace("\uFEFF", "");
      } catch (IOException e1) {        
        e1.printStackTrace();
      }
    }
    catch(IOException ioe){
     // Reporter.log(ioe.getLocalizedMessage(), true);
    }
    catch(ParseException pe) {
     // Reporter.log(pe.getLocalizedMessage(), true);
    }
  }*/
  
  //Возврат статуса последнего ответа
  public int getResponseStatusCode(){    
    return lastStatusCode;
  }

  //Вывод заголовков ответа
  public void printResponseHeaders(){
    for(Header header : response.getAllHeaders()) {
      log(header.toString());
    }
  }
  
  //Получение значения заголовка по его имени
  public String getResponseHeaderValue(String name){
    for(Header header : response.getAllHeaders()) {
      if(header.getName().equals(name)) {
        //log("Header \"" + name + "\" value is \"" + header.getValue() + "\"");
        return header.getValue();
      }
    }
    log("No header \"" + name + "\" in response.");    
    return null;
  }
  
  //Создание URI из строки 
  protected URI createURI(String url) {
    URI uri = URI.create(url);
    if(uri.isAbsolute()) {
      return URI.create(url);
    }
    else 
      return URI.create(environmentUrl + url);    
  }
  
  //Сохранение адреса последнего отправленного запроса
  protected void saveLastRequestedUrl(HttpContext context,String url){
    responseHelper.saveLastRequestedUrl(context, url);
    lastRequestedUrl = responseHelper.getLastRequestedUrl();
  }
  
  //Сохранение последнего полученного ответа
  protected void saveLastResponse(HttpResponse response){
    responseHelper.saveLastResponse(response);
    lastResponse = responseHelper.getLastResponse();
  }
  
  //Получение последнего полученного ответа
  public String getLastResponse() {
    return lastResponse;
  }
  
  //Получение адреса последнего отправленного запроса
  public String getLastRequestedUrl() {
    return lastRequestedUrl;
  }
  
  //Обертки для логирования
  public void log(String s) {
    //Reporter.log(s, true);
    logger.info(s);
  }
  
  public void logWarn(String s) {
   // Reporter.log("WARNING: " + s, true);
    logger.warn(s);
  }
  
  public void logError(String s) {
   // Reporter.log("ERROR: " + s, true);
    logger.error(s);
  }
}
