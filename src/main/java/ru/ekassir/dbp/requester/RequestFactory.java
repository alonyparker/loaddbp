package ru.ekassir.dbp.requester;

import org.apache.http.client.HttpClient;

public class RequestFactory {
  private HttpClient httpClient = null; 
  private String environmentUrl;
  private int requestTimeoutInSecond;
  private String userAgent;
  private String language;
  private LastResponseProvider lr;
  public RequestFactory(HttpClient httpClient, String environmentUrl, int requestTimeoutInSecond, String userAgent, String language){
    this.httpClient = httpClient;
    this.environmentUrl = environmentUrl;
    this.requestTimeoutInSecond = requestTimeoutInSecond;
    this.userAgent = userAgent;
    this.language = language;
  }
  public RequestFactory(HttpClient httpClient, String environmentUrl, int requestTimeoutInSecond, String userAgent, String language, LastResponseProvider lr){
	    this.httpClient = httpClient;
	    this.environmentUrl = environmentUrl;
	    this.requestTimeoutInSecond = requestTimeoutInSecond;
	    this.userAgent = userAgent;
	    this.language = language;
	    this.lr = lr;
	  }
  
  public Request getRequest(String type) {
    if(type.equalsIgnoreCase("GET")) {
      return new GetRequest(httpClient, environmentUrl, requestTimeoutInSecond, userAgent, language, lr);
    }
    else if(type.equalsIgnoreCase("POST")) {
      return new PostRequest(httpClient, environmentUrl, requestTimeoutInSecond, userAgent, language, lr);
    }
    else if(type.equalsIgnoreCase("DELETE")) {
      return new DeleteRequest(httpClient, environmentUrl, requestTimeoutInSecond, userAgent, language, lr);
    }
    else if(type.equalsIgnoreCase("PUT")) {
      return new PutRequest(httpClient, environmentUrl, requestTimeoutInSecond, userAgent, language, lr);
    }
    else if(type.equalsIgnoreCase("PATCH")) {
      return new PatchRequest(httpClient, environmentUrl, requestTimeoutInSecond, userAgent, language, lr);
    }
    return null;
  }
 
}
