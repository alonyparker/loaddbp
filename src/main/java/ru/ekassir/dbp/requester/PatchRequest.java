package ru.ekassir.dbp.requester;

import static ru.ekassir.dbp.helpers.HeadersConstants.STANDART_JSON;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPatch;

public class PatchRequest extends Request{
  private HttpPatch request = new HttpPatch();
  
  public PatchRequest(HttpClient httpClient, String environmentUrl, int requestTimeoutInSecond, String userAgent, String language){
    super(httpClient, environmentUrl, requestTimeoutInSecond);
    setAcceptHeader(STANDART_JSON);
    setContentTypeHeader(STANDART_JSON);
    setUserAgentHeader(userAgent);
    setHeader("Accept-Language", language);
  }
  public PatchRequest(HttpClient httpClient, String environmentUrl, int requestTimeoutInSecond, String userAgent, String language, LastResponseProvider lr){
	    super(httpClient, environmentUrl, requestTimeoutInSecond, lr);
	    setAcceptHeader(STANDART_JSON);
	    setContentTypeHeader(STANDART_JSON);
	    setUserAgentHeader(userAgent);
	    setHeader("Accept-Language", language);
	  }
  public String send(String url) {
    return sendRequest(request, url, "patch");    
  }
  
  public String send(String url, String body){
    return sendRequest(request, url, body, "patch");    
  }
    
  public PatchRequest setHeader(String header, String value) {
    return (PatchRequest) setRequestHeader(request, header, value);
  }
  
  public PatchRequest addHeader(String header, String value) {
    return (PatchRequest) addRequestHeader(request, header, value);
  }
  
  public PatchRequest setAcceptHeader(String value) {
    return (PatchRequest) setRequestHeader(request, "Accept", value);
  }
  
  public PatchRequest setContentTypeHeader(String value) {
    return (PatchRequest) setRequestHeader(request, "Content-type", value);
  }
  
  public PatchRequest setUserAgentHeader(String value) {
    return (PatchRequest) setRequestHeader(request, "User-Agent", value);
  }
  
  public PatchRequest setAuthorizationHeader(String value) {
    return (PatchRequest) setRequestHeader(request, "Authorization", value);
  }
  
  public PatchRequest setBody(String body) {
    return (PatchRequest) setRequestBody(request, body);
  }
  
  public PatchRequest clearBody(){
    return setBody("");
  }

  public void printRequestInfo(){
    printRequestInfo(request, "patch");
  }  
}
