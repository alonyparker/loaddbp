package ru.ekassir.dbp.requester;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;

public class DeleteRequest extends Request{
  private HttpDelete request = new HttpDelete();
  
  public DeleteRequest(HttpClient httpClient, String environmentUrl, int requestTimeoutInSecond, String userAgent, String language){
    super(httpClient, environmentUrl, requestTimeoutInSecond);
    setAcceptHeader("application/json");
    setUserAgentHeader(userAgent);
    setHeader("Accept-Language", language);
  }
  public DeleteRequest(HttpClient httpClient, String environmentUrl, int requestTimeoutInSecond, String userAgent, String language, LastResponseProvider lr){
	    super(httpClient, environmentUrl, requestTimeoutInSecond, lr);
	    setAcceptHeader("application/json");
	    setUserAgentHeader(userAgent);
	    setHeader("Accept-Language", language);
	  }
  
  public String send(String url) {
    return sendRequest(request, url, "delete");    
  }
    
  public DeleteRequest setHeader(String header, String value) {
    return (DeleteRequest) setRequestHeader(request, header, value);
  }
  
  public DeleteRequest addHeader(String header, String value) {
    return (DeleteRequest) addRequestHeader(request, header, value);
  }
  
  public DeleteRequest setAcceptHeader(String value) {
    return (DeleteRequest) setRequestHeader(request, "Accept", value);
  }
  
  public DeleteRequest setContentTypeHeader(String value) {
    return (DeleteRequest) setRequestHeader(request, "Content-type", value);
  }
  
  public DeleteRequest setUserAgentHeader(String value) {
    return (DeleteRequest) setRequestHeader(request, "User-Agent", value);
  }
  
  public DeleteRequest setAuthorizationHeader(String value) {
    return (DeleteRequest) setRequestHeader(request, "Authorization", value);
  }
  
  public void printRequestInfo(){
    printRequestInfo(request, "delete");
  }
}
