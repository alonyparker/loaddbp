package ru.ekassir.dbp.requester;

import java.io.UnsupportedEncodingException;

import static ru.ekassir.dbp.helpers.HeadersConstants.EKASSIR_JSON_UTF8;
import static ru.ekassir.dbp.helpers.HeadersConstants.STANDART_JSON;

import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
//import org.testng.Reporter;

public class PostRequest extends Request{  
  private HttpPost request = new HttpPost();
  
  public PostRequest(HttpClient httpClient, String environmentUrl,int requestTimeoutInSecond, String userAgent, String language){
    super(httpClient, environmentUrl, requestTimeoutInSecond);
    setAcceptHeader(EKASSIR_JSON_UTF8);
    setContentTypeHeader(STANDART_JSON);
    setUserAgentHeader(userAgent);
    setHeader("Accept-Language", language);
  }
  public PostRequest(HttpClient httpClient, String environmentUrl,int requestTimeoutInSecond, String userAgent, String language, LastResponseProvider lr){
	    super(httpClient, environmentUrl, requestTimeoutInSecond, lr);
	    setAcceptHeader(EKASSIR_JSON_UTF8);
	    setContentTypeHeader(STANDART_JSON);
	    setUserAgentHeader(userAgent);
	    setHeader("Accept-Language", language);
	  }
  
  public String send(){
    return sendRequest(request, "post");    
  }
  
  public String send(String url) {
    return sendRequest(request, url, "post");    
  }
  
  public String send(String url, String body){
    return sendRequest(request, url, body, "post");    
  }
  
  public PostRequest setUrl(String url){
    return (PostRequest) setRequestUrl(request, url);
  }
    
  public PostRequest setHeader(String header, String value) {
    return (PostRequest) setRequestHeader(request, header, value);
  }
  
  public PostRequest addHeader(String header, String value) {
    return (PostRequest) addRequestHeader(request, header, value);
  }
  
  public PostRequest setAcceptHeader(String value) {
    return (PostRequest) setRequestHeader(request, "Accept", value);
  }
  
  public PostRequest setContentTypeHeader(String value) {
    return (PostRequest) setRequestHeader(request, "Content-type", value);
  }
  
  public PostRequest setUserAgentHeader(String value) {
    return (PostRequest) setRequestHeader(request, "User-Agent", value);
  }
  
  public PostRequest setAuthorizationHeader(String value) {
    return (PostRequest) setRequestHeader(request, "Authorization", value);
  }
  
  public PostRequest setBody(String body) {
    return (PostRequest) setRequestBody(request, body);
  }
  
  public PostRequest clearBody(){
    return setBody("");
  }

  public void printRequestInfo(){
    printRequestInfo(request, "post");
  }
  
  public PostRequest addBodyQueryParam(String param, String value) {
    if((param != null)&&(value != null)) {
      paramsList.add(new BasicNameValuePair(param, value));
    }
    return this;
  }
  
  public void buildParams(){
    try {
      request.setEntity(new UrlEncodedFormEntity(this.paramsList));
    } catch (UnsupportedEncodingException e) {
     // Reporter.log("UnsupportedEncodingException: " + e.getLocalizedMessage(), true);
    }
  }
  
  public PostRequest addParam(String name, String value){
    String url = request.getURI().toString();
    //TODO: подумать    
    name = name.replaceAll(" ", "%20");
    value = value.replaceAll(" ", "%20");
    if(url.contains("?")){
      url = url + "&" + name + "=" + value;
    }
    else {
      url = url + "?" + name + "=" + value;
    }
    setUrl(url);
    return this;
  }
}
