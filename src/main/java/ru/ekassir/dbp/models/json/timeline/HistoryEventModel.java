package ru.ekassir.dbp.models.json.timeline;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.JsonBaseModel;
import ru.ekassir.dbp.models.json.MoneyModel;

public class HistoryEventModel extends JsonBaseModel{
  @NotNull
  public String id;
  @NotNull
  public String type;
  @NotNull
  public String timestamp;
  public Boolean important;
  public StatusModel status;
  @Localized
  public String caption;
  @NotNull @Localized
  public String title;
  @Localized
  public String description;
  //public String repeatOperationId;
  public Boolean canRepeat;
  public String templateId;
  //обязательно для contractServiceHistory, scheduledPayment, repayment, operation, changeStatus
  public ShortContractModel contract;
  public ShortCardModel card;
  public Service service;
  //обязательно для operation, fees
  public MoneyModel amount;
  public MoneyModel feeAmount;
  //обязательно для operation
  public MoneyModel operationAmount;
  public String serviceId;
  //обязательно для operation
  public String operationType;
  //обязательно для operation
  public String operationClass;
  public String categoryId;
  public String defaultCategoryId;
  public ShortContractModel creditContract;
  //обязателен для типа scheduledPayment
  public String term;
  //обязателен для типа scheduledPayment
  public String scheduledPaymentState;
  public Boolean isInsured;
  public String imageName;
  public String mcc;
  public MoneyModel interestDueAmount;
  public MoneyModel feeDueAmount;
  public MoneyModel interestOverdueAmount;
  public MoneyModel feeOverdueAmount;
  public MoneyModel interestOtherAmount;
  public MoneyModel feeOtherAmount;
  public DocumentModel [] availableDocuments;  
  public PromoModel promo;
  public ApplicationModel application;  
  
  public static class ApplicationModel {
    @NotNull
    public String id;
    @NotNull
    public String status;
  }
  
  public static class PromoModel {
   @NotNull
   public String id;
   @NotNull
   public String templateId;
   public Object [] parameters;
  }
  
  public static class StatusModel {
    public String description;
    public String id;
  }
  public static class Service {
    @NotNull
    public String id;
  }
  
  public static class DocumentModel {
    @NotNull
    public String type;
    @NotNull
    public String displayName;
  }
  
  public static class ShortContractModel {
    @NotNull
    public String id;
    @NotNull
    public String displayName;
    @NotNull
    public String type;
  }
  
  public static class ShortCardModel {
    @NotNull
    public String id;
    @NotNull
    public String displayName;
    @NotNull
    public String number;
    @NotNull
    public String paymentSystem;
  }
  
  //ТОЛЬКО для тестов
  public String paymentToolType;
}
