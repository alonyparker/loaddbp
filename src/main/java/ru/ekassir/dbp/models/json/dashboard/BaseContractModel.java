package ru.ekassir.dbp.models.json.dashboard;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.JsonBaseModel;
import ru.ekassir.dbp.models.json.MoneyModel;
import ru.ekassir.dbp.models.json.account.AccountsModel;
import ru.ekassir.dbp.models.json.card.CardsModel;
import ru.ekassir.dbp.models.json.service.ContractServiceStateModel;

public class BaseContractModel extends JsonBaseModel{
  @NotNull
  public String id;
  @NotNull
  public String contractNumber;
  @NotNull
  public String type;
  @NotNull
  public String productId;
  public String productUrl;  
  @NotNull @Localized
  public String name;
  @NotNull
  public String displayName;
  public String imageName;
  public String backgroundColor;
  public Boolean hidden;
  @NotNull
  public String state;
  @NotNull
  public String openedDate;
  @NotNull
  public String signingDate;
  public String closingDate;
  public String closedDate;  
  public Boolean transferAvailable;  
  public AccountsModel accounts;
  public CardsModel cards;  
  public MoneyModel availableLimitAmount;
  public MoneyModel contractAvailableLimitAmount;
  public MoneyModel depositAmount;
  public MoneyModel creditAmount;  
  @NotNull
  public Boolean hasOverdue;  
  public ContractServiceStatesModel contractServiceStates;
  
  public static class ContractServiceStatesModel{
    public ContractServiceStateModel [] items; 
  }
  
  public Boolean cashOutAvailable;
  
  public String backgroundImage;
  //Поле оставлено для совместимости
  public String backgroundImageName;
  
}