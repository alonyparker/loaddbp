package ru.ekassir.dbp.models.json.promo;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class BasePromoModel extends JsonBaseModel{
  @NotNull
  public String id;
  public String personalId;
  public String applicationId;
  @NotNull
  public String type; 
  public Integer sortOrder; 
  @NotNull
  public String category; 
  public String serviceId; 
  @NotNull
  public String startAt;
  @NotNull
  public String expires; 
  
  //@NotNull
  //public String  state; 
  @NotNull
  public String area;
  public String templateId; 
  public ViewModel [] viewSequence;
  public ParameterModel [] parameters;
  public static class ViewModel{
    @NotNull
    public String templateId;
    public String sortOrder;
  }
  public static class ParameterModel{
    @NotNull
    public String name;
    @NotNull
    public String value;
  }

}
