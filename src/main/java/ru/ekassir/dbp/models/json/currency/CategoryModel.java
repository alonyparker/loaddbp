package ru.ekassir.dbp.models.json.currency;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.JsonBaseModel;

public class CategoryModel extends JsonBaseModel{
  @NotNull
  public String id;
  public String parentId;
  @NotNull @Localized
  public String name;
  @NotNull @Localized
  public String description;
  
  //official, exchange
  @NotNull
  public String type;
  public Integer startAmount;
  public Integer endAmount;
}
