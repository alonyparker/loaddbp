package ru.ekassir.dbp.models.json.dashboard;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class ContractsModel  extends JsonBaseModel{
  @NotNull
  public BaseContractModel items[];
  public int count;
  
}
