package ru.ekassir.dbp.models.json.operation;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;
import ru.ekassir.dbp.models.json.MoneyModel;

public class SummaryModel extends JsonBaseModel{
  @NotNull
  public MoneyModel amount;
  
  @NotNull
  public MoneyModel interestAmount;
  public String interestDate;
  
  @NotNull
  public String operationType;
  @NotNull
  public String categoryId;
  @NotNull
  public String startDate;
  @NotNull
  public String endDate;
}
