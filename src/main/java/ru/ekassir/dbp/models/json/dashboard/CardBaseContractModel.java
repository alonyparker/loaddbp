package ru.ekassir.dbp.models.json.dashboard;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.MoneyModel;

public class CardBaseContractModel extends BaseContractModel{
  //@NotNull
  public MoneyModel limitAmount;
  public Boolean hasLimitAmount;
  //@NotNull
  public String paymentDueDate;
  //@NotNull
  public MoneyModel paymentDueAmount;
  public MoneyModel paymentDueAmountRest;
  public MoneyModel debtAmount;
  @Localized
  public Boolean hasDueClaim;
  //Только для почта банка
  public Boolean limitBlocked;
}
