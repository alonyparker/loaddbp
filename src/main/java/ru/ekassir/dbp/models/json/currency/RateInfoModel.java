package ru.ekassir.dbp.models.json.currency;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class RateInfoModel extends JsonBaseModel{
  //Возможные значения: up, down
  public String trend;
  public Double open;
  public Double close;
  public Double max;
  public Double min;
}
