package ru.ekassir.dbp.models.json.product;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class ProductsModel extends JsonBaseModel{
  @NotNull
  public BaseProductModel [] items;
  public int count;
}
