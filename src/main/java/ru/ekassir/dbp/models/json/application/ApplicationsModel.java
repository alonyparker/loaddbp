package ru.ekassir.dbp.models.json.application;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class ApplicationsModel extends JsonBaseModel{
  @NotNull
  public BaseApplicationModel items[];
}
