package ru.ekassir.dbp.models.json.operation;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.JsonBaseModel;
import ru.ekassir.dbp.models.json.MoneyModel;
import ru.ekassir.dbp.models.json.base.MetadataModel;

public class TemplateModel extends JsonBaseModel{
  public String operationId;
  @NotNull
  public String id;
  @NotNull
  public String serviceId;
  @NotNull
  public String menuParentId;
  public String imageName;
  @NotNull @Localized
  public String name;
  public Boolean isFavorite;
  public PaymentToolModel paymentTool;
  @NotNull
  public MoneyModel amount;
  public String createdDate;
  public String lastUsedDate;
  public MoneyModel lastAmount;
  public Integer usageCount;
  public Object parameters;
  //items?
  public String [] groupIds;
  public MetadataModel __schema;
}