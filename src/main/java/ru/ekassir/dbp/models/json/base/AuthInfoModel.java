package ru.ekassir.dbp.models.json.base;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class AuthInfoModel extends JsonBaseModel{
  public String authMethod;
  public Boolean otpConfirmed;
}
