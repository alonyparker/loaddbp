package ru.ekassir.dbp.models.json.thirdPartyCard;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class ThirdPartyCardsModel extends JsonBaseModel{
  @NotNull
  public ThirdPartyCardModel items [];
  public int count;
}
