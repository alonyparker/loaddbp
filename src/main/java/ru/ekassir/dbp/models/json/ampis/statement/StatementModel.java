package ru.ekassir.dbp.models.json.ampis.statement;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class StatementModel extends JsonBaseModel{
  @NotNull
  public String statementTime;
  @NotNull
  public String tin;
  @NotNull
  public Integer documentNumber;
  @NotNull
  public PersonalDataModel personalData;
  public StatementDataModel statementData;
}
