package ru.ekassir.dbp.models.json.base;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class ConfirmationModel extends JsonBaseModel{
  
  public ConfirmationModel(String id) {
    this.id = id;
  }
  
  @NotNull
  public String id; 
  @NotNull
  public String displayName;
  
  public String description;
}