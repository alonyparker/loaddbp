package ru.ekassir.dbp.models.json.topupMethod;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;

public class ExtendTopupmethodModel extends BaseTopupmethodModel{
  public static class InstructionModel{
    @Localized
    public String guidance;
    @Localized
    public String notes;
    public DetailModel [] details;
    public static class DetailModel{
      @NotNull @Localized
      public String name;
      @NotNull @Localized
      public String displayName;
      @Localized
      public String value;
    }
  }
  public InstructionModel [] instructions;
}
