package ru.ekassir.dbp.models.json.subscription;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;
import ru.ekassir.dbp.models.json.MoneyModel;

public class MessageModel extends JsonBaseModel{
  @NotNull
  public String id;
  @NotNull
  public String type;
  @NotNull
  public String from;
  @NotNull
  public String to;
  public String partial;
  @NotNull
  public String title;
  @NotNull
  public String text;
  public Object contract;
  public MoneyModel amount;
  public String historyEventId;
}
