package ru.ekassir.dbp.models.json.password;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.JsonBaseModel;

public class ComplexityGroupModel extends JsonBaseModel{
  @NotNull @Localized
  public String name;
  @NotNull
  public int minRate;
  @NotNull
  public int maxRate;
}
