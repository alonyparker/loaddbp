package ru.ekassir.dbp.models.json.barcode;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class BarcodeModel extends JsonBaseModel{
  @NotNull
  public String url;
  @NotNull
  public Integer width;
  @NotNull
  public Integer height;
  public Integer ttl;
  public String prompt;
}
