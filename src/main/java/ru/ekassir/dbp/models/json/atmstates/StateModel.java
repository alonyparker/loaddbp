package ru.ekassir.dbp.models.json.atmstates;

import javax.validation.constraints.NotNull;

public class StateModel {
  @NotNull
  public String version;
  
  public String header;
  
  @NotNull
  public String template;
  
  public ErrorModel errors[];
  
  @NotNull
  public CallbackModel callbacks[];
  public static class ErrorModel {
    public String code;
    public String name;
    public String value;
  }
}
