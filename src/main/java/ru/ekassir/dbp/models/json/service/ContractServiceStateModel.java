package ru.ekassir.dbp.models.json.service;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.JsonBaseModel;
import ru.ekassir.dbp.models.json.MoneyModel;
import ru.ekassir.dbp.models.json.thirdPartyCard.ThirdPartyCardModel;

public class ContractServiceStateModel extends JsonBaseModel{
  @NotNull
  public String contractId;
  @NotNull
  public Boolean hidden;
//  устарело
//  @NotNull
//  public Integer serviceId;
  @NotNull
  public ServiceModel service;
  public ShortCardModel card;
  @NotNull
  public String serviceType;
//  устарело  
//  @NotNull
//  public String name;
  public String notes;
//  устарело
//  @NotNull
//  public String description;
  @NotNull
  public Boolean isActive;
  public String channelType;
  @NotNull
  public Boolean isAvailable;
  public String availabilityDate;
  @NotNull
  public String activationDate;
  @NotNull
  public String deactivationDate;
  @NotNull
  public String nextExecutionDate;
  public ThirdPartyCardModel thirdPartyCard;
  public InsuranceInfoModel insuranceInfo;
  public MoneyModel feeAmount;
  public static class InsuranceInfoModel{
    @NotNull
    public String programId;
    @NotNull
    public String programName;
    @NotNull
    public MoneyModel feeAmount;
    @NotNull
    public String programUrl;
    @NotNull
    public String contactPhone;
    public MoneyModel coverage;
    public String [] insuredAccidents;
  }
  public static class ServiceModel{
    @NotNull
    public Integer id;
    @NotNull @Localized
    public String name;
    @Localized
    public String description;
    public Boolean blocked;
  }
  
  public static class ShortCardModel{
    @NotNull
    public String id;
    @NotNull @Localized
    public String displayName;
    @NotNull
    public String number;
    @NotNull
    public String paymentSystem;
  }
  public String id;
}
