package ru.ekassir.dbp.models.json.paymentSchedule;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.MoneyModel;
import ru.ekassir.dbp.models.json.base.FeesDueAmountModel;

public class ExtendSheduledPaymentModel extends BaseScheduledPaymentModel{
  public MoneyModel debtPaymentsAmount;
  public MoneyModel dueAmount;
  public MoneyModel debtDueAmount;
  public MoneyModel interestDueAmount;
  public FeesDueAmountModel [] feesDueAmounts;
  public MoneyModel payedAmount;
  public MoneyModel debtPayedAmount;
  public MoneyModel interestPayedAmount;
  public FeesPayedAmountModel []feesPayedAmounts;
  public MoneyModel advanceAmount;
  public MoneyModel restAmount;
  public MoneyModel payedRestAmount;
  public Object overdueInfo;
  
  public static class FeesPayedAmountModel{
    @NotNull
    public Integer amount;
    @NotNull
    public String type;
    @NotNull @Localized
    public String name;
  }    
}
