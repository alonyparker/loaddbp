package ru.ekassir.dbp.models.json.service;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class ServiceStatesModel extends JsonBaseModel{
  @NotNull
  public ContractServiceStateModel items[];
}
