package ru.ekassir.dbp.models.json.currency;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class RatesListModel extends JsonBaseModel{
  @NotNull
  public RatesModel[] items;
  public Integer count;
  public Integer totalCount;
  public Integer pages;
  public Integer page;
}
