package ru.ekassir.dbp.models.json.base;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;
import ru.ekassir.dbp.models.json.user.PersonalAccountModel;
import ru.ekassir.dbp.models.json.user.UserModel;

public class SessionInfoModel extends JsonBaseModel{
  @NotNull
  public PersonalAccountModel account;
  
  @NotNull
  public UserModel user;
  public Claims claims;
  public static class Claims{
    @NotNull
    public String authMethod;
    @NotNull
    public Boolean otpConfirmed;
    @NotNull
    public Boolean transferAvailable;
    @NotNull
    public Boolean hasPlasticCard;
    @NotNull
    public String mkPersonId;
  }
}
