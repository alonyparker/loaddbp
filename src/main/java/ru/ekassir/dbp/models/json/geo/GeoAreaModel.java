package ru.ekassir.dbp.models.json.geo;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class GeoAreaModel extends JsonBaseModel{
  @NotNull
  public GeoPointModel southWest;
  @NotNull
  public GeoPointModel nordEast;
}
