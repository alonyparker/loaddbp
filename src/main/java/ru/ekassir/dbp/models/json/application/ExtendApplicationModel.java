package ru.ekassir.dbp.models.json.application;

import javax.validation.constraints.NotNull;
import ru.ekassir.dbp.models.json.MoneyModel;

public class ExtendApplicationModel extends BaseApplicationModel{
  public OffersModel [] items;
  public AvailableDocumentModel [] availableDocuments;
  public Object __schema;
  
  public static class AvailableDocumentModel{
    @NotNull
    public String type;
    @NotNull
    public String documentName;
  }
  
  public static class OffersModel{
    @NotNull
    public Object product;
    public LoanTermsModel loanTerms;
  }
  public static class LoanTermsModel{
    public Double rate;
    @NotNull
    public PaymentAmountModel paymentAmount;
    public LimitAmountModel limitAmount;
    public TermRangeModel termRange;
  }
  public static class PaymentAmountModel{
    @NotNull
    public MoneyModel min;
    @NotNull
    public MoneyModel max;
    @NotNull
    public MoneyModel value;
  }
  public static class LimitAmountModel{
    @NotNull
    public MoneyModel min;
    @NotNull
    public MoneyModel max;
    @NotNull
    public MoneyModel value;
  }
  public static class TermRangeModel{
    @NotNull
    public Integer min;
    @NotNull
    public Integer max;
    @NotNull
    public Integer value;
  }
}