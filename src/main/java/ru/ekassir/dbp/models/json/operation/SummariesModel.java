package ru.ekassir.dbp.models.json.operation;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class SummariesModel extends JsonBaseModel{
  @NotNull
  public SummaryModel items[];
  @NotNull
  public String type;
}
