package ru.ekassir.dbp.models.json.dashboard;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.MoneyModel;

public class DepositBaseContractModel extends BaseContractModel{
  public String pendingDate;
  public MoneyModel pendingAmount;
  public MoneyModel pendingAmountRest;
  @NotNull
  public Integer depositTerm;
  @NotNull
  public Boolean withdrawalAvailable;
  public MoneyModel minTransferAmount;
  public MoneyModel maxTransferAmount;
  public MoneyModel minTopUpAmount;
  public MoneyModel maxTopUpAmount;
    
  //Устаревшее. Оставлено для совместимости
  public MoneyModel incomingDueAmount;
}
