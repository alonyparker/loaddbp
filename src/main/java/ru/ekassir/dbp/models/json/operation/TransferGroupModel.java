package ru.ekassir.dbp.models.json.operation;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;
import ru.ekassir.dbp.models.json.MoneyModel;
import ru.ekassir.dbp.models.json.base.MetadataModel;

public class TransferGroupModel extends JsonBaseModel{
  @NotNull
  public MoneyModel amount;  
  public PaymentToolModel paymentTool;  
  public PaymentToolModel creditPaymentTool;
  public double currencyRate;
  public String rateCategory;
  @NotNull
  public MetadataModel []__schema;
}
