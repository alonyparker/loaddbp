package ru.ekassir.dbp.models.json.dashboard;

import ru.ekassir.dbp.models.json.MoneyModel;

public class WalletBaseContractModel extends BaseContractModel{  
  public String pendingDate;  
  public MoneyModel maxTopUpAmount;
  public MoneyModel pendingAmount;
  public MoneyModel pendingAmountRest;
  public MoneyModel maxTransferAmount;
}
