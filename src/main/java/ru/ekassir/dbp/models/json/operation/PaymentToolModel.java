package ru.ekassir.dbp.models.json.operation;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.JsonBaseModel;
import ru.ekassir.dbp.models.json.MoneyModel;

public class PaymentToolModel extends JsonBaseModel{
  @NotNull
  public String id;
  @NotNull
  public String type;
  @NotNull @Localized
  public String displayName;
  public String tariff;
  public MoneyModel amount;
  public Card card;
  public String imageName;
  public Boolean disabled;
  public Boolean topUpAvailable;
  public Boolean topUpIsOnline;
  public MoneyModel minTopUpAmount;
  public MoneyModel maxTopUpAmount;
  public Boolean withdrawAvailable;
  public MoneyModel operationMaxAmount;
  public MoneyModel operationMinAmount;
  public Contract contract;
  public Account account;
  
  public static class Contract{
    @NotNull
    public String id;
  }
  
  public static class Card{
    @NotNull
    public String id;
    @NotNull
    public String number;
    @NotNull
    public String paymentSystem;
    @NotNull
    public String expiryDate;
  }
  
  public static class Account{
    @NotNull
    public String id;
    @NotNull
    public String number;
  }
  
  public static class SuggestedAmount{
    @NotNull
    public String id;
    @NotNull @Localized
    public String displayName;
    @Localized
    public String description;
    @NotNull
    public MoneyModel amount;
  }
}
