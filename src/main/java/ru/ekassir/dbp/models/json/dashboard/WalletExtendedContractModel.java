package ru.ekassir.dbp.models.json.dashboard;

import ru.ekassir.dbp.models.json.MoneyModel;
import ru.ekassir.dbp.models.json.topupMethod.BaseTopupmethodModel;

public class WalletExtendedContractModel extends WalletBaseContractModel{
  public MoneyModel maxExpenseAmount;
  public MoneyModel maxCashOutAmount;
  public BaseTopupmethodModel [] topUpMethods;
  
  public MoneyModel holdAmount;
}
