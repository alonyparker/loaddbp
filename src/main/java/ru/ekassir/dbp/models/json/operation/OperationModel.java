package ru.ekassir.dbp.models.json.operation;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.JsonBaseModel;
import ru.ekassir.dbp.models.json.MoneyModel;
import ru.ekassir.dbp.models.json.base.ConfirmationModel;
import ru.ekassir.dbp.models.json.base.MetadataModel;

public class OperationModel extends JsonBaseModel{
  @NotNull
  public String id;
  public String serviceId;
  public String templateId;
  public String sourceEventId;
  public PaymentToolModel paymentTool;
  @NotNull @Localized
  public String displayName;
  @Localized
  public String description;
  public MoneyModel amount;
  public MoneyModel fee;
  public MoneyModel total;
  public Boolean feeFromTotal;
  public Boolean feeFromTotalAllowed;
  public Integer updateAfter;
 // @NotNull еще нет функционала
  public Boolean serviceDisabled;
  public ContinuationModel continuation;
  @NotNull
  public OperationStatusModel status;
  public ConfirmationModel confirmation;
  public Boolean __isTemplated;
  public Boolean __saveTemplate;
  public TemplateModel __template;
  @NotNull
  public String __session;
  public String __historyEventId;
  @NotNull
  public MetadataModel []__schema;
  public Error __error;
  public Errors [] errors;
  //for simple identity not described in api documentation
  public String full_route_string;
  
  public static class ContinuationModel{
    @NotNull
    public String serviceId;
    public ViewSequenceModel viewSequence;
    public ParametersModel parameters;
  }
  
  public static class ViewSequenceModel{
    @NotNull
    public String templateId;
    public int sortOrder;
  }
  
  public static class ParametersModel{
    @NotNull
    public String name;
    @NotNull @Localized
    public String value;
  }
  
  public static class Error{
    @NotNull @Localized
    public String message;
    @NotNull
    public Boolean fatal;
  }
  public static class Errors{
    @NotNull
    public String field;
    @NotNull @Localized
    public String message;
  }
}