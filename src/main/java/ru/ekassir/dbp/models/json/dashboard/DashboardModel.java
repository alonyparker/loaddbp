package ru.ekassir.dbp.models.json.dashboard;

import javax.validation.constraints.NotNull;
import ru.ekassir.dbp.models.json.JsonBaseModel;
import ru.ekassir.dbp.models.json.MoneyModel;
import ru.ekassir.dbp.models.json.base.SessionInfoModel;
import ru.ekassir.dbp.models.json.service.ServicesModel;
import ru.ekassir.dbp.models.json.timeline.HistoryEventModel;

public class DashboardModel extends JsonBaseModel{
  @NotNull
  public SessionInfoModel session;
  
  public MoneyModel totalAvailableAmount;
  public BaseContractModel masterAccount;
  
  public CurrentContracts currentContracts;
  public CardContracts cardContracts;
  public LoanContracts loanContracts;
  public DepositContracts depositContracts;
  public WalletContracts walletContracts;  
  public Boolean hasDeposits;
  public Boolean topUpAvailable;
  public PlannedExpense plannedExpense;
  public String advice;
  public IdentificationStatusModel identificationStatus;
  @NotNull
  public Integer unreadMessageCount;
  public Integer importantEventCount;
  public ImportantEvents importantEvents;
  public ServicesModel services;  
 // public Contacts contacts;

  public static class PlannedExpense {
    @NotNull 
    public String date;
    @NotNull
    public MoneyModel amount;
  }

  //Поле убрано из API
 /* public static class Contacts {
    public String phone;
    public String alternativePhone;
    public String web;
    public String email;
  }*/

  public static class CardContracts{
    @NotNull
    public CardBaseContractModel items[];
  }

  public static class DepositContracts {
    @NotNull
    public DepositBaseContractModel items[];
  }

  public static class LoanContracts{
    @NotNull
    public LoanBaseContractModel items[];
  }
  
  public static class WalletContracts{
    @NotNull
    public WalletBaseContractModel items[];
  }
  
  public static class CurrentContracts{
    @NotNull
    public CurrentBaseContractModel items[];
  }
  
  public static class ImportantEvents{
    @NotNull
    public HistoryEventModel items[];
  }
}