package ru.ekassir.dbp.models.json.dashboard;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class IdentificationStatusModel extends JsonBaseModel{
  @NotNull
  public String status;
  @NotNull
  public String serviceId;
  @NotNull
  public String description;
  @NotNull
  public String displayName;
}