package ru.ekassir.dbp.models.json.user;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.JsonBaseModel;

public class UserModel extends JsonBaseModel{
  @NotNull  
  public String id;
  @NotNull
  public String login;
  @Localized
  public String name;
  public String mobilePhone;
  public String email;
  @Localized
  public String firstName;
  @Localized
  public String secondName;
  @Localized
  public String middleName;
  public String notificationChannel;
  public String[] notificationPhones;
  public String language;
}