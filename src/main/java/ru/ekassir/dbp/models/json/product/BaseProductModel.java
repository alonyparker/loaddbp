package ru.ekassir.dbp.models.json.product;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.JsonBaseModel;
import ru.ekassir.dbp.models.json.MoneyModel;

public class BaseProductModel extends JsonBaseModel{
  @NotNull
  public String id;
  @NotNull
  public String type;
  @NotNull @Localized
  public String name;
  @NotNull @Localized
  public String description;
  public Boolean hidden;
  @NotNull
  public String productUrl;
  @NotNull
  public String tariffUrl;
  @NotNull
  public String imageName;
  public AdditionalCards additionalCards;
  public String ebossed;
  public Limits limits;
  public String minDocumentsCount;
  public Document[] documents;
  public MoneyModel minDepositAmount;
  public TariffModel[] tariff;
  public RequirementsModel [] requirements;
  public LoanTermsModel loanTerms;
  
  public static class LoanTermsModel{
    public MoneyModel roundAmount;
    @NotNull
    public RatesModel [] rates;
    public SuperRateModel superRate;
    @NotNull
    public InsuranceModel insurance;
    
    public static class RatesModel {
      public MinMaxModel amountRange;
      //public MinMaxModel term;
      public TermModel term;
      public String insurance;
      public Double interestRate;
    }
    
    public static class SuperRateModel {
      @NotNull
      public Double rate;
      @NotNull
      public Double feeRate;
    }
    
    public static class InsuranceModel {
      @NotNull
      public Boolean required;
      public String paymentForm;
      @NotNull
      public Insurance [] insurances;
      
      public static class Insurance{
        @NotNull @Localized
        public String displayName;
        @NotNull
        public String type;
        @NotNull
        public Rate [] rates;
        public static class Rate{
          @NotNull
          public MinMaxModel amountRange;
          @NotNull
          public Double insuranceRate;
        }
      }
    }
  }
  
  public static class MinMaxModel {
    @NotNull
    public MoneyModel min;
    @NotNull
    public MoneyModel max;
  }
  
  public static class TermModel {
    @NotNull
    public Integer min;
    @NotNull
    public Integer max;
  }
  
  public static class Document {
    @NotNull
    public String id;
    @NotNull @Localized
    public String name;
    @NotNull
    public String requirement;
  }
  public static class Limit {
    @NotNull
    public String currency;
    @NotNull
    public MoneyModel min;
    @NotNull
    public MoneyModel max;
    public MoneyModel limits;
    public MoneyModel interval;
  }
  public static class AdditionalCard {
    @NotNull
    public String id;
    @NotNull @Localized
    public String name;
    @NotNull @Localized
    public String description;
    @NotNull
    public String image;
    @NotNull
    public String embossed;
  }
  
  public static class AdditionalCards {
	  public AdditionalCard[] items;
  }
  
  public static class Limits{
	  public Limit[] items;
  }
  
  public static class TariffModel {
    @NotNull
    public String sortOrder;
    @NotNull @Localized
    public String displayName;
    @Localized
    public String description;
    public SortOrderDisplayNameDescription [] items;
  }
  public static class SortOrderDisplayNameDescription{
    @NotNull
    public Integer sortOrder;
    @NotNull @Localized
    public String displayName;
    @Localized
    public String description;
  }

  public static class RequirementsModel{
    @NotNull
    public String sortOrder;
    @NotNull @Localized
    public String displayName;
    @Localized
    public String description;
    public SortOrderDisplayNameDescription [] items;
  }
}
