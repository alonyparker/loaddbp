package ru.ekassir.dbp.models.json.currency;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class BaseCurrencyModel extends JsonBaseModel{
  @NotNull
  public String currency;
  @NotNull
  public Integer discriminator;
}
