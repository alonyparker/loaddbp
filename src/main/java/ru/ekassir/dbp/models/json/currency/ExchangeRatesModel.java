package ru.ekassir.dbp.models.json.currency;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class ExchangeRatesModel extends JsonBaseModel{
  @NotNull
  public BaseCurrencyModel base;
  @NotNull
  public BaseCurrencyModel target;
  public String timestamp;
  public RateInfoModel buy;
  public RateInfoModel sell;  
}
