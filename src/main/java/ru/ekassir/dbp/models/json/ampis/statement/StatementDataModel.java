package ru.ekassir.dbp.models.json.ampis.statement;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.MoneyModel;

public class StatementDataModel {
  @NotNull
  public MoneyModel balance;
  public TransactionDataModel[] transactionData;

  public class TransactionDataModel{
    @NotNull
    public String transactionDate;
    @NotNull
    public String pensionFund;
    @NotNull
    public Double unitAmount;
    @NotNull
    public Double price;
    @NotNull
    public Double moneyAmount;
    @NotNull
    public String description;
    
  }
}
