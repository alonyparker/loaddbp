package ru.ekassir.dbp.models.json.paymentSchedule;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class BaseScheduledPaymentModel extends JsonBaseModel{
   @NotNull
   public String id;
   @NotNull
   public String term;
   @NotNull
   public String state;
}