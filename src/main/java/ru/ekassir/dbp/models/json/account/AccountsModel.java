package ru.ekassir.dbp.models.json.account;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class AccountsModel extends JsonBaseModel{
  @NotNull
  public AccountModel items[];
  public int count;
}
