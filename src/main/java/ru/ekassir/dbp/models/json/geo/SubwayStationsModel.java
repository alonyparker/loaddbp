package ru.ekassir.dbp.models.json.geo;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class SubwayStationsModel extends JsonBaseModel{
  @NotNull
  public SubwayStationModel items[];
}
