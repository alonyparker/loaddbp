package ru.ekassir.dbp.models.json.operation;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class TemplateGroupsModel extends JsonBaseModel{
  @NotNull
  public TemplateGroupModel items [];
  public int count;
}