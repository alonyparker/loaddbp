package ru.ekassir.dbp.models.json.atmstates;

import javax.validation.constraints.NotNull;

public class OptionModel {
 @NotNull
 public String name;

 @NotNull
 public String value;
}
