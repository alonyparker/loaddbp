package ru.ekassir.dbp.models.json;

import ru.ekassir.dbp.helpers.JsonHelper;

public class JsonBaseModel {

 @Override
  public String toString(){
    return JsonHelper.serializeModel(this);     
  }
}
