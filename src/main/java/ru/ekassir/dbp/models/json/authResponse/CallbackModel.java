package ru.ekassir.dbp.models.json.authResponse;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class CallbackModel extends JsonBaseModel{
  public String promt;
  public String watermark;
  public String name;
  public String type;
  public String value;
  public OptionModel options[];
}
