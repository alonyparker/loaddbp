package ru.ekassir.dbp.models.json.dashboard;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.MoneyModel;
import ru.ekassir.dbp.models.json.topupMethod.BaseTopupmethodModel;

public class DepositExtendedContractModel extends DepositBaseContractModel{
  public MoneyModel maxExpenseAmount;
  public MoneyModel maxCashOutAmount;
  public BaseTopupmethodModel [] topUpMethods;
  
  public Double interestRate;
  public Double originalInterestRate;
  
  @NotNull
  public DepositInfoModel depositInfo;
  
  public static class DepositInfoModel {
    @NotNull
    public MoneyModel inAmount;
    @NotNull
    public MoneyModel outAmount;
    @NotNull
    public MoneyModel planningInterestAmount;
    @NotNull
    public RateModel [] interestRates;
    @NotNull
    public Double earlyInterestRate;
    @NotNull
    public EarlyInterestRateModel [] earlyInterestRates;
    public MoneyModel minDepositAmount;
    
    @NotNull
    public InterestPayInfoModel interestPayInfo;
    public String incomeClosingDate;
    public MoneyModel interestAmount;
    public MoneyModel originalAmount;
    public MoneyModel interestReturnAmount;
    public ClosingActionModel closingAction;
  }
  
  public static class ClosingActionModel {
    public String type;
    public BaseContractModel contract;
    public MoneyModel amount;
  }
  
  public static class InterestPayInfoModel{
    @NotNull
    public String type;    
    public String period;
    public String periodInDays;
    @NotNull
    public String nextdate;
    public Object contract;
    public String requisites;
    public MoneyModel minDepositAmount;
    public MoneyModel betterMinDepositAmount;
    public MoneyModel betterRestAmount;
    public MoneyModel nextInterestAmount;
  }
  
  public static class EarlyInterestRateModel{
    @NotNull
    public Double rate;
    @NotNull
    public MoneyModel startAmount;
    public MoneyModel endAmount;
    @NotNull
    public Integer startPeriod;
    @NotNull
    public Integer endPeriod;
  }
  
  public static class RateModel {
    @NotNull
    public Double rate;
    @NotNull
    public MoneyModel startAmount;
    public MoneyModel endAmount;
    @NotNull
    public MoneyModel bearAmount;
    @NotNull
    public String type;
  }
}
