package ru.ekassir.dbp.models.json.geo;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.MoneyModel;

public class ExtendPointModel extends BasePointModel{
  @NotNull
  public String timeZone;
  public WorkingHourModel workingHours;
  public Service services;
  public MoneyModel banknotes;
  
  public static class WorkingHourModel{
    public String[] monday;
    public String[] tuesday;
    public String[] wednesday;
    public String[] thursday;
    public String[] friday;
    public String[] saturday;
    public String[] sunday;
  }
  public static class ExceptModel{
    @NotNull
    public String date;
    public String workingHours;
  }
  public static class Service{
    public String cashOutRUB;
    public String cashOutUSD;
    public String cashOutEUR;
    public String payments;
    public String cashInRUB;
  }
}
