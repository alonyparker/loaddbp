package ru.ekassir.dbp.models.json.base;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.JsonBaseModel;
import ru.ekassir.dbp.models.json.MoneyModel;

public class FeesDueAmountModel extends JsonBaseModel{
  @NotNull
  public MoneyModel amount;
  @NotNull
  public String type;
  @NotNull @Localized
  public String name;
}