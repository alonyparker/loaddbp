package ru.ekassir.dbp.models.json.currency;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class CurrenciesListModel extends JsonBaseModel{
  @NotNull
  public CurrencyModel[] items;
  public Integer count;
  public Integer totalCount;
  public Integer pages;
  public Integer page;
}
