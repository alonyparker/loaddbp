package ru.ekassir.dbp.models.json.service;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class DefiniteServiceModel extends JsonBaseModel{
  @NotNull
  public Integer id;
  @NotNull
  public String name;
  public String description;
  public Boolean blocked;
  @NotNull
  public String type;
  public Application application;
  public static class Application{
    @NotNull
    public String id;
    @NotNull
    public String displayName;
    public String description;
    public String status;
  }
}