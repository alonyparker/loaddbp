package ru.ekassir.dbp.models.json.thirdPartyCard;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.JsonBaseModel;

public class ThirdPartyCardModel extends JsonBaseModel{
  @NotNull
  public String id;
  @NotNull
  public String paymentSystem;
  @NotNull @Localized
  public String name;
  @NotNull
  public String number;
  @NotNull
  public String expiryDate; 
  @NotNull
  public String state; 
}
