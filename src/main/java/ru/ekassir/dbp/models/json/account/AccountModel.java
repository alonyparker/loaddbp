package ru.ekassir.dbp.models.json.account;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;
import ru.ekassir.dbp.models.json.MoneyModel;

public class AccountModel extends JsonBaseModel{
    @NotNull
    public String contractId;
    @NotNull
    public String number;
    public MoneyModel availableLimitAmount;
    @NotNull
    public String name;
    @NotNull
    public String openedDate;
    public String closedDate;
    @NotNull
    public String state;
    @NotNull
    public String id;
    
    //поле устарело. может присутствовать для обратной совместимости
    public String currency;
    
    //кандидаты на перенос в другие объекты
    public Double interestRate;
    public String tariffId;
}