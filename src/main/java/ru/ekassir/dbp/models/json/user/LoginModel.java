package ru.ekassir.dbp.models.json.user;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.JsonBaseModel;

public class LoginModel extends JsonBaseModel{
  @NotNull
  public String login;
  @NotNull @Localized
  public String policyDescription;
  public Boolean isCompliant;
}
