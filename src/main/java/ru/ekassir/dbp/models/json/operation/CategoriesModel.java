package ru.ekassir.dbp.models.json.operation;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class CategoriesModel extends JsonBaseModel{
  @NotNull
  public CategoryModel items [];
  public int count;
}