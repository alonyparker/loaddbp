package ru.ekassir.dbp.models.json.cart;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;
import ru.ekassir.dbp.models.json.MoneyModel;
import ru.ekassir.dbp.models.json.operation.OperationModel;
import ru.ekassir.dbp.models.json.operation.PaymentToolModel;

public class CartModel extends JsonBaseModel{
  @NotNull
  public String id;
  public String exchangeId;
  @NotNull
  public PaymentToolModel paymentTool;
  @NotNull
  public PaymentToolModel [] availablePaymentTools;
  public OperationModel [] operations;
  @NotNull
  public MoneyModel amount;
  @NotNull
  public MoneyModel fee;
  @NotNull
  public MoneyModel total;
  @NotNull
  public String deletedDate;
  public Integer updateAfter;
  @NotNull
  public Boolean _transferCart;
  public Boolean _useDelayed;
  @NotNull
  public StatusModel status;
  public ErrorModel __error;
  public static class StatusModel{
    @NotNull
    public String id;
    public String description;
  }
  public static class ErrorModel{
    @NotNull
    public String message;
  }
}
