package ru.ekassir.dbp.models.json.geo;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.JsonBaseModel;

public class GeoObjectModel extends JsonBaseModel{
  @NotNull @Localized
  public String name;
  @NotNull @Localized
  public String fullName;
  @NotNull
  public String type;
  @NotNull
  public GeoPointModel location;
  @NotNull
  public GeoAreaModel area;
  @NotNull @Localized
  public String countryName;
  @NotNull
  public String country;
  @Localized
  public String administrativeArea;
  @Localized
  public String subAdministrativeArea;
  @Localized
  public String localityName;
  @Localized
  public String address;
}
