package ru.ekassir.dbp.models.json.menu;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.JsonBaseModel;

public class MenuModel extends JsonBaseModel{
  @NotNull
  public String id;
  @NotNull @Localized
  public String displayName;
  @NotNull
  public Integer totalCount;
  @NotNull
  public Integer count;
  @NotNull
  public MenuElementModel items[];
  public String imagesLocation;
  public String suppressFeatures;
  public String identificationServiceId;
}
