package ru.ekassir.dbp.models.json.operation;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class PaymentToolsModel extends JsonBaseModel{
  @NotNull
  public PaymentToolModel items [];
  public int count;
}
