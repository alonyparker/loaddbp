package ru.ekassir.dbp.models.json.feedback;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class TopicsModel extends JsonBaseModel{
  public TopicModel[] items;
  public Integer count;
}
