package ru.ekassir.dbp.models.json.atmstates;

import javax.validation.constraints.NotNull;

public class CallbackModel {
  @NotNull
  public String name;

  @NotNull
  public String type;
  
  @NotNull
  public OptionModel options[];
  
  public String prompt;
  public String value;
  public String watermark;
  public String hint;
  public String mask;
  //todo
  //public String "default";
}
