package ru.ekassir.dbp.models.json.operation;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class TemplatesModel extends JsonBaseModel{
  @NotNull
  public TemplateModel items [];
  public int count;
}
