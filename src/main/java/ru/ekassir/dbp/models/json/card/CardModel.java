package ru.ekassir.dbp.models.json.card;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.JsonBaseModel;
import ru.ekassir.dbp.models.json.MoneyModel;

public class CardModel extends JsonBaseModel{
  @NotNull
  public String id;
  @NotNull
  public String contractId;
  @NotNull
  public String cardType;
  @NotNull
  public String paymentSystem;  
  @NotNull
  public Boolean isPrimary;
  @NotNull @Localized
  public String name;
  @NotNull @Localized
  public String displayName;
  public String imageName;
  public String backgroundColor;
  public Boolean hidden;
  @NotNull
  public String number;
  @NotNull
  public String currency;
  @NotNull
  public String expiryDate;
  public Long bonusPoints;
  public Boolean debitOperationsAllowed;
  public Boolean restrictOperationsAbroad;
  @NotNull
  public String state;
  public MoneyModel availableLimitAmount;
}