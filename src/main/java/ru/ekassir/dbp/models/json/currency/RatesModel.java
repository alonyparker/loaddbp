package ru.ekassir.dbp.models.json.currency;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class RatesModel extends JsonBaseModel{
  @NotNull
  public String categoryId;
  @NotNull
  public ExchangeRatesListModel rates;
  
  public class ExchangeRatesListModel{
    public ExchangeRatesModel[] items;
  }
}
