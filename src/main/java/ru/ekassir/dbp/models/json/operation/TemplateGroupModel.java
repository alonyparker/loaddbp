package ru.ekassir.dbp.models.json.operation;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.JsonBaseModel;

public class TemplateGroupModel extends JsonBaseModel{
  @NotNull
  public String id; 
  @NotNull @Localized
  public String name;
  public String createdDate;
  public String color;
  public Integer orderId;
  public String updatedDate;
  public String deletedDate;
  public Object attributes;
}
