package ru.ekassir.dbp.models.json.dashboard;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.MoneyModel;
import ru.ekassir.dbp.models.json.base.FeesDueAmountModel;
import ru.ekassir.dbp.models.json.paymentSchedule.PaymentsScheduleModel;
import ru.ekassir.dbp.models.json.topupMethod.BaseTopupmethodModel;

public class LoanExtendedContractModel extends LoanBaseContractModel{
  public MoneyModel maxExpenseAmount;
  public MoneyModel maxCashOutAmount;
  public BaseTopupmethodModel [] topUpMethods;
  
  public MoneyModel originalDebtAmount;
  public MoneyModel payedAmount;
  public MoneyModel debtDueAmount;
  public MoneyModel interestDueAmount;
  
  public FeesDueAmountModel [] feesDueAmounts;
  
  public Double interestRate;
  public Double originalInterestRate;
  
  public OverdueInfoModel overdueInfo;
  
  public static class OverdueInfoModel {
    @NotNull
    public Integer overduePaymentsCount;
    @NotNull
    public MoneyModel overduePaymentsAmount;
    @NotNull
    public MoneyModel overdueAmount;
    @NotNull
    public MoneyModel debtOverdueAmount;
    @NotNull
    public MoneyModel interestOverdueAmount;
    @NotNull
    public FeesOverdueAmountModel []feesOverdueAmounts;
  }
  
  public static class FeesOverdueAmountModel {
    @NotNull
    public MoneyModel amount;
    @NotNull
    public String type;
    @NotNull @Localized
    public String name;
  }
  
  public PaymentsScheduleModel paymentsSchedule;
  
  //только для почта банка
  public SuperRateModel superRate; 
  
  public static class SuperRateModel {
    @NotNull
    public Integer state;
    public Boolean hadDelayedPayments;
    public Integer paymentsState;
    public Boolean isRepayed;
    @NotNull
    public String stateDescription;
    @NotNull
    public Double newRate;
    @NotNull
    public Integer periodCount;
    @NotNull
    public Integer payCount;
    @NotNull
    public MoneyModel returmAmount;
    @NotNull
    public String description;
    @NotNull
    public String url;    
    @NotNull
    public TermModel [] terms;
  }
  public static class TermModel {
    @NotNull
    public String state;
    @NotNull
    public String name;
  }
  
  public String limitClosingDate;
}
