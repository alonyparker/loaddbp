package ru.ekassir.dbp.models.json.password;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class ComplexityModel extends JsonBaseModel{
	@NotNull
	public int maxRating;
	@NotNull
	public int rating;
	public ComplexityGroupModel groups[];

}
