package ru.ekassir.dbp.models.json.statement;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;
import ru.ekassir.dbp.models.json.MoneyModel;
import ru.ekassir.dbp.models.json.account.AccountsModel;

public class StatementModel extends JsonBaseModel{
  @NotNull
  public String startDate;
  @NotNull
  public String endDate;
  @NotNull
  public Object contract;
  public Object card;
  public AccountsModel account;
  @NotNull
  public SummaryModel summary;
  @NotNull
  public OperationModel [] operations;
  public static class SummaryModel{
    public MoneyModel inDepositAmount;
    public MoneyModel outDepositAmount;
    @NotNull
    public MoneyModel inAmount;
    @NotNull
    public MoneyModel outAmount;
  }
  public static class OperationModel{
    @NotNull
    public String id;
    @NotNull
    public String timestamp;
    public String timestampProcessing;
    @NotNull
    public String description;
    @NotNull
    public MoneyModel operationAmount;
    @NotNull
    public MoneyModel amount;
    @NotNull
    public String status;
    public String operationType;
    public String approvalCode;
  }
}
