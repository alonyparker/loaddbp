package ru.ekassir.dbp.models.json.image;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class Base64Image extends JsonBaseModel{
	@NotNull
	public String data;	
}
