package ru.ekassir.dbp.models.json.user;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.JsonBaseModel;
import ru.ekassir.dbp.models.json.geo.GeoObjectModel;

public class PersonalAccountModel extends JsonBaseModel{
  @NotNull
  public UserModel owner;
  @NotNull
  public String id;
  @Localized
  public GeoObjectModel homeLocality;  
  public String emailSecondary;
  public String mobileSecondary;
  //public String favoritePaymentToolId;
  public String favoriteCardId;
  //!!!! Api docs cant contain that field  
  public String name;
  public String language;
  public String favoriteCurrency;
  public String favoritePaymentToolId;
}