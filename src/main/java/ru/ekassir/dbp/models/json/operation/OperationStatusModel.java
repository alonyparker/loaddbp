package ru.ekassir.dbp.models.json.operation;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.JsonBaseModel;

public class OperationStatusModel extends JsonBaseModel{
  @NotNull
  public String id;
  @Localized
  public String description;
  @NotNull @Localized
  public String displayName;
}
