package ru.ekassir.dbp.models.json.dashboard;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.MoneyModel;

public class LoanBaseContractModel extends BaseContractModel{
  @NotNull
  public MoneyModel limitAmount;
  public Boolean hasLimitAmount;
  //@NotNull
  public String paymentDueDate;
  //@NotNull
  public MoneyModel paymentDueAmount;
  public MoneyModel paymentDueAmountRest;
  public MoneyModel debtAmount;
  public Boolean hasDueClaim;
  //Только для почта банка
  public Boolean limitBlocked;
  public Boolean limitClosed;
}
