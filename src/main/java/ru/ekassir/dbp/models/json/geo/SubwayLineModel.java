package ru.ekassir.dbp.models.json.geo;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.JsonBaseModel;

public class SubwayLineModel extends JsonBaseModel{
  @NotNull
  public String number;
  @NotNull @Localized
  public String name;
  @NotNull
  public String color;
}
