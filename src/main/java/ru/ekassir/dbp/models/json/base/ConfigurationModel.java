package ru.ekassir.dbp.models.json.base;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class ConfigurationModel extends JsonBaseModel{
  @NotNull
  public ContactsModel contacts;
  @NotNull
  public EnvironmentModel environments[];
  public String imagesLocation;
  public String offerLocation;
  public String welcomeMessage;
  public String logoName;
  public Object socialNets;
  @NotNull
  public RequisitsModel requisites;
  public CurrencyConfigurationModel currencyConfiguration;
  public MetadataModel [] __schema;
  public ServicesModel[] services;
  public Integer cardholderDataDisplayTimeout;
  public TimelineConfigurationModel timelineConfiguration;
  public ContractsBackgroundGalleryModel contractsBackgroundGallery[];
  public CategoriesImageGalleryModel categoriesImageGallery[];
  public CategoriesColorGalleryModel categoriesColorGallery[];
  public TemplateGroupsImageGalleryModel templateGroupsImageGallery[];
  public TemplateGroupsColorGalleryModel templateGroupsColorGallery[];
 
  //Classes
  public static class RequisitsModel {
    String fullName;
    String shortName;
    String businessAddress;
    String postAddress;
    String korrAcc;
    String inn;
    String swift;
    public MetadataModel [] __schema;
  }
  
  public static class TimelineConfigurationModel{
    public Integer pastPeriod;
    public Integer futurePeriod;
  }
  
  public static class ContractsBackgroundGalleryModel {
    @NotNull
    public  String imageName;
  }
  
  public static class CategoriesImageGalleryModel {
    @NotNull
    public  String imageName;
  }
  public static class CategoriesColorGalleryModel {
    @NotNull
    public String color;
  }
  public static class TemplateGroupsImageGalleryModel {
    @NotNull
    public String imageName;
  }
  public static class TemplateGroupsColorGalleryModel {
    @NotNull
    public String color;
  }
  public static class ContactsModel {
  //  @NotNull
    public String phone;
  //  @NotNull
    public String alternativePhone;
  //  @NotNull
    public String web;
  //  @NotNull
    public String email;
    public String skype;
  //  @NotNull
    public Object __schema [];
  }
  
  public static class EnvironmentModel {
    public  String description;
    @NotNull
    public  String id;
    @NotNull
    public  String url;
  }
  
  public static class CurrencyConfigurationModel {
    public CurrencyModel[] favorites;
    public CurrencyModel[] target;
    public CurrencyModel[] base;
  }
  
  public static class CurrencyModel {
    @NotNull
    public String currency;
  }
  
  public static class ServicesModel {
    @NotNull
    public String id;
    @NotNull
    public String name;
    @NotNull
    public String type;
    public String description;    
    public String confirmation;
  }
}