package ru.ekassir.dbp.models.json.subscription;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class SubscriptionModel extends JsonBaseModel{
  @NotNull
  public String hwid;
  @NotNull
  public String platform;
  @NotNull
  public String pushToken;
  @NotNull
  public String language;
  @NotNull
  public Integer timezone;
  @NotNull
  public String timestamp;
}
