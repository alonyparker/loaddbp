package ru.ekassir.dbp.models.json.base;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.JsonBaseModel;

public class MetadataModel extends JsonBaseModel{
  @NotNull
  public String name;
  @NotNull
  public String type; 
  @Localized
  public String displayName;
  @Localized
  public String description;
  public Integer sortOrder;
  public Boolean readOnly;
  public Boolean isRequired;
  public Boolean refreshOnChange;
  public EditorModel [] editors;
  //TODO: заполнять по факту
  //public  Dynamic defaultValue
  //public Dynamic min;
  //public Dynamic max;
  public Integer precision;
  public Integer scale;
  public String mask;
  public String regexp;
  @Localized
  public String errorMessage;
  public AvailableValue [] availableValues;
  public Boolean hidden;
  public String hiddenRelationRef;
  public String hiddenRegexp;
  public String readOnlyRelationRef;
  public String readOnlyRegexp;
  public static class EditorsModel {
    public EditorModel [] items;
  }
  
  public static class EditorModel {
    @NotNull
    public String id;
    @NotNull
    public Integer priority;
    public String hints;
  }
  
  public static class AvailableValues {
    public AvailableValue[] items;
  }
  
  public static class AvailableValue {
   /*
    @NotNull
    public String value;
    */
    @NotNull @Localized
    public String displayName;
    @Localized
    public String description;
  }
}
