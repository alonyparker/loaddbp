package ru.ekassir.dbp.models.json.am;

public class NewPassword {
  PasswordClass Password;
  public NewPassword(String password) {
    this.Password = new PasswordClass(password);
  }
  
  public class PasswordClass {
    String Value;
    String EffectiveDate;
    String ExpirationDate;
          
    public PasswordClass(){
      this.ExpirationDate = "2019-12-29T11:09:26.028Z";
      this.EffectiveDate = "2016-12-29T11:09:26.028Z";
      this.Value = "qwe123";
    }
    
    public PasswordClass(String password){
      this.ExpirationDate = "2019-12-29T11:09:26.028Z";
      this.EffectiveDate = "2016-12-29T11:09:26.028Z";
      this.Value = password;
    }
  }
}
