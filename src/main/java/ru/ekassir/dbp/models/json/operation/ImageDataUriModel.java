package ru.ekassir.dbp.models.json.operation;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class ImageDataUriModel extends JsonBaseModel{
 @NotNull
 public String data;
}
