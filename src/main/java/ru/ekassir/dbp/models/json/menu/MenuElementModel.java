package ru.ekassir.dbp.models.json.menu;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.JsonBaseModel;
import ru.ekassir.dbp.models.json.operation.TemplateModel;

public class MenuElementModel extends JsonBaseModel{
  @NotNull
  public String id;
  @NotNull @Localized
  public String displayName;
  @NotNull
  public String type;
  @Localized
  public String description;
  public String imageName;
  public String backgroundColor;
  public Integer totalCount;
  public String serviceId;
  public TemplateModel template;
  public Boolean disabled;
  //fields for tests (not in specs)
  public String score;
}