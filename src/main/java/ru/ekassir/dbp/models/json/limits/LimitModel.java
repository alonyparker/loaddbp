package ru.ekassir.dbp.models.json.limits;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.JsonBaseModel;
import ru.ekassir.dbp.models.json.MoneyModel;

public class LimitModel extends JsonBaseModel{
    @NotNull
    public String id;
    @NotNull @Localized
    public String name;
    @NotNull @Localized
    public String inactiveName;
    @NotNull @Localized
    public String currentName;
    @NotNull @Localized
    public String categoryName;
    @NotNull
    public String categoryId;
    @NotNull
    public Integer sortOrder;
    public Boolean readOnly;
    @NotNull
    public String type;
    @NotNull
    public String period;
    @NotNull
    public String startTime;
    @NotNull
    public String endTime;
    @NotNull
    public Boolean isActive;
    public Integer maxCountRestriction;
    public Integer maxCount;
    public Integer currentCount;
    public MoneyModel maxAmount;
    public MoneyModel maxAmountRestriction;
    public MoneyModel currentAmount;
    public HistoriesModel resetHistory;
    public static class HistoryModel {
      @NotNull
      public String time;
      public Integer count;
      public MoneyModel amount;
    }
    public static class HistoriesModel {
      public HistoryModel items[];
    }
}
