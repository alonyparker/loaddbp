package ru.ekassir.dbp.models.json.operation;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.JsonBaseModel;
import ru.ekassir.dbp.models.json.timeline.CodesModel;

public class CategoryModel extends JsonBaseModel{
  @NotNull
  public String id;
  @NotNull @Localized
  public String name;  
  public String color;
  public String deletedDate;
  public CodesModel[] codes;
  public Boolean readOnly;
}