package ru.ekassir.dbp.models.json.limits;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class LimitsModel extends JsonBaseModel{
  @NotNull
  public LimitModel items[];
  public int count;
}
