package ru.ekassir.dbp.models.json.timeline;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;
import ru.ekassir.dbp.models.json.MoneyModel;
import ru.ekassir.dbp.models.json.operation.SummariesModel;

public class TimelineModel extends JsonBaseModel{
  public static class HistoryEvents{
    @NotNull
    public HistoryEventModel items[];
  }
  @NotNull
  public HistoryEvents events;
  @NotNull
  public String id;
  @NotNull
  public Integer page;
  @NotNull
  public Integer count;
  public Integer totalCount;
  public MoneyModel totalDebitAmount;
  public MoneyModel totalCreditAmount;
  @NotNull
  public Integer futureCount;
  @NotNull
  public String direction;
  @NotNull
  public String imagesLocation;
  public SummariesModel summary;
}
