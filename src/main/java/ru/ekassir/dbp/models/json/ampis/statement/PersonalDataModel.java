package ru.ekassir.dbp.models.json.ampis.statement;

import javax.validation.constraints.NotNull;

public class PersonalDataModel {
  @NotNull
  public String psn;
  @NotNull
  public String lastName;
  @NotNull
  public String firstName;
  @NotNull
  public String dateOfBirth;
}
