package ru.ekassir.dbp.models.json.geo;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.JsonBaseModel;

public class BasePointModel extends JsonBaseModel{
  @NotNull
  public String id;
  @NotNull
  public String type;
  @NotNull @Localized
  public String title;
  @NotNull @Localized
  public String city;
  @NotNull @Localized
  public String region;
  @NotNull
  public GeoPointModel location;
  @NotNull @Localized
  public String address;
  public SubwayStationModel subwayStation;
  @Localized
  public String additionalInfo;
  @Localized
  public String importantInfo;
  public String deliveryOrgCode;
  @Localized
  public String schedule;
  public Double rating;
  public PartnerModel partner;
  public static class PartnerModel{
    @NotNull
    public String id;
    @NotNull
    public String name;
  }
  @NotNull
  public String createdAt;
  @NotNull
  public String modifiedAt;
  public Boolean deleted;
  public String working;
  @Localized
  public String operations;
}