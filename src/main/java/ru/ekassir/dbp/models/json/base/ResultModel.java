package ru.ekassir.dbp.models.json.base;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class ResultModel extends JsonBaseModel{
  @NotNull
  public String result;
}
