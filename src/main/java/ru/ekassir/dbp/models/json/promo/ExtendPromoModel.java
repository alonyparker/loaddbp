package ru.ekassir.dbp.models.json.promo;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.MoneyModel;

public class ExtendPromoModel extends BasePromoModel{
  public ProductTermsModel productTerms;
  public static class ProductTermsModel{
    @NotNull
    public Object product;
    public LoanTermsModel loanTerms;
    public Boolean photoRequired;
  }
  public static class LoanTermsModel{
    public Double rate;
    @NotNull
    public PaymentAmountModel paymentAmount;
    public LimitAmountModel limitAmount;
    public TermRangeModel termRange;
  }
  public static class PaymentAmountModel{
    @NotNull
    public MoneyModel min;
    @NotNull
    public MoneyModel max;
    @NotNull
    public MoneyModel value;
  }
  public static class LimitAmountModel{
    @NotNull
    public MoneyModel min;
    @NotNull
    public MoneyModel max;
    @NotNull
    public MoneyModel value;
  }
  public static class TermRangeModel{
    @NotNull
    public Integer min;
    @NotNull
    public Integer max;
    @NotNull
    public Integer value;
  }
}
