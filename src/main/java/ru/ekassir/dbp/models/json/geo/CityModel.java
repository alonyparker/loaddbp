package ru.ekassir.dbp.models.json.geo;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.JsonBaseModel;

public class CityModel extends JsonBaseModel{
  @NotNull
  public String id;
  @NotNull @Localized
  public String name;
  @NotNull
  public GeoPointModel location;
  @NotNull
  public GeoAreaModel area;
  @NotNull
  public String modifiedAt;
  @NotNull
  public String createdAt;

  public Boolean hasSubway;
  public Boolean deleted;
}
