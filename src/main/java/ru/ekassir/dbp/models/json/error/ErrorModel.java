package ru.ekassir.dbp.models.json.error;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class ErrorModel extends JsonBaseModel{
  public String code;
  public String name;
  public String value;
}

