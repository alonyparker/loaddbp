package ru.ekassir.dbp.models.json.geo;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class CitiesModel extends JsonBaseModel{
  @NotNull
  public CityModel items[];
}
