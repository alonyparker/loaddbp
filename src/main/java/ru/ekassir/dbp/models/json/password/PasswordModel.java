package ru.ekassir.dbp.models.json.password;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.JsonBaseModel;

public class PasswordModel extends JsonBaseModel{
  @NotNull
  public Boolean hasPassword;	
  @NotNull @Localized
  public String policyDescription;
  public String oldPassword;
  public String password;
  public Boolean isCompliant;
  public ComplexityModel complexity;
}
