package ru.ekassir.dbp.models.json.feedback;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class MessageModel extends JsonBaseModel{
 // @NotNull
  public String id;
  @NotNull
  public String title;
  public String text;
  
  public MessageModel(String id, String title, String text) {
    this.id = id;
    this.title = title;
    this.text  = text;
  }
  
  public MessageModel(String title, String text) {   
    this.title = title;
    this.text  = text;
  }
}
