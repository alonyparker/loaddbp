package ru.ekassir.dbp.models.json.dashboard;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.MoneyModel;
import ru.ekassir.dbp.models.json.base.FeesDueAmountModel;
import ru.ekassir.dbp.models.json.topupMethod.BaseTopupmethodModel;

public class CardExtendedContractModel extends CardBaseContractModel{
  public MoneyModel maxExpenseAmount;
  public MoneyModel maxCashOutAmount;
  public BaseTopupmethodModel [] topUpMethods;
  
  public MoneyModel payedAmount;
  public MoneyModel debtDueAmount;
  public MoneyModel holdAmount;
  public MoneyModel interestDueAmount;
  
  public FeesDueAmountModel [] feesDueAmounts;
  
  public Integer maxGraceCount;
  
  public OverdueInfoModel overdueInfo;
  
  public static class OverdueInfoModel {
    @NotNull
    public Integer overduePaymentsCount;
    @NotNull
    public MoneyModel overduePaymentsAmount;
    @NotNull
    public MoneyModel overdueAmount;
    @NotNull
    public MoneyModel debtOverdueAmount;
    @NotNull
    public MoneyModel interestOverdueAmount;  
    @NotNull
    public FeesOverdueAmountModel []feesOverdueAmounts;
  }
  
  public static class FeesOverdueAmountModel {
    @NotNull
    public MoneyModel amount;
    @NotNull
    public String type;
    @NotNull @Localized
    public String name;
  }
  
  public GracePeriodInfoModel gracePeriodInfo;
  
  public static class GracePeriodInfoModel {
    
    @NotNull
    public GracePeriodInfo [] items;
    
    public static class GracePeriodInfo{
      public String settlementStartDate;
      public String settlementEndDate;
      public String graceStartDate;
      public String graceEndDate;
      public MoneyModel graceAmount;
      public MoneyModel graceAmountRest;
    }
  }
  
  public Double interestRate;
  public Double originalInterestRate;
  public String statementDate;
  
  //только для почта банка
  public MoneyModel fullPaymentAmount;
  public String fullPaymentDate;
  public String paymentDueEventId;
  public String paymentTerms;
  
  //устаревшие, оставлены для совместимости
  public String graceDate;
  public MoneyModel graceAmount;
  public MoneyModel graceAmountRest;
}
