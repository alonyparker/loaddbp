package ru.ekassir.dbp.models.json.currency;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.JsonBaseModel;

public class CurrencyModel extends JsonBaseModel{
  @NotNull
  public String id;
  public String symbol;
  @NotNull @Localized
  public String shortName;
  @NotNull @Localized
  public String fullName;
  public Integer scale;
}
