package ru.ekassir.dbp.models.json.geo;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class GeoObjectsModel extends JsonBaseModel{
  @NotNull
  public Integer totalCount;
  @NotNull
  public Integer count;
  @NotNull
  public GeoObjectModel[] items;
}
