package ru.ekassir.dbp.models.json;

import javax.validation.constraints.NotNull;

public class MoneyModel extends JsonBaseModel{
  public MoneyModel(Long amount, String currencyCode) {
    this.currencyCode = currencyCode;
    this.amount = amount;
  }
  
  public MoneyModel(Long amount) {
    this(amount, "RUB");
  }

  @NotNull
  public Long amount;
 
  @NotNull
  public String currencyCode;
  
  public Long getAmount() {
    return amount;
  }

  public void setAmount(Long amount) {
    this.amount = amount;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }
}
