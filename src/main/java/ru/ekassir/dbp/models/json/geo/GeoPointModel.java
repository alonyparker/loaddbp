package ru.ekassir.dbp.models.json.geo;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class GeoPointModel extends JsonBaseModel{
  @NotNull
  public Double lat;
  @NotNull
  public Double lng;
}
