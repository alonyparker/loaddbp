package ru.ekassir.dbp.models.json.card;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class CardsModel extends JsonBaseModel{
  @NotNull
  public CardModel items[];
  public int count;
}
