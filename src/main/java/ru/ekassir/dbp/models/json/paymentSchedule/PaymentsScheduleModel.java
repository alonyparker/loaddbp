package ru.ekassir.dbp.models.json.paymentSchedule;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;
import ru.ekassir.dbp.models.json.MoneyModel;

public class PaymentsScheduleModel extends JsonBaseModel{
  @NotNull
  public String contractId;
  @NotNull
  public String startDate;
  @NotNull
  public String endDate;
  @NotNull
  public MoneyModel originalDebtAmount; 
  @NotNull
  public ScheduledPayments payments;
  
  public static class ScheduledPayments{
    @NotNull
    public BaseScheduledPaymentModel items[];
  }
}
