package ru.ekassir.dbp.models.json.application;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;
import ru.ekassir.dbp.models.json.MoneyModel;;

public class BaseApplicationModel extends JsonBaseModel{
  @NotNull
  public String id;
  @NotNull
  public String type;
  @NotNull
  public Object product;
  public LoanTermsModel loanTerms;
  public String serviceId;
  public String promoId;
  public StatusModel status;
  @NotNull
  public String createdDate;
  @NotNull
  public String updatedDate;
  public String expires;
  public String delivery;
  public String deliveryAddress;
  
  public static class LoanTermsModel {
    @NotNull
    public MoneyModel limitAmount;
    public Integer term;
    public MoneyModel paymentAmount;
    public Double rate;
    public Boolean superRate;
    public String insurance;
  }
  public static class StatusModel{
    @NotNull
    public String id;
    @NotNull
    public String name; 
    public String description;
  }
}
