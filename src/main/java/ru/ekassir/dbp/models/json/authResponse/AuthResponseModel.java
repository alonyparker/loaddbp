package ru.ekassir.dbp.models.json.authResponse;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class AuthResponseModel extends JsonBaseModel{
	@NotNull
	public CallbackModel callbacks[];
	public String header;
	public String version;
	public String template;
	public ErrorModel errors[];
	public class ErrorModel {
	  public String code;
	  public String name;
	  public String value;
	}
}
