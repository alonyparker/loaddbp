package ru.ekassir.dbp.models.json.base;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class ImageConfigurationModel extends JsonBaseModel{
  @NotNull
  public Integer maxLength;
  @NotNull
  public Integer width;
  @NotNull
  public Integer height;
}