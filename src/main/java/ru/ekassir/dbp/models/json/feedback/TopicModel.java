package ru.ekassir.dbp.models.json.feedback;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.JsonBaseModel;

public class TopicModel extends JsonBaseModel{
  @NotNull
  public String id;
  @NotNull @Localized
  public String name;
  @Localized
  public String description;
}
