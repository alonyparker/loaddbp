package ru.ekassir.dbp.models.json.promo;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class PromosTempatesModel extends JsonBaseModel{
  @NotNull
  public String baseURL;
  @NotNull
  public String [] templateList;
}
