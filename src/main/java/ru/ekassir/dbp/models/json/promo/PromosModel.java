package ru.ekassir.dbp.models.json.promo;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class PromosModel extends JsonBaseModel{
  @NotNull
  public BasePromoModel items[];
}
