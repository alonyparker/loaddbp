package ru.ekassir.dbp.models.json.topupMethod;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.annotations.Localized;
import ru.ekassir.dbp.models.json.JsonBaseModel;

public class BaseTopupmethodModel extends JsonBaseModel{
  @NotNull
  public String id;
  @NotNull
  public String contractId;
  @NotNull @Localized
  public String description;
  @NotNull @Localized
  public String name;
}
