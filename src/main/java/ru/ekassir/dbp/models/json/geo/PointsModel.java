package ru.ekassir.dbp.models.json.geo;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class PointsModel extends JsonBaseModel{
  @NotNull
  public BasePointModel items[];
}
