package ru.ekassir.dbp.models.jsonrpc;

import ru.ekassir.dbp.models.json.JsonBaseModel;

public class JsonRPCBaseModel extends JsonBaseModel{
  public String jsonrpc = "2.0";
  public String method;
  public String id;
}
