package ru.ekassir.dbp.models.jsonrpc;

public class ErrorModel {
  public Integer code;
  public String message;
  public ErrorDataModel data;
}
