package ru.ekassir.dbp.models.jsonrpc.updatecache;

public class UpdateCacheParamsModel {
  public String eventTime;
  public OwnerId ownerId;
  //Возможные значения: session, contract, historyEvent, promo, application
  public String resourceType;
  public ResourceId resourceId;
  public Object resource;
  
  public UpdateCacheParamsModel(){
    this.eventTime = "2016-12-21T14:28:23+04:00";
    this.ownerId = new OwnerId();
    this.resourceType = "contract";
    this.resourceId = new ResourceId();
  }
  
  public UpdateCacheParamsModel(String eventTime, String resourceType, String ownerIdSource, String ownerIdId, String resourceIdSource, String resourceIdId){
    this.eventTime = eventTime;    
    this.resourceType = resourceType;
    this.ownerId = new OwnerId(ownerIdSource, ownerIdId);
    this.resourceId = new ResourceId(resourceIdSource, resourceIdId);
  }
  
  public UpdateCacheParamsModel(String eventTime, String resourceType, String ownerIdSource, String ownerIdId, String resourceIdSource, String resourceIdId, Object object){
    this.eventTime = eventTime;    
    this.resourceType = resourceType;
    this.ownerId = new OwnerId(ownerIdSource, ownerIdId);
    this.resourceId = new ResourceId(resourceIdSource, resourceIdId);
    this.resource = object;
  }
  
  public class OwnerId {
    //возможные значения am, dbp
    public String source;
    //source = am, id - идентификатор в АМ, поле account.name
    //source = dbp, id - идентификатор в DBP, поле account.id
    public String id;
    
    public OwnerId() {
      this.source = "dbp";
      this.id = "22309";
    }
    
    public OwnerId(String source, String id) {
      this.source = source;
      this.id = id;
    }
  }
  
  public class ResourceId {
    //возможные значения operationHub, dbp
    public String source;
    //source = operationHub, id - идентификатор сущности в operation hub
    public String id;
    
    public ResourceId() {
      this.source = "dbp";
      this.id = "14061457";
    }
    
    public ResourceId(String source, String id) {
      this.source = source;
      this.id = id;
    }
  }
}
