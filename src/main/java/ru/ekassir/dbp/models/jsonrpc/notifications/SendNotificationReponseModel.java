package ru.ekassir.dbp.models.jsonrpc.notifications;

import javax.validation.constraints.NotNull;

import ru.ekassir.dbp.models.jsonrpc.JsonRPCBaseModel;

public class SendNotificationReponseModel extends JsonRPCBaseModel{
  @NotNull
  String statusTime;
  
  public class Notifications {
    String id;
  }

}
