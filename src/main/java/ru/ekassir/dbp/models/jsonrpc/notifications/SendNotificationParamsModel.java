package ru.ekassir.dbp.models.jsonrpc.notifications;

public class SendNotificationParamsModel {
  public String eventTime;
  public OwnerId ownerId;
  public String notificationChannel = "push";
  public String messageText = "Message from autotest.";
  public String messageTitle = "Autotest";
  
  public SendNotificationParamsModel(String userId){
    this.eventTime = "2017-03-13T10:28:23+04:00";
    this.ownerId = new OwnerId(userId);   
  }
  
  public SendNotificationParamsModel(String eventTime, String resourceType, String ownerIdSource, String ownerIdId, String resourceIdSource, String resourceIdId){
    this.eventTime = eventTime;   
    this.ownerId = new OwnerId(ownerIdSource, ownerIdId);    
  }  
  
  public class OwnerId {
    //возможные значения am, dbp
    public String source;
    //source = am, id - идентификатор в АМ, поле account.name
    //source = dbp, id - идентификатор в DBP, поле account.id
    public String id;
    
    public OwnerId(String userId) {
      this.source = "dbp";
      this.id = userId;
    }
    
    public OwnerId(String source, String id) {
      this.source = source;
      this.id = id;
    }
  }
}
