package ru.ekassir.dbp.models.jsonrpc;

public class EchoModel {
  public String jsonrpc;
  public String method;
  public String id;
  
  public EchoModel() {
    this.jsonrpc = "2.0";
    this.method = "echo";
    this.id = "1";
  }

}
