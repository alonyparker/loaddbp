package ru.ekassir.dbp.models.jsonrpc.updatecache;

import ru.ekassir.dbp.models.jsonrpc.JsonRPCBaseModel;

public class UpdateCacheMethodModel extends JsonRPCBaseModel{
  public UpdateCacheParamsModel params;
  //Если не передавать id запрос асинхронный и ответа сразу не будет
  
  public UpdateCacheMethodModel(){    
    this.method = "updateCache";
    this.id = "7";
    this.params = new UpdateCacheParamsModel();
  }
  
  public UpdateCacheMethodModel(String eventTime, String resourceType, String ownerIdSource, String ownerIdId, String resourceIdSource, String resourceIdId){    
    this.method = "updateCache";    
    this.params = new UpdateCacheParamsModel(eventTime, resourceType, ownerIdSource, ownerIdId, resourceIdSource, resourceIdId);
  }
  
  public UpdateCacheMethodModel(String eventTime, String resourceType, String ownerIdSource, String ownerIdId, String resourceIdSource, String resourceIdId, String id){    
    this.method = "updateCache";
    this.id = id;
    this.params = new UpdateCacheParamsModel(eventTime, resourceType, ownerIdSource, ownerIdId, resourceIdSource, resourceIdId);
  }
  
  public UpdateCacheMethodModel(String eventTime, String resourceType, String ownerIdSource, String ownerIdId, String resourceIdSource, String resourceIdId, String id, Object object){    
    this.method = "updateCache";
    this.id = id;
    this.params = new UpdateCacheParamsModel(eventTime, resourceType, ownerIdSource, ownerIdId, resourceIdSource, resourceIdId, object);
  }
}
