package ru.ekassir.dbp.models.jsonrpc.notifications;

import ru.ekassir.dbp.models.jsonrpc.JsonRPCBaseModel;
import ru.ekassir.dbp.models.jsonrpc.notifications.SendNotificationParamsModel;

public class SendNotificationMethodModel extends JsonRPCBaseModel{
  public SendNotificationParamsModel params;
  
  public SendNotificationMethodModel(String userId){    
    this.method = "sendNotification";
    this.id = "7";
    this.params = new SendNotificationParamsModel(userId);
  }

}
