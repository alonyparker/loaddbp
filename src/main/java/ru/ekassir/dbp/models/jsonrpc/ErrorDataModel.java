package ru.ekassir.dbp.models.jsonrpc;

public class ErrorDataModel {
  public String ClassName;
  public String  Message;
  public String StackTraceString;
  public Integer RemoteStackIndex;
  public String  ExceptionMethod;
  public Long HResult;
  public String Source;
}
