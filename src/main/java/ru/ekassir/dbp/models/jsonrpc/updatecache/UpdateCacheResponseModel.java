package ru.ekassir.dbp.models.jsonrpc.updatecache;

import ru.ekassir.dbp.models.jsonrpc.ErrorModel;
import ru.ekassir.dbp.models.jsonrpc.JsonRPCBaseModel;

public class UpdateCacheResponseModel extends JsonRPCBaseModel{
  public ErrorModel error;
}
