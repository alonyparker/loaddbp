package ru.ekassir.dbp.models.xml;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ClientAuthorizationRequest")
public class ClientAuthorizationRequestModel {
	public ClientAuthorizationRequestModel(String crm, String externalPointId) {
		this.clientId = crm;		
		this.externalPointId = externalPointId;	
	}
	
	public ClientAuthorizationRequestModel(String crm) {
	  this(crm, "145443");			
	} 
	
	public ClientAuthorizationRequestModel() {          
  } 
	
	@XmlElement(name = "Login")
	String login = "SADMIN";
	@XmlElement(name = "OperatorFullName")
	String operatorFullName = "Иванов Иван Иванович";
	@XmlElement(name = "ClientId")
	String clientId;
	@XmlElement(name = "ExternalPointId")	
	String externalPointId; 
	@XmlElement(name = "ServiceRequestId")
	String serviceRequestId = "14000016697";
	@XmlElement(name = "ServiceRequestType")
	String serviceRequestType = "Payment";
	@XmlElement(name = "ActionId")
	String actionId = "6787548987";
	@XmlElement(name = "AuthType")
	String authType = "limited";
	@XmlElement(name = "AuthMethod")
	String authMethod = "passport";
	@XmlElement(name = "UserType")
	String userType = "cashier";
}
