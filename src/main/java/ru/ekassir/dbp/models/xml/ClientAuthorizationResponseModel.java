package ru.ekassir.dbp.models.xml;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ClientAuthorizationResponse")
public class ClientAuthorizationResponseModel {
  @XmlElement(name = "SessionId")
  public String sessionId;
}
