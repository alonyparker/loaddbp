package ru.ekassir.dbp.helpers;

import ru.ekassir.dbp.models.json.limits.LimitModel;

public class MessageLogHelper {
   public String correctDateFormatMessage(String date){
     return "Date \"" + date + "\" have correct format.";
     
     
   }
   public String hasnIncorrectDateFormat(String date){
	     return date + " has incorrect date format ";
	   }
//   public static String lastResponseDoesntReturn(Object model){
//     return "Last response doesnt return " + model.getClass().getName();
//   }
   public String correctFieldValueMessage(String fieldName){
     return "Field \"" + fieldName + "\" has wrong value ";
   }
//   public static String filtredFieldDoesntContain(String fieldName, String filterValue){
//     return "In filtred result " + fieldName + "doesnt contain " + filterValue;
//   }
//   public static String categoriesListDoesntContainCreatedCategory(){
//     return "Categories list doesnt contain created category ";
//   }
//   public static String isServiceIdInCategoriesCode(){
//     return "Is serviceId in categories code?";
//   }
   public String correctDateValueMessage(String date){
     return "\"" + date + "\" has wrong value";
   }
  public  String incorrectLimit(LimitModel limit){
     return "Limit \"" + limit.name + "\" id: " + limit.id + " has wrong fields.";
   }
  public  String wrongFieldsValue(String fieldName){
	     return "Field \"" + fieldName + "\" has wrong value ";
	   }
//
//   public static String doesFieldCityContainAcronym(){
//     return "Does fields city contain г. ?";
//   }


}