package ru.ekassir.dbp.helpers;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

import java.util.Base64;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.message.BasicHttpRequest;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpCoreContext;
import org.junit.Assert.*;
import org.junit.Assert;

import ru.ekassir.dbp.models.json.image.Base64Image;
import ru.ekassir.dbp.utils.PropertyReader;

public class ResponseHelper {
  public static final String PNG = "png";
  public static final String JPEG = "jpeg";
  public static final String GIF = "gif";
  public static final String PDF = "pdf";
  public static final String HTML = "html";
  JsonHelper JsonHelper = new JsonHelper();
  PropertyReader propertyReader = new PropertyReader();
   public String lastResponse;
   public String lastRequestedUrl;
  
   public void saveLastResponse(HttpResponse response){
    try {
      String ct = response.getFirstHeader("Content-type").getValue();
      switch(ct) {
      case "image/png":
        lastResponse = responseToImage(response, PNG);
        break;
      case "image/jpeg":
        lastResponse = responseToImage(response, JPEG);
        break;
      case "image/gif": 
        lastResponse = responseToImage(response, GIF);
        break; 
      case "application/vnd.ekassir.v1+json; charset=utf-8":
        if((lastRequestedUrl.contains("format")&&(lastRequestedUrl.contains("png")||lastRequestedUrl.contains("jpg")||lastRequestedUrl.contains("gif")))&&(response.getStatusLine().getStatusCode() == 200)){
          lastResponse = responseToBase64Image(response);
        }else{
          lastResponse = responseToString(response).replace("\uFEFF", "");
        }
        break;
      case "application/vnd.ekassir.v1+json":
        if((lastRequestedUrl.contains("format=")&&((lastRequestedUrl.contains("jpeg"))||(lastRequestedUrl.contains("png"))||lastRequestedUrl.contains("gif"))&&(response.getStatusLine().getStatusCode() == 200))){
          lastResponse = responseToBase64Image(response);
        }else{
          lastResponse = responseToString(response).replace("\uFEFF", "");
        }
        break;
      case "application/pdf":
        lastResponse = responseToImage(response, PDF);
        break;
      case "application/html":
        lastResponse = responseToString(response).replace("\uFEFF", "");
        responseToImage(response, HTML);
        break;
      case "text/html":
        lastResponse = responseToString(response).replace("\uFEFF", "");
        responseToImage(response, HTML);
        break;
      default:
        lastResponse = responseToString(response).replace("\uFEFF", "");
        break;
      }
    }
    catch(NullPointerException e) {
      try {
        lastResponse = responseToString(response).replace("\uFEFF", "");
      } catch (IOException e1) {        
        e1.printStackTrace();
      }
    }
    catch(IOException ioe){
     // Reporter.log(ioe.getLocalizedMessage(), true);
    }
    catch(ParseException pe) {
     // Reporter.log(pe.getLocalizedMessage(), true);
    }
    
  }
  public String responseToImage(HttpResponse response, String imageFormat) throws IOException{
    String saveImages = propertyReader.getProperty("saveImages");
   Assert.assertTrue(saveImages != null); 
    if (saveImages.equals("true")) {
      Throwable thr = new Throwable();
      StackTraceElement[] ste = thr.getStackTrace();    
      SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
      String date = format.format(new Date());
      InputStream is = response.getEntity().getContent();
      String logText = ste[6].getMethodName() + "_" + date + "." + imageFormat;
      new File("UploadedImages").mkdir();
      FileOutputStream fos = new FileOutputStream(new File("UploadedImages\\" + logText));
      int inByte;
      while ((inByte = is.read()) != -1) {
        fos.write(inByte);
      }
      is.close();
      fos.close();
      return logText;
    }
    return imageFormat + " image.";
  }
  
  public String responseToBase64Image(HttpResponse response) throws IOException{
    String saveImages = propertyReader.getProperty("saveImages");
    Assert.assertTrue(saveImages != null);
    String responseString = "", currentLine;
    if(response.getStatusLine().getStatusCode() == HttpStatus.SC_NOT_MODIFIED){
      return responseString;
    }
    BufferedReader br = new BufferedReader (new InputStreamReader(response.getEntity().getContent(),"UTF-8"));
    while ((currentLine = br.readLine()) != null) {
      responseString += currentLine;
    }
    if (saveImages.equals("true")) {
      Base64Image b64imodel = JsonHelper.deserializeJson(Base64Image.class, responseString);
      String base = b64imodel.data; 
      String imageFormat = base.substring(11, base.indexOf(",") - 7);
      String imageDataBytes = base.substring(base.indexOf(",") + 1);
      byte[] imageByteArray = Base64.getDecoder().decode(imageDataBytes);
      Throwable thr = new Throwable();
      StackTraceElement[] ste = thr.getStackTrace();
      SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
      String date = format.format(new Date());
      String logText = ste[6].getMethodName() + "_" + date + "." + imageFormat;
      FileOutputStream fos = new FileOutputStream(new File("UploadedImages\\" + logText));
      fos.write(imageByteArray);
      fos.close();
      return responseString;
    }
    return responseString;
  }

  public  String getLastResponse() {
    return lastResponse;
  }
  public  String getLastRequestedUrl() {
    return lastRequestedUrl;
  }
  //very bad
  public  void saveLastRequestedUrl(HttpContext context, String url){
    try{
      HttpUriRequest currentReq = (HttpUriRequest) context.getAttribute(HttpCoreContext.HTTP_REQUEST);
      HttpHost currentHost = (HttpHost) context.getAttribute(HttpCoreContext.HTTP_TARGET_HOST);
      lastRequestedUrl = (currentReq.getURI().isAbsolute()) ? currentReq.getURI().toString() : (currentHost.toURI() + currentReq.getURI()); 
    }catch(Exception e){
      BasicHttpRequest currentReq = (BasicHttpRequest) context.getAttribute(HttpCoreContext.HTTP_REQUEST);
      if(currentReq.getRequestLine().getMethod().equals("CONNECT")){
        lastRequestedUrl = url;
      }
    }
  }

  public  String responseToString(HttpResponse response) throws IOException{
    if(response == null){
      return null;
    }
    String responseString = "", currentLine;
    if(response.getStatusLine().getStatusCode() == HttpStatus.SC_NOT_MODIFIED){
      return responseString;
    }
    
    BufferedReader br = new BufferedReader (new InputStreamReader(response.getEntity().getContent(),"UTF-8"));

    while ((currentLine = br.readLine()) != null) {
      responseString += currentLine;
    }
    return responseString;
  }
}
