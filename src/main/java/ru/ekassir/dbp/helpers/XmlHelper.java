package ru.ekassir.dbp.helpers;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.LinkedList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import ru.ekassir.dbp.utils.PropertyReader;

public class XmlHelper{
  //TODO: разобраться с исключениями
  /*public static String serializeToXml(Class<?> model){
    Object o = null;
    try {
      o = model.getConstructor().newInstance();
    } catch (InstantiationException | IllegalAccessException
        | IllegalArgumentException | InvocationTargetException
        | NoSuchMethodException | SecurityException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
    String xml = null;
    try {
      JAXBContext context = JAXBContext.newInstance(model);
      Marshaller m = context.createMarshaller(); 
      m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      m.marshal(o, baos);
      xml = baos.toString();
    } catch (JAXBException e) {
      e.printStackTrace();
    }
    return xml;
  } */
	  protected PropertyReader propertyReader = new PropertyReader();
  public static String serializeToXml(Object o){
    Class<?> model = o.getClass();
    String xml = null;
    try {
      JAXBContext context = JAXBContext.newInstance(model);
      Marshaller m = context.createMarshaller(); 
      m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      m.marshal(o, baos);
      xml = baos.toString();
    } catch (JAXBException e) {
      e.printStackTrace();
    }
    return xml;
  }
  
  public static <T> T deserializeXml(Class<T> modelClass, String xml){
    Object o = null;
    try {
      JAXBContext context = JAXBContext.newInstance(modelClass);
      Unmarshaller un = context.createUnmarshaller();
      o = un.unmarshal(new ByteArrayInputStream(xml.getBytes()));
    } catch (JAXBException e) {
        e.printStackTrace();
    }
    return  modelClass.cast(o);
  }

  public static void modifyXml(String xmlPath) throws SAXException, IOException, TransformerException{
    try {
      DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
      Document doc = docBuilder.parse(xmlPath);
      
      NodeList list = doc.getElementsByTagName("AvailableServicePolicy");
      
      LinkedList<String> paymentToolsList = new LinkedList<String>();
      LinkedList<String> availableList = new LinkedList<String>();
      
      String firstServiceId = getAttrValueOrEmptyString("ServiceId", list.item(0));
      
      Node AvailableServicePolicyList = doc.getElementsByTagName("AvailableServicePolicyList").item(0);
      
      for(int i = 0; i < list.getLength(); i++){
        paymentToolsList.add(getAttrValueOrEmptyString("PaymentTool", list.item(i)));
        availableList.add(getAttrValueOrEmptyString("AvailableType", list.item(i)));
        
        if(!firstServiceId.equals(list.item(i).getAttributes().getNamedItem("ServiceId").getNodeValue())){
          if(!paymentToolsList.contains("3") && !availableList.contains("2") ){
            System.out.println(firstServiceId);
            Element newElement = doc.createElement("AvailableServicePolicy");
            newElement.setAttribute("ServiceId", firstServiceId);
            newElement.setAttribute("PaymentTool", "3");
            newElement.setAttribute("AvailableType", "2");
            AvailableServicePolicyList.appendChild(newElement);
          }
          firstServiceId = list.item(i).getAttributes().getNamedItem("ServiceId").getNodeValue();
          paymentToolsList.clear();
          availableList.clear();
        }
      }
      TransformerFactory transformerFactory = TransformerFactory.newInstance();
      Transformer transformer = transformerFactory.newTransformer();
      transformer.setOutputProperty(OutputKeys.METHOD, "xml");
      transformer.setOutputProperty(OutputKeys.INDENT, "yes");
      transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-16");
      DOMSource source = new DOMSource(doc);
      StreamResult result = new StreamResult(xmlPath);
      transformer.transform(source, result);
      //Resave(xmlPath, xmlPath);
    } catch (ParserConfigurationException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }
  
  private static String getAttrValueOrEmptyString(String attrName, Node node){
    String attrValue;
    try{
      attrValue = node.getAttributes().getNamedItem(attrName).getNodeValue();
    }catch(NullPointerException e){
      attrValue = "";
    }
    return attrValue;
  }
  
  public  String prettyPrintXml(String notPrettyXml) {
    String prettyPrint = propertyReader.getProperty("prettyPrintXml");
    switch(prettyPrint) {
    case "true":
      try {
        final InputSource src = new InputSource(new StringReader(notPrettyXml));
        final Node document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(src).getDocumentElement();
        final Boolean keepDeclaration = Boolean.valueOf(notPrettyXml.startsWith("<?xml"));
        //May need this: System.setProperty(DOMImplementationRegistry.PROPERTY,"com.sun.org.apache.xerces.internal.dom.DOMImplementationSourceImpl");
        final DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
        final DOMImplementationLS impl = (DOMImplementationLS) registry.getDOMImplementation("LS");
        final LSSerializer writer = impl.createLSSerializer();
        writer.getDomConfig().setParameter("format-pretty-print", Boolean.TRUE); // Set this to true if the output needs to be beautified.
        writer.getDomConfig().setParameter("xml-declaration", keepDeclaration); // Set this to true if the declaration is needed to be outputted.
        return writer.writeToString(document);
      } catch (Exception e) {
        return notPrettyXml;
      }
     default: 
       return notPrettyXml;
    }
  }
  
}