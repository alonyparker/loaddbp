package ru.ekassir.dbp.helpers;

public class HeadersConstants {
  //public static String ACCEPT_DEFAULT = "application/json";
  public static String EKASSIR_JSON_UTF8 = "application/vnd.ekassir.v1+json; charset=utf-8";
  public static String EKASSIR_JSON = "application/vnd.ekassir.v1+json";
  public static String EKASSIR_XML_UTF8 = "application/vnd.ekassir.v1+xml; charset=utf-8";
  public static String EKASSIR_XML = "application/vnd.ekassir.v1+xml";
  public static String EKASSIR_HTML_UTF8 = "application/vnd.ekassir.v1+html; charset=utf-8";
  public static String EKASSIR_HTML = "application/vnd.ekassir.v1+html";
  public static String EKASSIR_PDF_UTF8 = "application/vnd.ekassir.v1+pdf; charset=utf-8";
  public static String EKASSIR_PDF = "application/vnd.ekassir.v1+pdf";
  public static String STANDART_JSON_UTF8 = "application/json; charset=utf-8";
  public static String STANDART_JSON = "application/json";
  public static String STANDART_XML_UTF8 = "application/xml; charset=utf-8";
  public static String STANDART_XML = "application/xml";    
  public static String WEB_PAGE = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
  public static String ATM_USER_AGENT = "ATM/2.6.1234.2 (Device Features barcode, EPP, touchscreen, cardreader)";
  public static String INFINITY_USER_AGENT = "Infinity/2.2.6 (iOS; 7.0; iPhone5,4)";
  public static String PROMETHEUS_USER_AGENT = "Prometheus/1.7.2 (iOS; 7.1.2; iPhone3,1)";
  public static String WEB_FORM = "application/x-www-form-urlencoded";
  public static String STANDART_PNG = "image/png";
  public static String STANDART_JPEG = "image/jpeg";
  public static String STANDART_GIF = "image/gif";
}
