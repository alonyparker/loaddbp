package ru.ekassir.dbp.helpers;

import ru.ekassir.dbp.helpers.TestHelper;

import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.apache.logging.log4j.Logger;
//import org.testng.Assert;
//import org.testng.Reporter;
import org.junit.Assert.*;
//import org.testng.Assert;

import com.google.common.collect.ObjectArrays;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.SerializedName;

import gnu.trove.set.hash.THashSet;
import junit.framework.Assert;
import ru.ekassir.dbp.models.json.JsonBaseModel;
import ru.ekassir.dbp.utils.PropertyReader;
import org.apache.logging.log4j.LogManager;

public class JsonHelper {
	protected TestHelper TestHelper= new TestHelper();
	private Logger logger = LogManager.getRootLogger();
	//private TestHelper TestHelper = new TestHelper();
	
   /* public <T> T deserializeJson(Class<T> model, String json){
    Gson gson = new GsonBuilder().registerTypeAdapter(model, new ValidateDeserializer<T>()).create();
    T modelInstance = null;    
    logger.info("Create model \"" + model.getSimpleName() + "\" on the basis of server's answer.");
    modelInstance = gson.fromJson(json, model);
    logger.info("Created successfull model \"" + model.getSimpleName() + "\" on the basis of server's answer.");
    return modelInstance;
  }*/
  
	//без проверки
	 public <T> T deserializeJson(Class<T> model, String json){
		    Gson gson = new GsonBuilder().create();
		    T modelInstance = null;    
		    logger.info("Create model \"" + model.getSimpleName() + "\" on the basis of server's answer. Without check.");
		    modelInstance = gson.fromJson(json, model); 
		    logger.info("Created successfull model \"" + model.getSimpleName() + "\" on the basis of server's answer.. Without check.");
		    return modelInstance;
		  }
	 
  public <T> T deserializeJsonWithoutCheck(Class<T> model, String json){
    Gson gson = new GsonBuilder().create();
    T modelInstance = null;    
    logger.info("Create model \"" + model.getSimpleName() + "\" on the basis of server's answer. Without check.");
    modelInstance = gson.fromJson(json, model); 
    logger.info("Created successfull model \"" + model.getSimpleName() + "\" on the basis of server's answer.. Without check.");
    return modelInstance;
  }
  
  static public String serializeModel(Object object){
    Gson gson = new Gson();
    return gson.toJson(object);    
  }
  

  public Boolean isAllNotNullFieldsPresent(Object cls)  {
   Field[] fieldsArray = ObjectArrays.concat(cls.getClass().getDeclaredFields(), cls.getClass().getFields(), Field.class);

   for(Field field: fieldsArray) {
     String name = field.getName(); 
     Annotation[] annotations = field.getAnnotations();
     boolean isNotNull = false;
     for(Annotation annotation: annotations) {
       if(annotation instanceof NotNull) {
         isNotNull = true;
       }
     }
     field.setAccessible(true);
     try {
       if(isNotNull && field.get(cls) == null){
    	   logger.info("Mandatory fields: " + name + " for \""+ cls.getClass().getSimpleName() + "\" absent");
         throw new JsonParseException("Mandatory fields: " + name + " for \""+ cls.getClass().getSimpleName() + "\" absent");
       }
    } catch (IllegalArgumentException e) {
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    }
   }
   return true;
  }
 
  public class ValidateDeserializer<T> implements JsonDeserializer<T> {
    private Set<String> fields = null;
    private Set<String> notNullFields = null;

    @SuppressWarnings("rawtypes")
    public void init(Type type) {
      Class cls = (Class) type;
      Field[] fieldsArray = ObjectArrays.concat(cls.getDeclaredFields(), cls.getFields(), Field.class);
      fields = new THashSet<String>(fieldsArray.length);
      notNullFields = new THashSet<String>(fieldsArray.length);
      for(Field field: fieldsArray) {
        String name = field.getName().toLowerCase(); 
        Annotation[] annotations = field.getAnnotations();
        boolean isNotNull = false;
        for(Annotation annotation: annotations) {
          if(annotation instanceof NotNull) {
            isNotNull = true;
          } else if(annotation instanceof SerializedName) {
            name = ((SerializedName) annotation).value().toLowerCase();
          }
        }
        fields.add(name);
        if(isNotNull) {
          notNullFields.add(name);
        }
      }
    }

    public T deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
      //TODO: подумать как лучше получить имя
      String modelName = type.getTypeName().substring(type.getTypeName().lastIndexOf('.') + 1);      
      if(fields == null) {
        init(type); 
      }
      Set<Map.Entry<String, JsonElement>> entries = json.getAsJsonObject().entrySet();
      Set<String> keys = new THashSet<String>(entries.size());
      Set<String> unknownFields = new THashSet<String>(entries.size());  
      for (Map.Entry<String, JsonElement> entry : entries) {
        if(!entry.getValue().isJsonNull()) { 
          keys.add(entry.getKey().toLowerCase()); 
          unknownFields.add(entry.getKey().toLowerCase());
        }
      }              
      if ((!fields.containsAll(unknownFields))&&(!fields.contains("__schema"))) {
        unknownFields.removeAll(fields);        
        if((unknownFields.size() != 1) && (!unknownFields.toString().equals("links"))){
        	logger.info("Unknown fields for model \"" + modelName + "\": ");
          for(String unknownField : unknownFields) {
            if(!unknownField.equals("links")) {
              //Reporter.log(unknownField, true);
            }
          }
        }
      }
      //TODO: Проверка на дубликаты      
      if (!keys.containsAll(notNullFields)) { 
        ArrayList<String> nnf = new ArrayList<String>();
        logger.info("The required fields is absent in json for model \"" + modelName + "\": ");
        for(String notNullField : notNullFields){
          if(!keys.contains(notNullField)){
           // Reporter.log(notNullField, true);
            nnf.add(notNullField);
          }
        }
        logger.info("Mandatory fields is absent for model \"" + modelName + "\": " + nnf);
        throw new JsonParseException("Mandatory fields is absent for model \"" + modelName + "\": " + nnf);
      }
      return new Gson().fromJson(json, type); 
    }
  }
  
  //Проверяет в экземпляре модели по ее классу обязательные поля
  public void isAllMandatoryFieldsPresent(Object obj, Class<?> clazz) {
	  logger.info("Checking \"" + clazz.getSimpleName() + "\":");
    Field[] fieldsArray = obj.getClass().getDeclaredFields();
    Set<String> absentNotNullFields = new HashSet<String>();
    String name = null, type = null;
    int length = 0;
    boolean isArray = false;    
    try {      
      for(Field field: fieldsArray) {
        field.setAccessible(true);
        name = field.getName();
        type = field.getType().getSimpleName().toString();
        isArray = field.getType().isArray();
        Annotation[] annotations = field.getAnnotations();
        //Если поле - не массив
        if(isArray == false) {
          //Если поле стандартный Java тип
          if(TestHelper.isStandartJavaType(type)) {              
            for(Annotation annotation: annotations) {
              //Если у поля есть аннотация NotNull и поля нет в экземпляре, то добавляем емо название в absentNotNullFields
              if((annotation instanceof NotNull)&&(field.get(obj) == null)) {                
                absentNotNullFields.add(name);                
              }
            }  
          }
          //Если поле не стандартный Java тип
          else{
            for(Annotation annotation: annotations) {
              //Если у поля есть аннотация NotNull
              if(annotation instanceof NotNull) {
                //Поля нет в экземпляре, то добавляем емо название в absentNotNullFields
                if((field.get(obj) == null)){
                  absentNotNullFields.add(name);
                }
                //Поле есть в экземпляре. Рекурсивно выполняем для него этот метод
                else {
                  Object o = clazz.getField(name).get(obj);              
                  isAllMandatoryFieldsPresent(o, o.getClass());
                }
              }              
            }
          }
        }
        //Если поле массив
        else {
          for(Annotation annotation: annotations) {
            //Если у поля есть аннотация NotNull
            if(annotation instanceof NotNull) {
              //Поля нет в экземпляре, то добавляем его название в absentNotNullFields
              if(clazz.getDeclaredField(name).get(obj) == null){
                absentNotNullFields.add(name);
              }
              //Поле есть в экземпляре
              else {
                length = Array.getLength(clazz.getDeclaredField(name).get(obj));
                //Проверяем каждый элемент массива
                for(int i = 0; i < length; ++i) {                                
                  if(TestHelper.isStandartJavaType(type)) {           
                    for(Annotation innerAnnotation: annotations) {
                      if((innerAnnotation instanceof NotNull)&&(field.get(obj) == null)) {
                        absentNotNullFields.add(name);                       
                      }                 
                    }                
                  }
                  else{                
                    Object o = Array.get(clazz.getDeclaredField(name).get(obj), i);
                    isAllMandatoryFieldsPresent(o, o.getClass());
                  }
                }
              }
            }
          }
        }
      }
      //Если у класса есть суперкласс, проверяем его поля
      if((clazz.getSuperclass() != JsonBaseModel.class)&&(clazz.getSuperclass() != Object.class)) {
        Class<?> superClass = clazz.getSuperclass();
        isAllMandatoryFieldsPresent(obj, superClass);
      } 
    }
    catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
      e.printStackTrace();
      Assert.fail(e.getLocalizedMessage());
    }    
    //Выписываем отсутствующие обязательные поля
    if(!absentNotNullFields.isEmpty()) {      
    	logger.info("Absent mandatory fields for model \"" + clazz.getSimpleName() + "\":");
      for(String mandatoryField : absentNotNullFields) {
    	  logger.info("\"" + mandatoryField + "\"");
      }     
    }    
  }
 
  public String toPrettyFormat(String jsonString) 
  { 	
    try{
	  String prettyPrintJson = "true";		
	  switch(prettyPrintJson) {
	    case "true":
	      try {	        
	        JsonParser parser = new JsonParser();
          if(jsonString.substring(0, 1).equals("[")) {
            JsonArray jsonArray = parser.parse(jsonString).getAsJsonArray();          
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            return gson.toJson(jsonArray);
          }
	        JsonObject jsonObject = parser.parse(jsonString).getAsJsonObject();          
          Gson gson = new GsonBuilder().setPrettyPrinting().create();
          return gson.toJson(jsonObject);          
	      }
	        catch(IllegalStateException ise) {	       
	        return jsonString;
	      }
	      catch(JsonSyntaxException jse) {	        
          return jsonString;
	      }
	    default:
		  return jsonString;
	  }	
    }
    catch(NullPointerException c) {
    	return jsonString;
    }
  }
  
  public boolean isValidJson(String json) {
    JsonParser parser = new JsonParser();
    try {
      parser.parse(json).getAsJsonObject();
      return true;
    }
    catch(JsonSyntaxException e) {
      return false;
    }
  }
}
