package ru.ekassir.dbp.helpers;

import java.util.HashMap;
import java.util.Map;

//import org.testng.Reporter;

import ru.ekassir.dbp.models.json.MoneyModel;

public class MoneyBag {
  private Map<String, MoneyModel> currencies;
  
  public MoneyBag(){
    currencies = new HashMap<String, MoneyModel>();
  }
  
  public void add(MoneyModel money){    
    if(currencies.containsKey(money.currencyCode)){
      if(money.amount != 0){
        currencies.get(money.currencyCode).amount += money.amount;
      }
    }
    else{
      currencies.put(money.currencyCode, new MoneyModel(money.amount, money.currencyCode));
    }
  }
  
  public MoneyModel get(String currency) {
    try{
      return currencies.get(currency);
    }catch (NullPointerException e) {
   //   Reporter.log("Have no currency: " + currency, true);
    }
    return null;
  }
  
  public Long getAmount(String currency){
    try{
      return currencies.get(currency).getAmount();
    }catch (NullPointerException e) {
    //  Reporter.log("Have no currency: " + currency, true);
    }
    return 0L;
  }
  
  public void setAmount(String currency, Long amount) {
    try{
      currencies.get(currency).setAmount(amount);
    }catch (NullPointerException e) {
    //  Reporter.log("Can't set amount. Have no currency: " + currency, true);
    } 
  }

  public Map<String, MoneyModel> getCurrencies() {
    return currencies;
  }
}
