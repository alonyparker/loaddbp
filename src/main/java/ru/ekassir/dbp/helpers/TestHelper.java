package ru.ekassir.dbp.helpers;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.codec.digest.HmacUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONObject;
import org.junit.*;
import org.junit.Assert.*;
//import org.testng.Assert;
//import org.testng.Reporter;
//import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import ru.ekassir.dbp.utils.DateValidator;
import ru.ekassir.dbp.utils.PropertyReader;

public class TestHelper {
	DateValidator DateValidator = new DateValidator();
  private Logger logger = LogManager.getRootLogger();
  private final Set<String> STANDART_JAVA_TYPES = getStandartJavaTypes();
  //protected PropertyReader propertyReader = new PropertyReader();
  public String encodeDate(String date){
    String encodedDate = null;
    try {
      encodedDate = date.replace("+", URLEncoder.encode("+", "UTF-8"));
    } catch (UnsupportedEncodingException e) {
      //Reporter.log(e.getLocalizedMessage(), true);      
    }
    return encodedDate;
  }
  
  public int countZeroInMask(String mask){
    int count = StringUtils.countMatches(mask, "0");
    return count;
  }
  
  public int randInt(int min, int max) {
    Random rand = new Random();
    int randomNum = rand.nextInt((max - min) + 1) + min;
    return randomNum;
  }
 
  public String generateRandonGuid() {
    UUID uuid = UUID.randomUUID();
    String randomUUIDString = uuid.toString();
    return randomUUIDString;
  }
  
  public boolean isUid(String str) {
    Pattern p = Pattern.compile("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}");
    Matcher m = p.matcher(str);
    
    if(m.matches()){
      return true;
    }
    return false;
  }
  
  public String randHexColor() {
    Random rand = new Random();    
    float r = rand.nextFloat();
    float g = rand.nextFloat();
    float b = rand.nextFloat();
    Color randomColor = new Color(r, g, b);
    return "#" + toBrowserHexValue(randomColor.getRed()) + toBrowserHexValue(randomColor.getGreen()) + toBrowserHexValue(randomColor.getBlue());
  }
  
  private String toBrowserHexValue(int number) {
    StringBuilder builder = new StringBuilder(Integer.toHexString(number & 0xff));
    while (builder.length() < 2) {
      builder.append("0");
    }
    return builder.toString().toUpperCase();
  }

/*  public static void checkErrorInLastState(String lastResponse) throws Exception{
    StateModel stateModel = JsonHelper.stringToModel(StateModel.class, lastResponse);
    if(stateModel.errors.length != 0){
      String errorText = "State " + stateModel.template + " contain error "+ stateModel.errors[0].code + stateModel.errors[0].name + " " + stateModel.errors[0].value;
      Reporter.log(errorText, true);
      throw new Exception(errorText);
    }
  }
*/
  public  String addParamsToUrl(String url,List<NameValuePair> paramList){
    String params = "";
    for(NameValuePair valPair : paramList){
      String parameter = "";
      boolean isParamValueDateTime = DateValidator.isDateTime(valPair.getValue());
      boolean isParamValueDate = DateValidator.isDate(valPair.getValue());
      if(isParamValueDateTime || isParamValueDate) {
        parameter = valPair.getName() + "=" + encodeDate(valPair.getValue()) + "&";
      }else {
        try {
          parameter = valPair.getName() + "=" + URLEncoder.encode(valPair.getValue(), "UTF-8") + "&";
        } catch (UnsupportedEncodingException e) {
        //    Reporter.log(e.getLocalizedMessage(), true);          
        }
      }      
      params+= parameter; 
    }
    return url + "?" +  StringUtils.chop(params);
  }
  
  public boolean isInteger(String s) {
    try { 
      Integer.parseInt(s); 
    } catch(NumberFormatException e) { 
      return false; 
    } catch(NullPointerException e) {
      return false;
    }
    return true;
}

  public String getOtp(Boolean takeOtpFromFile, String defaultOtpValue, String filePath) throws IOException{
    if(takeOtpFromFile){
      InputStream fis = new FileInputStream(filePath);
      InputStreamReader isr = new InputStreamReader(fis, Charset.forName("UTF-8"));
      BufferedReader br = new BufferedReader(isr); 
      String lastLine = "", currentLine;
      while ((currentLine = br.readLine()) != null)
      {
        lastLine = currentLine;
      }
      br.close();
      
      Matcher m = Pattern.compile("(\\d{4,6})").matcher(lastLine);
      if(m.find()){ return m.group();} 
    }
    return defaultOtpValue;
  }
  
  public byte[] extractBytes (String imagePath, String imageType){
    BufferedImage originalImage = null;
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    try {
      originalImage = ImageIO.read(new File(imagePath));   
      ImageIO.write(originalImage, imageType, baos);
      baos.flush();
    } catch (IOException e) {      
     // Reporter.log(e.getLocalizedMessage(), true);
    }
    byte[] imageInByte = baos.toByteArray();
    return imageInByte;
  }
  
  public String addSimpleFieldToJsonString(String property, Object value, String json){
    if(isPrimitiveOrPrimitiveWrapperOrString(value.getClass())){
      JsonParser parser = new JsonParser();
      JsonObject jsonObject = parser.parse(json).getAsJsonObject();
      jsonObject.addProperty(property, value.toString());
      return jsonObject.toString();
    }else{
      String errorMessage = "Property not Primitive or Primitive Wrapper or String";
     // Reporter.log(errorMessage, true);
      return errorMessage;
    }
  }
  
  private boolean isPrimitiveOrPrimitiveWrapperOrString(Class<?> type) {
    return (type.isPrimitive() && type != void.class) ||
      type == Double.class || type == Float.class || type == Long.class ||
      type == Integer.class || type == Short.class || type == Character.class ||
      type == Byte.class || type == Boolean.class || type == String.class;
  }

 /* public static void checkCorrectLogin(String accountJson) {
   try {
     StateModel stateModel = JsonHelper.stringToModel(StateModel.class, accountJson);
     Assert.fail(stateModel.errors[0].value);
   }
   catch(JsonParseException e) {}
 } */
 
 public void checkCorrectLogin(String accountJson) {
	 
   JsonParser parser = new JsonParser();
   JsonObject operationJsonObject = parser.parse(accountJson).getAsJsonObject();   
   if((operationJsonObject.get("errors") != null)&&(operationJsonObject.get("errors").getAsJsonArray().size() != 0)){
     Assert.fail(operationJsonObject.get("errors").getAsJsonArray().get(0).getAsJsonObject().get("value").toString());
   }
   if((operationJsonObject.get("header") != null)&&(operationJsonObject.get("callbacks") != null)) {
     Assert.fail("Incorrect screen after login!\nHeader: " + operationJsonObject.get("header").getAsString());
   }
 }

/*  public static HistoryEventModel findEKHistoryEventFromTimeline(TimeLineModel timeLineModel){
    for(int i = 0; i < timeLineModel.events.items.length; i++){
      if(timeLineModel.events.items[i].type.equals("operation") && 
         timeLineModel.events.items[i].id.contains("EK") 
        )
      {
        return timeLineModel.events.items[i];
      }
    }
    Reporter.log("Timeline hasnt EK Events with operation type", true); 
    Assert.fail("Timeline hasnt EK Events with operation type");
    return null;
  }
  public static String generateRandomTenNumberString(){
    return String.valueOf(TestHelper.randInt(10001, 88889)) + String.valueOf(TestHelper.randInt(10001, 99999));
  }
  public static String generatePnoneNotExistingInOud(OudConnector oc){
    String randomMobileNumber = generateRandomTenNumberString();
    boolean isPhoneInOud = true;
    try {
      isPhoneInOud = oc.isEntryWithSuchAttributeExist("mobile", "+7" + randomMobileNumber) || oc.isEntryWithSuchAttributeExist("cn", randomMobileNumber);
    } catch (Exception e) {
      Reporter.log("Failed find entry with"+ randomMobileNumber +" in cn or mobile in oud", true);
      e.printStackTrace();
    }
    
    if(isPhoneInOud){
      generatePnoneNotExistingInOud(oc);
    }
    Reporter.log("Entry with "+ randomMobileNumber +" in cn or mobile not exist in oud", true);
    return randomMobileNumber;
  }
  */
  public String createBadTimeToken(String mkpersonid, String mkAccount){
    Integer mkid = randInt(100000, 999999);
    String secret = "551B8354D207766A";
    String mnum = "137777";
    String salt = "12345678ABCDIFGH";
    DateTime curDate = new DateTime(System.currentTimeMillis());
    String pattern = "dd.MM.yy HH:mm:ss";
    DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
    String time = formatter.print(curDate.minusWeeks(1));
    
    String beforeHash = mkid + "-" + mnum + "-" + mkpersonid + "-" + mkAccount + "-" + time + "-" + salt + "-" + secret;
    System.out.println(beforeHash);
    String hash = DigestUtils.sha1Hex(beforeHash);
    
    JSONObject TokenJson = new JSONObject();
    TokenJson.put("MKID", mkid);
    TokenJson.put("MNUM", mnum);
    TokenJson.put("MKPERSONID", mkpersonid);
    TokenJson.put("FROMACCT", mkAccount);
    TokenJson.put("TIME", time);
    TokenJson.put("SALT", salt);
    TokenJson.put("HASH", hash.toUpperCase());
    return TokenJson.toString();
  }
  
  public  String createToken(){
    JSONObject TokenJson = new JSONObject();
    TokenJson.put("MKID", JSONObject.NULL);
    TokenJson.put("MNUM", "137777");
    TokenJson.put("MKPERSONID", JSONObject.NULL);
    TokenJson.put("FROMACCT", JSONObject.NULL);
    TokenJson.put("TIME", JSONObject.NULL);
    TokenJson.put("SALT", "7C9CF653BE24FF38B3687BC25446B3C3");
    TokenJson.put("HASH", "CA2026395D6EA5EFEFD3F320EA0C5B95CF3D6A2D");
    return TokenJson.toString();
  }

  public  String createToken(String mkpersonid, String mkAccount){
    Integer mkid = randInt(100000, 999999);
    String secret = "551B8354D207766A";
    String mnum = "137777";
    String salt = "12345678ABCDIFGH";
    DateTime curDate = new DateTime(System.currentTimeMillis());
    String pattern = "dd.MM.yy HH:mm:ss";
    DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
    String time = formatter.print(curDate);
    
    String beforeHash = mkid + "-" + mnum + "-" + mkpersonid + "-" + mkAccount + "-" + time + "-" + salt + "-" + secret;
    System.out.println(beforeHash);
    String hash = DigestUtils.sha1Hex(beforeHash);
    
    JSONObject TokenJson = new JSONObject();
    TokenJson.put("MKID", mkid);
    TokenJson.put("MNUM", mnum);
    TokenJson.put("MKPERSONID", mkpersonid);
    TokenJson.put("FROMACCT", mkAccount);
    TokenJson.put("TIME", time);
    TokenJson.put("SALT", salt);
    TokenJson.put("HASH", hash.toUpperCase());
    return TokenJson.toString();
  }
  
  public  String createRtmToken(){
    JSONObject TokenJson = new JSONObject();
    TokenJson.put("MKID", JSONObject.NULL);
    TokenJson.put("MNUM", "LIPT0001");
    TokenJson.put("MKPERSONID", JSONObject.NULL);
    TokenJson.put("FROMACCT", JSONObject.NULL);
    TokenJson.put("TIME", JSONObject.NULL);
    TokenJson.put("SALT", JSONObject.NULL);
    TokenJson.put("HASH", JSONObject.NULL);
    return TokenJson.toString();
  }
  //now not used
  /*public static List<Method> getMethodsAnnotatedWith(final Class<?> type, final Class<? extends Annotation> annotation, boolean isEnabled) {
    final List<Method> methods = new ArrayList<Method>();
    Class<?> klass = type;
    while (klass != Object.class) {
      final List<Method> allMethods = new ArrayList<Method>(Arrays.asList(klass.getDeclaredMethods()));
      for (final Method method : allMethods) {
        if (method.isAnnotationPresent(annotation)) {
          Test annotInstance = method.getAnnotation(Test.class);
          if (annotInstance.enabled() == isEnabled) {
            methods.add(method);
          }
        }
      }
      klass = klass.getSuperclass();
    }
    return methods;
  }*/
  
 /* public String getAuthHeaderForCachControl(String requestHttpMethod, String body) {
    UUID uuid = UUID.randomUUID();
    //final String uri = PropertyReader.getProperty("endpointForCacheControl");
    final String appId = propertyReader.getProperty("appId");
    final String appKey = propertyReader.getProperty("appKey");
    final String authorizationSchema = propertyReader.getProperty("authSchema");
      
    //Get current unix time in seconds    
    long unixTime = System.currentTimeMillis() / 1000L;
    //Reporter.log("Unix time: " + unixTime, true);    
    //Generate nonce (random uuid)
    String randomUUIDString = uuid.toString();
   // Reporter.log("Nonce: " + randomUUIDString, true);          
    byte[] md5BytesFromBody = DigestUtils.md5(body);    
    byte[] base64BytesFromMd5Bytes = Base64.encodeBase64(md5BytesFromBody);
    String base64strMD5 = new String(base64BytesFromMd5Bytes);
   // Reporter.log("Base64 result of calculating MD5 hash from request body: " + base64strMD5, true);    
    String commonStringToCalculateSignature = appId + requestHttpMethod + unixTime + randomUUIDString + base64strMD5;
   // Reporter.log("Full string for calculating signature: " + commonStringToCalculateSignature, true);
    byte[] utf8SignatureBytes =  commonStringToCalculateSignature.getBytes(StandardCharsets.UTF_8);
    byte[] base64AppKeyBytes = Base64.decodeBase64(appKey);    
    byte[] signatureHmacBytes = HmacUtils.hmacSha256(base64AppKeyBytes, utf8SignatureBytes);    
    byte[] signatureBase64Bytes = Base64.encodeBase64(signatureHmacBytes);
    String signature = new String(signatureBase64Bytes);
   // Reporter.log("Calculated signature: " + signature, true);       
    String header = authorizationSchema + " " + appId + ":" + signature + ":" + randomUUIDString + ":" + unixTime;    
   // Reporter.log("Authorization header: " + header + "\n", true);
    return header;
  }*/
  
  //Проверка строки на соответствие стандартным типам Java
  public boolean isStandartJavaType(String type)
  {
    type = type.toLowerCase();
    type = type.replaceAll("\\[\\]$", "");    
    return STANDART_JAVA_TYPES.contains(type);
  }

  protected  Set<String> getStandartJavaTypes()
  {
    Set<String> ret = new HashSet<String>();
    ret.add("boolean");
    ret.add("character");
    ret.add("byte");
    ret.add("short");
    ret.add("integer");
    ret.add("long");
    ret.add("float");
    ret.add("double");
    ret.add("void");
    ret.add("int");
    ret.add("char");
    ret.add("string");
    return ret;
  }
  
  //Обертки для логирования
  public void log(String s) {
   // Reporter.log(s, true);
    logger.info(s);
  }
  
  public void logWarn(String s) {
 //   Reporter.log("WARNING: " + s, true);
    logger.warn(s);
  }
  
  public void logError(String s) {
  //  Reporter.log("ERROR: " + s, true);
    logger.error(s);
  }
}
