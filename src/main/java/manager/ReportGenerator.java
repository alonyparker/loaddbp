package manager;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/*import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestContext;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;
import org.testng.annotations.Test;
import org.testng.xml.XmlSuite;*/
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class ReportGenerator /*implements IReporter*/ {

	// привязка к testsuite для публикации результатов

	private static final String TFS_CONNECTION_URL = "https://tfs.ekassir.com/tfs/eKassirProjectCollection/"; // изменение
																												// url-а
																												// коннекта

	private static final String TFS_COLLECTION_TYPE = "eKassir RBP"; // выбор
																	// проекта

	private static final String TEST_PLAN_ID = "86281"; // выбор тестплана

	private static final String TEST_SUITE_ID = "86649"; // выбор тест-пакета

	private static final String CONFIGURATION = "55"; // выбор конфигурации

	private static final String BUILD_NUMBER = "DBP_MTEST_24032017 15062017"; // установка
																							// версии
																							// билда

	public static final String ATTRIBUTE_ID = "id";

	private static final String RESULT_PATH = "resources\\results\\results.xml";

	public static final String UPLOADER_EXE = "resources\\uploader.exe";

	public static final String UPLOADER_PATH = "resources";

	private static final boolean IS_NEED_PUBLICATE = true; // указывать false в
															// случае если не
															// требуется
															// публикация
															// результатов

	private static final String TESTS_LOGGER_PATH = "tests_logger\\";

	final static Logger logger = LogManager.getLogger(ReportGenerator.class);

	public static void main(String[] args) {
		try {
			Runtime.getRuntime().exec(ReportGenerator.UPLOADER_EXE, null, new File(UPLOADER_PATH));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*private void logErrorParsing(ITestNGMethod iTestNGMethod) {
		System.out.println("");
		System.out.println("==== ERROR PARSING TEST CASE ID  " + iTestNGMethod + "====");
		System.out.println("Do not forget add test id to attribute 'description' in test method annotation");
		System.out.println("==== TEST SKIPPED ====");
		System.out.println("");
		logger.error(
				"ERROR PARSING TEST CASE ID. Do not forget add test id to attribute 'description' in test method annotation. TEST SKIPPED "
						+ iTestNGMethod);

	};

	public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {

		for (ISuite suite : suites) {
			for (ISuiteResult result : suite.getResults().values()) {
				ITestContext context = result.getTestContext();
				System.out.println(result.getTestContext() + " tests context");
				logger.info("Test Case's result was received: " + context);
				List<ITestResult> allResults = new ArrayList<ITestResult>(context.getFailedTests().getAllResults());
				allResults.addAll(context.getSkippedTests().getAllResults());
				allResults.addAll(context.getPassedTests().getAllResults());

				if (!allResults.isEmpty()) {
					try {
						DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
						Document doc = docBuilder.newDocument();

						// header
						Element tests = doc.createElement("tests");
						doc.appendChild(tests);
						Attr attr = doc.createAttribute("testsuite");
						attr.setValue(TEST_SUITE_ID);
						tests.setAttributeNode(attr);
						attr = doc.createAttribute("tfsconnectionuri");
						attr.setValue(TFS_CONNECTION_URL);
						tests.setAttributeNode(attr);
						attr = doc.createAttribute("tfscollectionType");
						attr.setValue(TFS_COLLECTION_TYPE);
						tests.setAttributeNode(attr);
						attr = doc.createAttribute("testplanId");
						attr.setValue(TEST_PLAN_ID);
						tests.setAttributeNode(attr);
						attr = doc.createAttribute("configuration");
						attr.setValue(CONFIGURATION);
						tests.setAttributeNode(attr);
						attr = doc.createAttribute("buildNumber");
						attr.setValue(BUILD_NUMBER);
						tests.setAttributeNode(attr);

						for (ITestResult testResult : allResults) {
							String id = null;
							Test testAnnotation = testResult.getMethod().getConstructorOrMethod().getMethod()
									.getAnnotation(Test.class);
							id = testAnnotation.description();
							String resultValue = testResult.isSuccess() ? "true" : "false";

							for (String retval : id.split(",")) {

								if (!StringUtils.isNumeric(retval)) {
									logErrorParsing(testResult.getMethod());
									continue;
								}

								Element item = doc.createElement("item");
								attr = doc.createAttribute("item");
								attr.setValue(retval);
								item.setAttributeNode(attr);
								attr = doc.createAttribute("result");
								attr.setValue(resultValue);
								item.setAttributeNode(attr);
								tests.appendChild(item);
							}

							Date date = new Date();
							SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
							SimpleDateFormat dateFormatLog = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
							File newFile = new File(TESTS_LOGGER_PATH + dateFormat.format(date) + ".log");

							try {
								boolean created = newFile.createNewFile();
								if (created)
									System.out.println("Файл создан");
								logger.info("Logging file of the tests was created");

								try {
									String case_res = new String();
									if (resultValue.equals("true")) {
										case_res = "Passed";
									} else {
										case_res = "Fail";
									}

									FileWriter writer = new FileWriter(
											TESTS_LOGGER_PATH + dateFormat.format(date) + ".log", true);
									BufferedWriter bufferWriter = new BufferedWriter(writer);
									bufferWriter.write(dateFormatLog.format(context.getStartDate()) + "  "
											+ testResult.getMethod().getMethodName() + "  " + case_res + "\n");
									bufferWriter.close();
								}

								catch (IOException ex) {

									System.out.println(ex.getMessage());
									logger.warn("File already exists" + ex.getMessage());
								}
							} catch (IOException ex) {

								System.out.println(ex.getMessage());
								logger.warn("File already exists" + ex.getMessage());
							}
						}

						// writer
						TransformerFactory transformerFactory = TransformerFactory.newInstance();
						Transformer transformer = transformerFactory.newTransformer();
						DOMSource source = new DOMSource(doc);
						File file = new File(RESULT_PATH);
						if (!file.getParentFile().exists())
							file.getParentFile().mkdir();
						file.createNewFile();
						StreamResult res = new StreamResult(file);
						transformer.transform(source, res);
					} catch (Exception e) {
						e.printStackTrace();
						System.out.println(" =========== Exception while creating results =========");
						logger.warn("Exception while creating results.");

					}

					if (IS_NEED_PUBLICATE) {
						try {

							Runtime.getRuntime().exec(ReportGenerator.UPLOADER_EXE, null, new File(UPLOADER_PATH));
							System.out.println(" =========== Results uploaded! =========");
							logger.info("Results uploaded!");

						} catch (Exception e) {
							e.printStackTrace();
							System.out.println(" =========== Exception while posting results =========");
							logger.warn("Exception while posting results.");
						}
					}
				}
			}
		}
	}*/
}